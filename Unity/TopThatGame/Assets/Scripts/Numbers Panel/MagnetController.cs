﻿using UnityEngine;
using System.Collections;

public class MagnetController : MonoBehaviour
{

    public enum eMagnetStatus
    {
        Free, 

        Interacted , 

        Attached
    }

    #region Events

    public delegate void RegisterCollision(MagnetController i_Invoker, MagnetController i_Invoked , Detector i_Detector);

    public event RegisterCollision RegisterCollisionEvent;

    public delegate void RegisterDrag(MagnetController i_Invoker);

    public event RegisterDrag RegisterDownEvent;

    public delegate void RegisterUp(MagnetController i_Invoker , bool i_IsForced = false);

    public event RegisterUp RegisterUpEvent;

    public delegate void RegisterExit(MagnetController i_Invoker, MagnetController i_Invoked , Detector i_Detector);

    public event RegisterExit RegisterExitEvent;

    public delegate void ResisterDrag(MagnetController i_Invoker);

    public event ResisterDrag ResisterDragEvent;


    #endregion 

    public int mr_Id;

    private int m_number;

    public  TextMesh mr_textNumber;

    public GameObject mr_Detectors;

    public tk2dSprite mr_Sprite;

    private string m_spriteName;

    private Vector3 m_LastPosition;

    private bool m_isInvoker;

    public eMagnetStatus m_magnetStatus;

    private bool m_isDragAlowed = true;


    public int GetNumber
    {
        get
        {
            return this.m_number;
        }
    }

    private void Awake()
    {
        this.m_spriteName = this.mr_Sprite.CurrentSprite.name;
    }

    public void SetOption(int i_Number)
    {
        this.m_number = i_Number;
        this.mr_textNumber.text = i_Number.ToString();
    }


    private void OnMouseDown()
    {
        this.m_LastPosition = this.transform.localPosition;
        this.mr_Sprite.transform.localPosition = this.mr_Sprite.transform.localPosition + new Vector3(0, 0, -5);
        this.mr_textNumber.transform.localPosition = this.mr_textNumber.transform.localPosition + new Vector3(0, 0, -5);
        if (this.RegisterDownEvent != null)
        {
            this.RegisterDownEvent(this);
        }
    }

    private void OnMouseUp()
    {
        if (this.RegisterUpEvent != null)
        {
            this.RegisterUpEvent(this);
        }

        this.mr_Sprite.transform.localPosition = this.mr_Sprite.transform.localPosition + new Vector3(0, 0, 5);
        this.mr_textNumber.transform.localPosition = this.mr_textNumber.transform.localPosition + new Vector3(0, 0, 5);
    }

    private void OnMouseDrag()
    {
        if (this.m_isDragAlowed)
        {
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector3(worldPosition.x, worldPosition.y, -20) + new Vector3(-33, 180, 0);
            if (this.ResisterDragEvent != null)
            {
                this.ResisterDragEvent(this);
            }
        }

    }

    void OnTriggerEnter(Collider other)
    {
        var detector = other.GetComponent<Detector>();
        var border = other.GetComponent<BorderController>();
        if (border != null)
        {
            if (this.RegisterUpEvent != null)
            {
                this.RegisterUpEvent(this,true);
            }
            this.m_isDragAlowed = false;

        }

        if (!this.m_isInvoker || detector == null)
        {
            return;
        }
        var planet = other.gameObject.GetComponentInParent<MagnetController>();
        if (this.RegisterCollisionEvent != null)
        {
            this.RegisterCollisionEvent(this, planet,detector);
        }
    }

    void OnTriggerExit(Collider other)
    {
        var detector = other.GetComponent<Detector>();
        if (!this.m_isInvoker || detector == null)
        {
            return;
        }
        var planet = other.gameObject.GetComponentInParent<MagnetController>();
        if (this.RegisterExitEvent != null)
        {
            this.RegisterExitEvent(this, planet, detector);
        }
    }

    public void MakeMagnetInvoked()
    {
        this.m_isDragAlowed = true;
        this.m_isInvoker = false;
        this.GetComponent<BoxCollider>().enabled = false;
        this.GetComponent<Rigidbody>().collider.enabled = false;
        this.mr_Detectors.gameObject.SetActive(true);
    }

    public void MakeMagnetInvoker()
    {
        this.m_isDragAlowed = true;
        this.m_isInvoker = true;
        this.mr_Detectors.gameObject.SetActive(false);
        this.m_magnetStatus = eMagnetStatus.Free;
        this.GetComponent<BoxCollider>().enabled = true;
        this.GetComponent<Rigidbody>().collider.enabled = true;
    }

    public void SetMagnetStatus(eMagnetStatus i_MagnetStatus)
    {
        this.m_magnetStatus = i_MagnetStatus;
        if (this.m_isInvoker)
        {
            switch (i_MagnetStatus)
            {
                    case eMagnetStatus.Attached:
                        break;

                    case eMagnetStatus.Free:
                        break;

                    case eMagnetStatus.Interacted:
                        break;
            }
        }
        else 
        {
            switch (i_MagnetStatus)
            {
                case eMagnetStatus.Attached:

                    break;

                case eMagnetStatus.Free:
                    break;

                case eMagnetStatus.Interacted:
                    break;
            }
        }
    }

    public void DisableMagnet()
    {
        this.mr_Sprite.SetSprite(this.m_spriteName + "-dis");
    }

    public void EnableMagnet()
    {
        this.mr_Sprite.SetSprite(this.m_spriteName);
    }
}

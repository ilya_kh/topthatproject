﻿using UnityEngine;
using System.Collections;

public class ResignAsked : ViewEventAbstract
{
    public readonly BoardController.ePlayer PlayerAsked;

    public ResignAsked(BoardController.ePlayer i_Player)
    {
        this.PlayerAsked = i_Player;
    }

}

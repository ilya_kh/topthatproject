using SignalR.Infrastructure;

using UnityEngine;

public class CommunicationBridge : MonoBehaviour
{
    public delegate void PauseChangedEvent(bool i_IsPaused);
    public event PauseChangedEvent PauseChanged;

    public CommunicationBridgeMultiThreadRunner mr_Runner;
    public PauseScreen mr_PauseScreen;

    public string m_host = "";	
	public string m_sessionID = "";
    
    public string _sessionCode;
    private string _url;

    public bool m_IsPaused = false;
	
	void Start() {
		DontDestroyOnLoad(this.transform.gameObject);
	    mr_Runner.GameMessageEvent += this.TogglePauseGame;
	}
	
	void TogglePauseGame(string text) {
		if (text == "pause")		setPause(true);
		else if (text == "resume")	setPause(false);
		else if (text == "quit")
		{
            Debug.Log("Exiting Game!!!");

            if (Application.platform == RuntimePlatform.Android)
            {
                //Android
                Application.Quit();
            }
            else
            {
                //Web
                Application.ExternalEval("jQuery(document).trigger('unityClose');");
            }
		}
	}
	
	void setPause(bool state)	{
        Time.timeScale = state ? 0 : 1;
        AudioListener.volume = state ? 0 : 1;

        if (state)
        {
            this.m_IsPaused = true;
            this.mr_PauseScreen.Show();
            if (this.PauseChanged != null)
            {
                this.PauseChanged(true);
            }
        }
        else
        {
            this.m_IsPaused = false;
            this.mr_PauseScreen.Hide();
            if (this.PauseChanged != null)
            {
                this.PauseChanged(false);
            }
        }
	}

    #region Web
    public void PauseGame()
    {
        this.TogglePauseGame("pause");
    }

    public void ResumeGame()
    {
        this.TogglePauseGame("resume");
    }
    #endregion

    public void SetHost(string i_Host){
        this.m_host = UriQueryUtility.UrlDecode(i_Host);
		Debug.Log ("The host is : " + m_host);
        this.StartCommunicationBridge();
	}
	
	public void SetSessionID(string i_SessionID){
        this.m_sessionID = i_SessionID;
		Debug.Log ("The session ID is : " + m_sessionID);
    }

    public void StartCommunicationBridge()
    {
        if (this.m_host != null && this.m_sessionID != null)
        {
            Debug.Log("Starting Communication Bridge!!!");
            mr_Runner.InitWorker(this.m_host, m_sessionID);
            mr_Runner.StartListeningToDigitalClassServer();
        }
        else
        {
            Debug.Log("Error Starting Communication Bridge!!!");
        }
    }

    //Test Digital Class
    public void Update()
    {

        if (!Application.isEditor)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.A))        {
           TogglePauseGame("pause");
       }
       if (Input.GetKeyDown(KeyCode.X))
        {
            TogglePauseGame("resume");
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            TogglePauseGame("quit");
        }
    }
}

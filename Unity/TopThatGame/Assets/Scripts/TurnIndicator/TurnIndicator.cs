﻿using UnityEngine;
using System.Collections;

public class TurnIndicator : MonoBehaviour
{

    public tk2dSprite mr_Sprite;

    public MLTextMesh mr_Text;

    public Material mr_Orange;

    public Material mr_Purpe;


    private Vector3 mr_StartPosition;

    private Vector3 mr_EndPosition;

    private Vector3 mr_AlmostEndPosition;

    private readonly Vector3 m_endPositionOffset = new Vector3(500,0,0);

    private readonly Vector3 m_endPositionToAlmostEndOffset = new Vector3(430,0,0);


    private void Awake()
    {
        this.mr_StartPosition = gameObject.transform.localPosition;
        this.mr_Sprite.gameObject.SetActive(false);
    }


    public IEnumerator ShowTurn(BoardController.ePlayer i_Player)
    {
        this.setImage(i_Player);
        this.setPositions(i_Player);
        this.transform.localPosition = this.mr_StartPosition;
        yield return new WaitForSeconds(0.5F);
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.TurnIn, SoundManagerStaticInfo.eTags.Effects, false);
        this.mr_Sprite.gameObject.SetActive(true);
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0.1F));
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.mr_StartPosition, this.mr_EndPosition, 0.5f));
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.mr_EndPosition, this.mr_AlmostEndPosition, 0.35f));
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.mr_AlmostEndPosition,this.mr_EndPosition, 0.35f));

        /*
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.mr_EndPosition, this.mr_AlmostEndPosition, 0.2f));
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.mr_AlmostEndPosition, this.mr_EndPosition, 0.2f));
        */
        yield return new WaitForSeconds(0.5F);
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 1, 0, 0.3F));
        this.transform.localPosition = new Vector3(9000,9000,9000);
        this.mr_Sprite.gameObject.SetActive(false);
        yield break;
    }

    private void setPositions(BoardController.ePlayer i_Player)
    {
        if (i_Player == BoardController.ePlayer.Purple)
        {
            this.mr_EndPosition = this.mr_StartPosition + this.m_endPositionOffset;
            this.mr_AlmostEndPosition = this.mr_StartPosition + this.m_endPositionToAlmostEndOffset;
        }

        if (i_Player == BoardController.ePlayer.Red)
        {
            this.mr_EndPosition = this.mr_StartPosition - this.m_endPositionOffset;
            this.mr_AlmostEndPosition = this.mr_StartPosition - this.m_endPositionToAlmostEndOffset;
        }
    }

    private void setImage(BoardController.ePlayer i_Player)
    {
        if (i_Player == BoardController.ePlayer.Red)
        {
            this.mr_Sprite.SetSprite("Red");
            this.mr_Text.ChangeText(TextService.GetTextById("1-1-10"));
            this.mr_Text.renderer.material = this.mr_Orange;
        }
        if (i_Player == BoardController.ePlayer.Purple)
        {
            this.mr_Sprite.SetSprite("Purple");
            this.mr_Text.ChangeText(TextService.GetTextById("1-1-10"));
            this.mr_Text.renderer.material = this.mr_Purpe;
        }
    }



}

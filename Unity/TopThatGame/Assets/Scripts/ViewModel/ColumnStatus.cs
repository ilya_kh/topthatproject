﻿using UnityEngine;
using System.Collections;

public class ColumnStatus  {


    public enum eColumnStatus
    {
        PossibleClimb ,

        ImpossibleClimb,

        ColumnConquered
    }


    public readonly eColumnStatus Status;

    public readonly ColumnController Column;

    public ColumnStatus(ColumnController i_ColumnController, eColumnStatus i_ColumnStatus)
    {
        this.Status = i_ColumnStatus;
        this.Column = i_ColumnController;
    }

}

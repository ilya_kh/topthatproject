﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.View.ConcreteResponses
{
    public class ShowPerformExclusiveMoveActionResponse : ActionResponseAbstract
    {
        public readonly Tuple<int, int> Options;

        public ShowPerformExclusiveMoveActionResponse(Tuple<int, int> i_Options)
        {
            Options = i_Options;
        }
    }
}

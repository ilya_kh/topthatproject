﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class responsibility is handle initialization process (by some smart init module component )
///  and handle transition between screens based on events from screens. 
/// </summary>
public class ApplicationManager : MonoBehaviour
{

    public GameView mr_GameView;

    public MainScreenView mr_MainScreenView;

    public SuccsesScreenView mr_SuccsesScreenView;

    public GamePopUpManager mr_GamePopUpManager;

    private Vector3 m_TrashLocalPosition = new Vector3(9000, 9000, 9000);

    // Use this for initialization
	IEnumerator Start ()
	{
	    Input.multiTouchEnabled = false;
        SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Music, false);
        SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Effects, false);

	    this.StartCoroutine(this.mr_MainScreenView.StartSplashAnimation());

        yield return this.StartCoroutine(TextService.Instance.Init());
        this.setListenersToGameEvents();
        this.setListenersToMainScreenEvents();
	    this.setListenersToSuccsesScreenEvents();
        yield return this.StartCoroutine(this.mr_GamePopUpManager.Init());
        this.mr_GamePopUpManager.transform.localPosition = Vector3.zero;
        yield return this.StartCoroutine(this.mr_MainScreenView.Init());
	    this.mr_GameView.transform.localPosition = this.m_TrashLocalPosition;
	    yield return this.StartCoroutine(this.mr_GameView.Init());
	}



    private void setListenersToGameEvents()
    {
        this.mr_GameView.GameWasEndedEvent += this.onGameWonEvent;
        this.mr_GameView.BackToMainRequestedEvent += this.onGotoHomeEvent;
    }

    private void setListenersToMainScreenEvents()
    {
        this.mr_MainScreenView.StartGameEvent += this.onStartGameEvent;
    }

    private void setListenersToSuccsesScreenEvents()
    {
        this.mr_GamePopUpManager.RestartEvent += this.onRestartGameEvent;
        this.mr_GamePopUpManager.GoToHomeEvent += this.onGotoHomeEvent;

    }

    private void onGotoHomeEvent()
    {     
        this.StartCoroutine(this.mr_MainScreenView.EnableScreen(false));
        this.mr_GameView.transform.localPosition = this.m_TrashLocalPosition;
        StartCoroutine(this.mr_GamePopUpManager.Hide());
    }

    private void onRestartGameEvent()
    {
        this.StartCoroutine(this.RestartEnumarator());
    }

    private void onStartGameEvent()
    {
        
        this.StartCoroutine(this.StartGameEnumarator());
    }

    private IEnumerator StartGameEnumarator()
    {
        this.mr_GameView.Reset();
        this.mr_GameView.transform.localPosition = Vector3.zero;
        yield return this.StartCoroutine(this.mr_MainScreenView.HideAnimation());
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Equipment, SoundManagerStaticInfo.eTags.Effects, false);
        this.StartCoroutine(this.mr_GameView.StartGame());
    }

    private void onGameWonEvent(BoardController.ePlayer i_WonPlayer, bool i_IsGameWasResigned = false)
    {
        this.StartCoroutine(onGameWonEventEnumarator(i_WonPlayer, i_IsGameWasResigned));

    }

    private IEnumerator onGameWonEventEnumarator(BoardController.ePlayer i_WonPlayer, bool i_IsGameWasResigned = false)
    {
        this.mr_GameView.transform.localPosition = this.m_TrashLocalPosition;
        if (this.mr_GamePopUpManager.IsActivePopUp())
        {
            yield return
                this.StartCoroutine(
                    this.mr_GamePopUpManager.SwitchToPopUp(
                        PopUpManager.ePopUpType.Success, new SuccessState(i_WonPlayer, i_IsGameWasResigned)));
        }
        else
        {
            yield return
    this.StartCoroutine(
        this.mr_GamePopUpManager.Show(
            PopUpManager.ePopUpType.Success, new SuccessState(i_WonPlayer, i_IsGameWasResigned)));
        }
        this.mr_GameView.transform.localPosition = Vector3.zero;
        yield break;
    }


    /// <summary>
    /// Called when its from GameView. 
    /// </summary>
    /// <returns></returns>
    private IEnumerator RestartEnumarator()
    {
        this.mr_GameView.Reset();
        yield return this.StartCoroutine(this.mr_GamePopUpManager.Hide());
        this.StartCoroutine(this.mr_GameView.StartGame());
        yield break;
    }
}

﻿using System.Collections;

public class RedPanelPopUpManager : PanelPopUpManager 
{
    public override IEnumerator ShowMovePopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Red_MovePopUp, i_Param));
    }

    public override IEnumerator ShowExlusiveMovePopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Red_ExclusiveMovePopUp,i_Param));
    }

    public override IEnumerator ShowNoMovesPopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Red_NoLegalMovesPopUp, i_Param));
    }

    public override IEnumerator ShowPlayOrKeppPopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Red_KeepOrPlayPopUp, i_Param));
    }

    public override IEnumerator ShowIlegalChoicePopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Red_IlegalChoicePopUp, i_Param));
    }

    
}


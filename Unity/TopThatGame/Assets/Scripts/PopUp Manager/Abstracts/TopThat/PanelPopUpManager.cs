﻿using System.Collections;
using System.Linq;
using Assets.Scripts.PopUp_Manager.Abstracts.TopThat;

public abstract class PanelPopUpManager : PopUpManager
{

    public event GameView.UserInteraction UserInteractionEvent;

    public override IEnumerator SpecificInit()
    {
        this.GetComponentsInChildren<TopThatPanelPopUp>(true).ToList().ForEach(m => m.UserEvent += this.onUserEvent); 
        yield break;
    }

    private void onUserEvent(ViewEventAbstract i_ViewEventAbstract)
    {
        if (this.UserInteractionEvent != null)
        {
            this.UserInteractionEvent(i_ViewEventAbstract);
        }
    }


    public virtual IEnumerator HidePopUp()
    {
        yield return this.StartCoroutine(this.Hide());
        yield break;
    }

    public abstract IEnumerator ShowMovePopUp(object i_Param = null);

    public abstract IEnumerator ShowExlusiveMovePopUp(object i_Param = null);

    public abstract IEnumerator ShowNoMovesPopUp(object i_Param = null);

    public abstract IEnumerator ShowPlayOrKeppPopUp(object i_Param = null);

    public abstract IEnumerator ShowIlegalChoicePopUp(object i_Param = null);


}


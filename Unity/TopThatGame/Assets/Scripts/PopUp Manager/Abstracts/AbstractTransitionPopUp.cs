﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts
{

    using System.Collections;
    using UnityEngine;

    public abstract class AbstractTransitionPopUp : AbstractPopUpScreen
    {
        public TextMesh mr_Header;

        public TextMesh mr_Body;

        public TextMesh mr_RetryButtonText;

        public TextMesh mr_NextButtonText;

        public override IEnumerator Show()
        {
            yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1,0.5F));
        }
        public override IEnumerator Hide()
        {
            yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 1, 0, 0.5F));
        }

    }
}

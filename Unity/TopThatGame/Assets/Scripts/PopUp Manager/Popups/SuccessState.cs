﻿using UnityEngine;
using System.Collections;

public class SuccessState
{
    public BoardController.ePlayer Winner { get; set; }
    public bool HasLoserResigned { get; set; }


    public SuccessState()
    {

    }

    public SuccessState(BoardController.ePlayer i_Winner, bool i_HasLoserResigned)
    {
        this.Winner = i_Winner;
        this.HasLoserResigned = i_HasLoserResigned;
    }
}

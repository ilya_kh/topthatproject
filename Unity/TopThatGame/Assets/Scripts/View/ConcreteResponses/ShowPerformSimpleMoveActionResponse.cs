﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.View.ConcreteResponses
{
    public class ShowPerformSimpleMoveActionResponse : ActionResponseAbstract
    {
        public readonly TopThat.Models.PlayMove PlayMove;

        public readonly bool OnlyOneMovement;

        public ShowPerformSimpleMoveActionResponse(TopThat.Models.PlayMove i_PlayMove, bool i_OnlyOneMovement)
        {
            this.PlayMove = i_PlayMove;
            this.OnlyOneMovement = i_OnlyOneMovement;
        }
    }
}

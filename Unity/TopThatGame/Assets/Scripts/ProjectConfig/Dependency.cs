﻿public class Dependency  {

    public string Name { get; set; }

    public string Source { get; set; }

    public string Destination { get; set; }

}

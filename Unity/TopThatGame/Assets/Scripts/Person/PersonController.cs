﻿using UnityEngine;
using System.Collections;

public class PersonController : MonoBehaviour
{


    public enum ePivot
    {
        Left,

        Right
    }

    public BoardController.ePlayer mr_Player;

    public int ColumnId;

    public int CellId;

    private bool m_IsFirstAppearance;

    public tk2dSprite mr_Sprite;

    public int mr_PlayerId;



    public void SetPlayer(BoardController.ePlayer i_Player , int i_ColumnId , int i_CellId , PersonsController.Vector3Serializer i_Vector3)
    {
        if (this.mr_Player != BoardController.ePlayer.None)
        {
            this.m_IsFirstAppearance = false;
        }

        this.mr_Player = i_Player;
        this.ColumnId = i_ColumnId;
        this.CellId = i_CellId;
        this.StartCoroutine(this.setSquareontent(i_Vector3));
    }

    public void Reset()
    {
        this.CellId = 0;
        this.ColumnId = 0;
        this.mr_Player = BoardController.ePlayer.None;
        this.transform.position = new Vector3(9000,9000,9000);
        this.m_IsFirstAppearance = true;
    }


    private IEnumerator setSquareontent(PersonsController.Vector3Serializer i_Vector3)
    {

        if ((this.ColumnId + this.CellId) % 2 != 0)
        {
            this.setPlayer(ePivot.Right);
        }
        else
        {
            this.setPlayer(ePivot.Left);
        }
        yield return new WaitForSeconds(0.2F);
        this.gameObject.transform.position = new Vector3(i_Vector3.x,i_Vector3.y,i_Vector3.z);
        if (this.m_IsFirstAppearance)
        {
            
            yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0.3F));
        }
        yield break;
    }


    private void setPlayer(ePivot i_Pivot)
    {
        if (this.mr_Player == BoardController.ePlayer.Red)
        {
            this.mr_Sprite.SetSprite("player" + this.mr_PlayerId + "_" + i_Pivot.ToString().ToLower() + "_" + "red");
        }
        else if (this.mr_Player == BoardController.ePlayer.Purple)
        {
            this.mr_Sprite.SetSprite("player" + this.mr_PlayerId + "_" + i_Pivot.ToString().ToLower() + "_" + "purple");
        }
    }
}

﻿using System;
using System.Collections.Generic;

using Assets.Scripts.PopUp_Manager.Abstracts;

using UnityEngine;
using System.Collections;

public class RulesPopup : AbstractPopUpScreen
{
    //Events
    public delegate void CloseButtonClickedDelegate();
    public event CloseButtonClickedDelegate CloseButtonWasClicked;

    //Constants
    private readonly Vector3 k_preAnimationPreviousRuleButtonPosition = new Vector3(-140, -400, 0);
    private readonly Vector3 k_preAnimationNextRuleButtonPosition = new Vector3(1335, -400, 0);

    #region Static Fields
    #endregion

    #region Fields

    public GameObject mr_NextRuleButton;
    public GameObject mr_PreviousRuleButton;

    public GameObject mr_ButtonsContainer;
    public GameObject mr_RuleScreensContainer;
    public GameObject mr_CenterOfScreen;

    public float centerOffset;

    public int numOfRules;

    public List<Transform> rules;

    public float rulesOffset;

    public RulesTexts mr_TextsController;

    private int currentTransformId;

    private Vector3 draggingStartPosition;

    private List<Vector3> finalPositions = new List<Vector3>();

    private List<Vector3> origPositions = new List<Vector3>();


    public MLTextMesh mr_RulesNumber;

    #endregion

    #region Enums

    /// <summary>
    /// The e_ slide direction.
    /// </summary>
    public enum e_SlideDirection
    {
        /// <summary>
        /// The left.
        /// </summary>
        Left,

        /// <summary>
        /// The right.
        /// </summary>
        Right,

        /// <summary>
        /// The center.
        /// </summary>
        Center
    }

    #endregion

    #region Public Properties

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// The next rule.
    /// </summary>
    public void NextRule()
    {
        this.StartCoroutine(this.nextRule());
    }

    /// <summary>
    /// The previous rule.
    /// </summary>
    public void PreviousRule()
    {
        this.StartCoroutine(this.previousRule());
    }

    /// <summary>
    /// The replace last rule.
    /// </summary>
    /// <param name="dir">
    /// The dir.
    /// </param>
    public void ReplaceLastRule(int dir)
    {
        if (dir == 0)
        {
            return;
        }

        int chosenRuleIndex;
        Vector3 minVec;
        Vector3 maxVec;

        chosenRuleIndex = 0;
        minVec = this.rules[0].localPosition;
        maxVec = this.rules[0].localPosition;

        for (int i = 1; i < this.numOfRules; i++)
        {
            if (this.rules[i].transform.localPosition.x < minVec.x)
            {
                minVec = this.rules[i].transform.localPosition;
                if (dir == -1)
                {
                    chosenRuleIndex = i;
                }

                continue;
            }

            if (this.rules[i].transform.localPosition.x > maxVec.x)
            {
                if (dir == 1)
                {
                    chosenRuleIndex = i;
                }

                maxVec = this.rules[i].transform.localPosition;
            }
        }

        if (dir == -1)
        {
            this.rules[chosenRuleIndex].transform.localPosition = maxVec + new Vector3(this.rulesOffset, 0, 0);
        }
        else
        {
            this.rules[chosenRuleIndex].transform.localPosition = minVec - new Vector3(this.rulesOffset, 0, 0);
        }
    }

    /*
     * Called by external class : Model.  
     */

    /// <summary>
    /// The reset rules positions.
    /// </summary>
    public void ResetRulesPositions()
    {
        int originalXPos = 0;
        int originalYPos = 0;
        int middle = this.numOfRules / 2;
        int mod = this.numOfRules % 2;

        if (mod == 1)
        {
            for (int i = 1; i < middle + 1; i++)
            {
                this.rules[i].localPosition = new Vector3(this.rulesOffset * i + originalXPos, originalYPos, 0);
                this.rules[this.numOfRules - i].localPosition = new Vector3(-1 * this.rulesOffset * i + originalXPos, originalYPos, 0);
            }

            this.rules[0].localPosition = new Vector3(0, 0, 0);
        }
        else
        {
            for (int i = 0; i < middle; i++)
            {
                this.rules[i].localPosition = new Vector3(this.rulesOffset * i + originalXPos, originalYPos, 0);
                this.rules[this.numOfRules - i - 1].localPosition = new Vector3(-1 * this.rulesOffset * (i + 1) + originalXPos, originalYPos, 0);
            }
        }

        this.currentTransformId = 0;
    }

    /// <summary>
    /// The slide animation.
    /// </summary>
    /// <param name="direction">
    /// The direction.
    /// </param>
    /// <returns>
    /// The <see cref="IEnumerator"/>.
    /// </returns>
    public IEnumerator SlideAnimation(e_SlideDirection direction)
    {

        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.FadeInOut, SoundManagerStaticInfo.eTags.Effects, false);


        // GameControl.Instance.lockAnimMutex = 1;
        int dir;
        if (direction == e_SlideDirection.Left)
        {
            dir = -1;
        }
        else if (direction == e_SlideDirection.Right)
        {
            dir = 1;
        }
        else
        {
            // direction is Center 
            dir = 0;
        }
        int originalXPos = 0;
        float startTime = Time.time;
        float duration = 0.6F;
        Vector3 movementVector = new Vector3(this.rulesOffset * dir - this.rules[this.currentTransformId].localPosition.x + originalXPos, 0, 0);

        for (int i = 0; i < this.numOfRules; i++)
        {
            this.origPositions[i] = this.rules[i].localPosition;
            this.finalPositions[i] = this.origPositions[i] + movementVector;
        }

        if (this.currentTransformId == 0 && dir == 1)
        {
            this.currentTransformId = this.numOfRules - 1;
        }
        else if (this.currentTransformId == this.numOfRules - 1 && dir == -1)
        {
            this.currentTransformId = 0;
        }
        else
        {
            this.currentTransformId = this.currentTransformId - dir;
        }

        while (Time.time < startTime + duration)
        {
            float prop = (Time.time - startTime) / duration;
            for (int i = 0; i < this.numOfRules; i++)
            {
                this.rules[i].localPosition = this.origPositions[i] + movementVector * prop;
            }

            yield return 0;
        }

        this.mr_RulesNumber.ChangeText((this.currentTransformId+1)+"/19");

        for (int i = 0; i < this.numOfRules; i++)
        {
            this.rules[i].localPosition = this.origPositions[i] + movementVector;
        }

        this.ReplaceLastRule(dir);
    }

    #endregion

    // private List<UIButton> allRulesButtons = new List<UIButton>();
    #region Methods

    /// <summary>
    /// The awake.
    /// </summary>
    private void Awake()
    {
        for (int i = 0; i < this.numOfRules; i++)
        {
            this.origPositions.Add(new Vector3(0, 0, 0));
            this.finalPositions.Add(new Vector3(0, 0, 0));
        }

        this.ResetRulesPositions();
    }

    /// <summary>
    /// The on mouse down.
    /// </summary>
    private void OnMouseDown()
    {
        this.draggingStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    /// <summary>
    /// The on mouse drag.
    /// </summary>
    private void OnMouseDrag()
    {
        Vector3 change = new Vector3((Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.draggingStartPosition).x, 0, 0);
        foreach (Transform t in this.rules)
        {
            t.localPosition = t.localPosition + change;
        }

        this.draggingStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    /// <summary>
    /// The on mouse up.
    /// </summary>
    /// <returns>
    /// The <see cref="IEnumerator"/>.
    /// </returns>
    private IEnumerator OnMouseUp()
    {
        // GameControl.Instance.lockAnimMutex = 1;
        this.draggingStartPosition = new Vector3(0, 0, 0);

        float movementDistance = this.rules[this.currentTransformId].localPosition.x;
        e_SlideDirection direction;

        if (Math.Abs(movementDistance) < 25)
        {
            direction = e_SlideDirection.Center;
        }
        else if (movementDistance < 0)
        {
            direction = e_SlideDirection.Left;
        }
        else
        {
            direction = e_SlideDirection.Right;
        }

        yield return this.StartCoroutine(this.SlideAnimation(direction));
    }

    /**
     * Slide animator for all rules 
     */

    /// <summary>
    /// The next rule.
    /// </summary>
    /// <returns>
    /// The <see cref="IEnumerator"/>.
    /// </returns>
    private IEnumerator nextRule()
    {

        yield return this.StartCoroutine(this.SlideAnimation(e_SlideDirection.Left));
    }

    /// <summary>
    /// The previous rule.
    /// </summary>
    /// <returns>
    /// The <see cref="IEnumerator"/>.
    /// </returns>
    private IEnumerator previousRule()
    {

        yield return this.StartCoroutine(this.SlideAnimation(e_SlideDirection.Right));
    }

    #endregion

    // Update is called once per frame
    public override IEnumerator Show()
    {
       yield return this.StartCoroutine(this.playInAnimation());

        foreach (Transform rule in rules)
        {
            rule.transform.parent = mr_RuleScreensContainer.transform;
        }
    }

    private IEnumerator playInAnimation()
    {
        yield return this.StartCoroutine(this.playInRulesAnimation());
        yield return this.StartCoroutine(this.playButtonsInAnimation());
    }

    private IEnumerator playInRulesAnimation()
    {
        float growDuration = 1;
        float shrinkDuration = 0.25f;

        foreach (Transform rule in rules)
        {
            rule.transform.parent = mr_CenterOfScreen.transform;
        }

        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.TightFabric, SoundManagerStaticInfo.eTags.Effects, false);

        yield return
            this.StartCoroutine(
                GenericAnimator.Instance.LocalScale(
                    mr_CenterOfScreen.gameObject, new Vector3(0, 0, 0), new Vector3(1.2f, 1.2f, 1), growDuration));

        yield return
            this.StartCoroutine(
                GenericAnimator.Instance.LocalScale(
                    mr_CenterOfScreen.gameObject, new Vector3(1.2f, 1.2f, 1), new Vector3(1, 1, 1), shrinkDuration));
    }

    private IEnumerator playButtonsInAnimation()
    {
        float duration = 0.5f;

        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.MiniWhoosh, SoundManagerStaticInfo.eTags.Effects, false);


        Vector3 nextRuleButtonDestinationPosition =
            k_preAnimationNextRuleButtonPosition + new Vector3(-140, 0, 0);
        Vector3 previousRuleButtonDestinationPosition =
            k_preAnimationPreviousRuleButtonPosition + new Vector3(140, 0, 0);


        this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(
                    mr_PreviousRuleButton.gameObject, mr_PreviousRuleButton.transform.localPosition
                    , previousRuleButtonDestinationPosition, duration));

        yield return
            this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(
                    mr_NextRuleButton.gameObject, mr_NextRuleButton.transform.localPosition
                    , nextRuleButtonDestinationPosition, duration));
    }
   
    public override void Reset(object i_Args)
    {
        mr_TextsController.Init();
        mr_PreviousRuleButton.transform.localPosition = k_preAnimationPreviousRuleButtonPosition;
        mr_NextRuleButton.transform.localPosition = k_preAnimationNextRuleButtonPosition;
        
        this.ResetRulesPositions();
    }

    #region Buttons Click Handling

        public void CloseButtonClicked()
        {

            SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Click, SoundManagerStaticInfo.eTags.Effects, false);
            if (CloseButtonWasClicked != null)
            {
                CloseButtonWasClicked();
            }
        }

    #endregion
}

﻿using UnityEngine;
using System.Collections;

public class PeekPair : ViewEventAbstract
{
    public readonly Tuple<MagnetController, MagnetController> FirstTuple;

    public readonly Tuple<MagnetController, MagnetController> SecondTuple;

    public PeekPair(Tuple<MagnetController, MagnetController> i_FirstTuple, Tuple<MagnetController, MagnetController> i_SecondTuple)
    {
        this.FirstTuple = i_FirstTuple;
        this.SecondTuple = i_SecondTuple;
    }
}

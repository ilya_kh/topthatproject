﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class BoardController : MonoBehaviour {


    public enum ePlayer
    {
        None,

        Red, 

        Purple
    }

    public List<ColumnController> mr_ColumnControllers;

    public PersonsController mr_PersonsController;

    private void Awake()
    {
        this.mr_PersonsController.Reset();
    }

    public void Reset()
    {
        this.mr_PersonsController.Reset();
        foreach (var columnController in this.mr_ColumnControllers)
        {
            columnController.Reset();
        }
        
    }


    private void ClearCells()
    {
        foreach (var columnController in this.mr_ColumnControllers)
        {
            columnController.ClearCells();
        }
    }


    public IEnumerator ForteitTurn(ViewGameState i_ViewGameState)
    {
        yield return this.StartCoroutine(this.mr_PersonsController.DropPlayers());
        this.DrawState(i_ViewGameState);
    }

    public void DrawChosenMove(ViewMove i_ViewMove)
    {
        foreach (var column in i_ViewMove.Columns)
        {
            this.drawColumnStatus(column);   
        }

    }

    public bool KeepMove(ViewGameState i_ViewGameState)
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Whoosh, SoundManagerStaticInfo.eTags.Effects, false);
        this.mr_PersonsController.Reset();
        return this.DrawState(i_ViewGameState);
    }


    private void drawColumnStatus(ColumnStatus i_ColumnStatus)
    {
        if (i_ColumnStatus.Status == ColumnStatus.eColumnStatus.ImpossibleClimb)
        {
            i_ColumnStatus.Column.MakeColumnUnAvaliable();
        }
        if (i_ColumnStatus.Status == ColumnStatus.eColumnStatus.PossibleClimb)
        {
            i_ColumnStatus.Column.MakeColumnHighlated();
        }
    }


    public bool DrawState(ViewGameState i_ViewGameState , bool i_ClearPlayers = false)
    {

        if (i_ClearPlayers)
        {
            this.mr_PersonsController.Reset();
        }

        bool isConquaringWillHappen = false;
        this.ClearCells();
        foreach (var redPlayerPlayer in i_ViewGameState.RedPlayerPlayers)
        {
            redPlayerPlayer.ChangeCellVisulalState(ePlayer.Red);
        }


        foreach (var redPlayerPlayer in i_ViewGameState.PurplePlayerPlayers)
        {
            redPlayerPlayer.ChangeCellVisulalState(ePlayer.Purple);
        }

        foreach (var redPlayerMarker in i_ViewGameState.RedPlayerMarkers)
        {
            redPlayerMarker.SetMarkerStatus(true);
        }

        foreach (var redPlayerMarker in i_ViewGameState.PurplePlayerMarkers)
        {
            redPlayerMarker.SetMarkerStatus(true);
        }

        foreach (var columnController in i_ViewGameState.PurpleWonColors)
        {
            if (columnController.IsColumnWillBeConquredFirst())
            {
                isConquaringWillHappen = true;
            }
            columnController.MakeColumnConquared(ePlayer.Purple);   
        }

        foreach (var columnController in i_ViewGameState.RedWonColors)
        {
            if (columnController.IsColumnWillBeConquredFirst())
            {
                isConquaringWillHappen = true;
            }
            columnController.MakeColumnConquared(ePlayer.Red);
        }

        this.mr_PersonsController.SetPlayers(i_ViewGameState.PurplePlayerPlayers);
        this.mr_PersonsController.SetPlayers(i_ViewGameState.RedPlayerPlayers);

        return isConquaringWillHappen;
    }



    public IEnumerator NewWinAnimation(ePlayer i_WonPlayer)
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.WinGame, SoundManagerStaticInfo.eTags.Effects, false);
        this.Reset();
        for (int i = 0; i < this.mr_ColumnControllers.Count/2; i++)
        {
            var leftColumn = this.mr_ColumnControllers.ElementAt(i);
            var rightColum = this.mr_ColumnControllers.ElementAt(this.mr_ColumnControllers.Count - i-1);
            this.StartCoroutine(leftColumn.WinAnimation(i_WonPlayer, 1.4F - 0.1f * i, 0.1f));
            this.StartCoroutine(rightColum.WinAnimation(i_WonPlayer, 1.4F - 0.1f * i, 0.1f));
            yield return new WaitForSeconds(0.05f);
        }
        this.StartCoroutine(this.mr_ColumnControllers.ElementAt(5).WinAnimation(i_WonPlayer, 0.5F, 0.1f));
        yield return new WaitForSeconds(2.1f);

    }


    public IEnumerator WinAnimation(ePlayer i_WonPlayer)
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.WinGame, SoundManagerStaticInfo.eTags.Effects, false);
        foreach (var columnController in this.mr_ColumnControllers)
        {
            if (columnController.GetConauredPlayer() != i_WonPlayer)
            {
                continue;
            }
            this.StartCoroutine(columnController.WinAnimation(i_WonPlayer));
        }
        yield return new WaitForSeconds(2);
        yield break;
    }
}

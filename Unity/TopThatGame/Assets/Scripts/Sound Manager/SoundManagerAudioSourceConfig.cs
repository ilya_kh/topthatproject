﻿using UnityEngine;
using System.Collections;

public class SoundManagerAudioSourceConfig
{
    public float Pitch { get; set; }

    public float Volume { get; set; }

    public bool IsMuted { get; set; }

    public float VolumeToReturnToAfterMute { get; set; }

    public SoundManagerAudioSourceConfig()
    {
        this.Pitch = 1;
        this.Volume = 1;
        this.IsMuted = false;
        this.VolumeToReturnToAfterMute = this.Volume;
    }
}

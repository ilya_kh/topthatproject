﻿using Assets.Scripts.PopUp_Manager.Abstracts;
using UnityEngine;

public class ResignPopup : AbstractPopUpScreen
{
    //Events
    public delegate void YesButtonClickedDelegate(BoardController.ePlayer i_Player);
    public event YesButtonClickedDelegate YesButtonWasClicked;

    public delegate void NoButtonClickedDelegate();
    public event NoButtonClickedDelegate NoButtonWasClicked;

    public MLTextMesh mr_YesButtonText;
    public MLTextMesh mr_NoButtonText;
    public MLTextMesh mr_BodyText;

    public tk2dSprite mr_ResigningPlayerSprite;

    private const string k_RedGirlspriteName = "red-girl-x325-y384";
    private const string k_PurpleGirlspriteName = "prpl-girl-x325-y384";

    private ResignState m_resignState;


	public override void Reset(object i_ResetObject)
    {
        if (i_ResetObject is ResignState)
        {
            ResignState resignState = i_ResetObject as ResignState;
            this.initFromResignState(resignState);
            this.m_resignState = resignState;
            this.setTexts();
        }
        else
        {
            Debug.LogError("Error: Argument must be of type: 'SuccessState'");
        }
    }

    private void initFromResignState(ResignState i_ResignState)
    {
        if (i_ResignState.Resigner == BoardController.ePlayer.Red)
        {
            mr_ResigningPlayerSprite.SetSprite(k_RedGirlspriteName);
        }
        else
        {
            if (i_ResignState.Resigner == BoardController.ePlayer.Purple)
            {
                mr_ResigningPlayerSprite.SetSprite(k_PurpleGirlspriteName);
            }
        }
    }

    private void setTexts()
    {
        mr_YesButtonText.ChangeText(TextService.GetTextById("3.5.1.3"));
        mr_NoButtonText.ChangeText(TextService.GetTextById("3.5.1.4"));
        mr_BodyText.ChangeText(TextService.GetTextById("3.5.3.1"));
    }

    #region Buttons Click Handling

        public void YesButtonClicked()
        {
            if (YesButtonWasClicked != null)
            {
                YesButtonWasClicked(this.m_resignState.Resigner);
            }
        }

        public void NoButtonClicked()
        {
            if (NoButtonWasClicked != null)
            {
                NoButtonWasClicked();
            }
        }

    #endregion

        public override System.Collections.IEnumerator Hide()
        {
            SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.WhooshOut, SoundManagerStaticInfo.eTags.Effects, false);
            yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 1, 0, 0.3F));
        }

        public override System.Collections.IEnumerator Show()
        {
            this.gameObject.SetActive(true);
            yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1,0));
            
        }
}

using System;
using System.Threading;

using Newtonsoft.Json.Linq;

using SignalR.Client._20.Hubs;

using UnityEngine;
using System.Collections;

public class CommunicationBridgeMultiThreadRunner : MonoBehaviour
{
    Thread thread;
    Mutex mainLoop;
    public HubConnection _conn;
    public IHubProxy _proxy;
    private string m_host = "";
    private string m_sessionID = "";


    public delegate void GameMessage(string i_Message);
    public event GameMessage GameMessageEvent;

    public void StartListeningToDigitalClassServer()
    {
        mainLoop = new Mutex(true);
        thread = new Thread(ThreadWorker);
        thread.Start();
    }


    public void InitWorker(string i_Host, string i_SessionId)
    {
        Debug.Log("Init...");
        m_host = i_Host;
        m_sessionID = i_SessionId;
    }

    void OnApplicationQuit()
    {
        if (thread != null)
        {
            thread.Abort();
        }
    }

    void ThreadWorker()
    {
        //Catch and report any exceptions here, 
        //so that Unity doesn't crash!
        try
        {
            _ThreadWorker();
        }
        catch (Exception e)
        {
            if (!(e is ThreadAbortException))
                Debug.LogError("Unexpected Death: " + e.ToString());
        }
    }

    void _ThreadWorker()
    {
        InitAndroidListener();
        StartAndroidListener();
    }

    #region Android
    public void StartAndroidListener()
    {
        _proxy.Invoke("JoinFromGame", m_sessionID);
    }

    public void InitAndroidListener()
    {

        Debug.Log("Host in Worker: " + m_host);
        Debug.Log("Port in Worker: " + m_sessionID);

        _conn = new HubConnection(m_host);
        _proxy = _conn.CreateProxy("digitalClass");

        _proxy.Subscribe("invokeCommand").Data += data =>
        {
            if (data == null || data.Length == 0)
                return;

            JToken _first = data[0] as JToken;

            if (_first["command"].ToString() == "PauseActivity")
            {
                Debug.Log("Pause Game");
                if (GameMessageEvent != null)
                {
                    GameMessageEvent("pause");
                } 
            }
                

            if (_first["command"].ToString() == "ResumeActivity")
            {
                Debug.Log("Resume Game");
                if (GameMessageEvent != null)
                {
                    GameMessageEvent("resume");
                }
            }
                

            if (_first["command"].ToString() == "ClosePresentation")
            {
                Debug.Log("Close Game");
                if (GameMessageEvent != null)
                {
                    GameMessageEvent("quit");
                }
            }
        };
       
        _conn.Start();

        Debug.Log("Session with server started!!!");
    }
    #endregion
}

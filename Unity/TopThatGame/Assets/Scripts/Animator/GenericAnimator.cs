using System.Collections.Generic;

using UnityEngine;
using System.Collections;

public class GenericAnimator : MonoBehaviour
{
    public static GenericAnimator Instance;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    /// <summary>
    /// Generic Fade Animator.
    /// Is an IEnumerator Function.
    /// Is a recursive function(Works on all children components of the given GameObject).
    /// </summary>
    /// <param name="i_GameObject">The GameObjec to fade.</param>
    /// <param name="i_OriginAlpha">The origin alpha of the GameObject to fade.</param>
    /// <param name="i_DestinationAlpha">The destination alpha of the GameObject to fade.</param>
    /// <param name="i_AnimationDuration">Duration of animation.</param>
    /// <returns></returns>
	public IEnumerator Fade(GameObject i_GameObject,float i_OriginAlpha,float i_DestinationAlpha,float i_AnimationDuration)
	{
        SpriteRenderer[] spritesToAnimate;
        TextMesh[] textMeshesToAnimate;


        SpriteBase[] ezGuiObjectsToAnimate;
        SpriteText[] ezGuiTextsToAnimate;
        tk2dSprite[] tk2dSpritesToAnimate;


        ezGuiObjectsToAnimate = i_GameObject.GetComponentsInChildren<SpriteBase>(true);
        tk2dSpritesToAnimate = i_GameObject.GetComponentsInChildren<tk2dSprite>(true);
        ezGuiTextsToAnimate = i_GameObject.GetComponentsInChildren<SpriteText>(true);


        spritesToAnimate = i_GameObject.GetComponentsInChildren<SpriteRenderer>(true);
        textMeshesToAnimate = i_GameObject.GetComponentsInChildren<TextMesh>(true);

        float startTime = Time.time;
        float endTime = startTime + i_AnimationDuration;

        //Must be a number between 0 and 1
        float interpolationFactor = 0;

        while (Time.time < endTime)
        {
            foreach (TextMesh textMesh in textMeshesToAnimate)
            {
                textMesh.renderer.material.color = Color.Lerp(
                    new Color(textMesh.renderer.material.color.r,
                        textMesh.renderer.material.color.g,
                        textMesh.renderer.material.color.b,
                        i_OriginAlpha),
                    new Color(textMesh.renderer.material.color.r,
                        textMesh.renderer.material.color.g,
                        textMesh.renderer.material.color.b,
                        i_DestinationAlpha),
                    interpolationFactor / i_AnimationDuration);
            }

            foreach (SpriteRenderer sprite in spritesToAnimate)
            {
                sprite.color = Color.Lerp(new Color(1, 1, 1, i_OriginAlpha), new Color(1, 1, 1, i_DestinationAlpha), interpolationFactor / i_AnimationDuration);
            }



            foreach (tk2dSprite tk2dSprite in tk2dSpritesToAnimate)
            {
                tk2dSprite.color = Color.Lerp(new Color(1, 1, 1, i_OriginAlpha), new Color(1, 1, 1, i_DestinationAlpha), interpolationFactor / i_AnimationDuration);
            }

            foreach (SpriteBase ezGuiObject in ezGuiObjectsToAnimate)
            {
                

                ezGuiObject.SetColor(Color.Lerp(new Color(1, 1, 1, i_OriginAlpha), new Color(1, 1, 1, i_DestinationAlpha), interpolationFactor / i_AnimationDuration));
            }

            foreach (SpriteText ezGuiText in ezGuiTextsToAnimate)
            {
                ezGuiText.SetColor(Color.Lerp(new Color(1, 1, 1, i_OriginAlpha), new Color(1, 1, 1, i_DestinationAlpha), interpolationFactor / i_AnimationDuration));
            }

            interpolationFactor = Time.time - startTime;
            yield return 0;
        }

        //Adjust to final value

        foreach (TextMesh textMesh in textMeshesToAnimate)
        {
            textMesh.renderer.material.color = new Color(textMesh.renderer.material.color.r,
                        textMesh.renderer.material.color.g,
                        textMesh.renderer.material.color.b,
                        i_DestinationAlpha);
        }

        foreach (var tk2DSprite in tk2dSpritesToAnimate)
        {
            tk2DSprite.color = new Color(1,1,1,i_DestinationAlpha);
        }

        foreach (var spriteBase in ezGuiObjectsToAnimate)
        {
            spriteBase.SetColor(new Color(1,1,1,i_DestinationAlpha));
        }
        

        foreach (SpriteRenderer sprite in spritesToAnimate)
        {
           sprite.color = new Color(1, 1, 1, i_DestinationAlpha);
        }
	}


    public IEnumerator Glow(GameObject i_GameObject, float i_GlowInDuration, float i_GlowOutDuration, float i_GlowBackDuration)
    {

        yield return
            this.StartCoroutine(this.LocalScale(i_GameObject, Vector3.one, new Vector3(1.5f, 1.5f, 1), i_GlowInDuration));
        yield return
    this.StartCoroutine(this.LocalScale(i_GameObject, new Vector3(1.5f, 1.5f, 1), new Vector3(0.7f, 0.7f, 1), i_GlowOutDuration));
        yield return
            this.StartCoroutine(this.LocalScale(i_GameObject, new Vector3(0.7f, 0.7f, 1), Vector3.one, i_GlowBackDuration));

        yield break;
    }

    /// <summary>
    /// Generic Move Animator.
    /// Is an IEnumerator Function.
    /// </summary>
    /// <param name="i_GameObject">The GameObject to move.</param>
    /// <param name="i_OriginPosition">The origin position of the GameObject to move.</param>
    /// <param name="i_DestinationPosition">The destination position of the GameObject to move.</param>
    /// <param name="i_AnimationDuration">Duration of animation.</param>
    /// <returns></returns>
    public IEnumerator Move(GameObject i_GameObject,
        Vector3 i_OriginPosition,
        Vector3 i_DestinationPosition,
        float i_AnimationDuration)
    {
        float startTime = Time.time;
        float endTime = startTime + i_AnimationDuration;

        //Must be a number between 0 and 1
        float interpolationFactor = 0;

        while (Time.time < endTime)
        {
            i_GameObject.transform.position = Vector3.Lerp(i_OriginPosition, i_DestinationPosition, interpolationFactor / i_AnimationDuration);
            interpolationFactor = Time.time - startTime;
            yield return 0;
        }

        //Adjust to final value
        i_GameObject.transform.position = i_DestinationPosition;
    }

    /// <summary>
    /// Generic Move Animator.
    /// Is an IEnumerator Function.
    /// </summary>
    /// <param name="i_GameObject">The GameObject to move.</param>
    /// <param name="i_OriginPosition">The origin position of the GameObject to move.</param>
    /// <param name="i_DestinationPosition">The destination position of the GameObject to move.</param>
    /// <param name="i_AnimationDuration">Duration of animation.</param>
    /// <returns></returns>
    public IEnumerator MoveLocal(GameObject i_GameObject,
        Vector3 i_OriginPosition,
        Vector3 i_DestinationPosition,
        float i_AnimationDuration)
    {
        float startTime = Time.time;
        float endTime = startTime + i_AnimationDuration;

        //Must be a number between 0 and 1
        float interpolationFactor = 0;

        while (Time.time < endTime)
        {
            i_GameObject.transform.localPosition = Vector3.Lerp(i_OriginPosition, i_DestinationPosition, interpolationFactor / i_AnimationDuration);
            interpolationFactor = Time.time - startTime;
            yield return 0;
        }

        //Adjust to final value
        i_GameObject.transform.localPosition = i_DestinationPosition;
    }

    /// <summary>
    /// Generic Scale Animator.
    /// Is an IEnumerator Method.
    /// </summary>
    /// <param name="i_GameObject">GameObject to scale.</param>
    /// <param name="i_OriginScale">The origin vector of the GameObject to scale.</param>
    /// <param name="i_DestinationScale">The destination vector of the GameObject to scale.</param>
    /// <param name="i_AnimationDuration">Duration of animation.</param>
    /// <returns></returns>
    public IEnumerator LocalScale(GameObject i_GameObject,
        Vector3 i_OriginScale,
        Vector3 i_DestinationScale,
        float i_AnimationDuration)
    {
        float startTime = Time.time;
        float endTime = startTime + i_AnimationDuration;

        //Must be a number between 0 and 1
        float interpolationFactor = 0;

        while (Time.time < endTime)
        {
            i_GameObject.transform.localScale = Vector3.Lerp(i_OriginScale, i_DestinationScale, interpolationFactor / i_AnimationDuration);
            interpolationFactor = Time.time - startTime;
            yield return 0;
        }

        //Adjust to final value
        i_GameObject.transform.localScale = i_DestinationScale;
    }

    /// <summary>
    /// Generic Blink Animator.
    /// Is an IEnumerator Function.
    /// Is a recursive function(Works on all children components of the given GameObject).
    /// </summary>
    /// <param name="i_GameObject">GameObject to blink.</param>
    /// <param name="i_NumOfBlinks">Number of blinks to be execute by the GameObject.</param>
    /// <param name="i_AnimationDuration">Duration of animation.</param>
    /// <returns></returns>
    public IEnumerator Blink(GameObject i_GameObject,
        int i_NumOfBlinks, float i_AnimationDuration)
    {
        float timeToWaitBetweenBlinks = i_AnimationDuration / i_NumOfBlinks;

        SpriteRenderer[]spritesToAnimate = i_GameObject.GetComponentsInChildren<SpriteRenderer>(true);

        for (int i = 0; i < i_NumOfBlinks; i++)
        {
            yield return new WaitForSeconds(timeToWaitBetweenBlinks / 2);

            foreach (SpriteRenderer sprite in spritesToAnimate)
            {
                sprite.gameObject.renderer.enabled = false;
            }

            yield return new WaitForSeconds(timeToWaitBetweenBlinks / 2);

            foreach (SpriteRenderer sprite in spritesToAnimate)
            {
                sprite.gameObject.renderer.enabled = true;
            }
        }
    }

    /// <summary>
    /// Generic Rotate Animator.
    /// Is an IEnumerator Function.
    /// </summary>
    /// <param name="i_GameObject">The GameObject to rotate.</param>
    /// <param name="i_OriginRotation">The original rotation vector of the GameObject to rotate.</param>
    /// <param name="i_DestinationRotation">The destination rotation vector of the GameObject to rotate.</param>
    /// <param name="i_AnimationDuration">Duration of the animation.</param>
    /// <returns></returns>
    public IEnumerator Rotate(GameObject i_GameObject,
        Vector3 i_OriginRotation,
        Vector3 i_DestinationRotation,
        float i_AnimationDuration)
    {
        float startTime = Time.time;
        float endTime = startTime + i_AnimationDuration;

        //Must be a number between 0 and 1
        float interpolationFactor = 0;

//        Debug.Log("start time: " + startTime);
//        Debug.Log("End time: " + endTime);
        

        while (Time.time < endTime)
        {
            i_GameObject.transform.eulerAngles = Vector3.Lerp(i_OriginRotation, i_DestinationRotation, interpolationFactor / i_AnimationDuration);
            interpolationFactor = Time.time - startTime;
            yield return 0;
        }

        //Adjust to final value
        i_GameObject.transform.eulerAngles = i_DestinationRotation;
    }

    public IEnumerator RotateLocal(GameObject i_GameObject,
        Vector3 i_OriginRotation,
        Vector3 i_DestinationRotation,
        float i_AnimationDuration)
    {
        float startTime = Time.time;
        float endTime = startTime + i_AnimationDuration;

        //Must be a number between 0 and 1
        float interpolationFactor = 0;

        //        Debug.Log("start time: " + startTime);
        //        Debug.Log("End time: " + endTime);


        while (Time.time < endTime)
        {
            i_GameObject.transform.localEulerAngles = Vector3.Lerp(i_OriginRotation, i_DestinationRotation, interpolationFactor / i_AnimationDuration);
            interpolationFactor = Time.time - startTime;
            yield return 0;
        }

        //Adjust to final value
        i_GameObject.transform.localEulerAngles = i_DestinationRotation;
    }

    public static IEnumerator FadePulse(GameObject i_GameObject, int i_NumberOfPulses, float i_PulseDuration)
    {
        for (int i = 0; i < i_NumberOfPulses; i++)
        {
            yield return Instance.StartCoroutine(Instance.Fade(i_GameObject, 1, 0, i_PulseDuration / 2));
            yield return Instance.StartCoroutine(Instance.Fade(i_GameObject, 0, 1, i_PulseDuration / 2));
        }
    }


    public static IEnumerator Bounce(GameObject i_GameObject, int i_NumberOfBounces, float i_YTargetScale)
    {
        for (int i = 0; i < i_NumberOfBounces; i++)
        {
            yield return Instance.StartCoroutine(Instance.LocalScale(i_GameObject, Vector3.one, new Vector3(1, i_YTargetScale, 1), 1));
            yield return Instance.StartCoroutine(Instance.LocalScale(i_GameObject, new Vector3(1, i_YTargetScale, 1), Vector3.one, 1));
        }
    }

    public static IEnumerator BounceUp(GameObject i_GameObject, int i_NumberOfBounces, float i_YTargetJump)
    {
        Vector3 originalPosition = i_GameObject.transform.localPosition;
        for (int i = 0; i < i_NumberOfBounces; i++)
        {
            yield return Instance.StartCoroutine(Instance.MoveLocal(i_GameObject, originalPosition, new Vector3(1, i_YTargetJump, 1), 0.5f));
            yield return Instance.StartCoroutine(Instance.MoveLocal(i_GameObject, new Vector3(1, i_YTargetJump, 1), originalPosition, 0.5f));
        }
    }

}

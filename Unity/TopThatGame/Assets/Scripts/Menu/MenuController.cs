﻿using Assets.Scripts.PopUp_Manager.Interfaces;

using UnityEngine;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;

public class MenuController : MonoBehaviour
{

    public GameObject mr_Pause;

    public UIButton mr_MenuButton;

    public UIButton mr_ExitButton;

    public UIButton mr_HomeButton;

    public UIButton mr_RulesButton;

    public UIStateToggleBtn mr_Sound;


    private bool m_isMenuOpened = false;

    private IPopUpManager m_PopUpManager;

    public IEnumerator Init(IPopUpManager i_PopUpManager)
    {
        this.m_PopUpManager = i_PopUpManager;
        this.SetInputListeners();
        this.GetComponentsInChildren<TooltipController>(true).ToList().ForEach(m => m.Init());
        this.DisableButtons();
        yield break;
    }


    private IEnumerator shakeAnimation()
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.mr_MenuButton.gameObject, this.mr_MenuButton.transform.localPosition, this.mr_MenuButton.transform.localPosition + new Vector3(10, 0, 0), 0.05F));
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.mr_MenuButton.gameObject, this.mr_MenuButton.transform.localPosition, this.mr_MenuButton.transform.localPosition + new Vector3(-10, 0, 0), 0.05F));
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.mr_MenuButton.gameObject, this.mr_MenuButton.transform.localPosition, this.mr_MenuButton.transform.localPosition + new Vector3(-10, 0, 0), 0.05F));
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.mr_MenuButton.gameObject, this.mr_MenuButton.transform.localPosition, this.mr_MenuButton.transform.localPosition + new Vector3(10, 0, 0), 0.05F));
    }

    private void DisableButtons()
    {
        this.mr_HomeButton.gameObject.SetActive(false);
        this.mr_ExitButton.gameObject.SetActive(false);
        this.mr_RulesButton.gameObject.SetActive(false);
        this.mr_Sound.gameObject.SetActive(false);
    }

    private void EnaleButtons()
    {
        this.mr_HomeButton.gameObject.SetActive(true);
        this.mr_ExitButton.gameObject.SetActive(true);
        this.mr_RulesButton.gameObject.SetActive(true);
        this.mr_Sound.gameObject.SetActive(true);
    }

    private IEnumerator InAnimation()
    {
        this.mr_Pause.SetActive(true);
        this.StartCoroutine(this.shakeAnimation());
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.MenuButtons, SoundManagerStaticInfo.eTags.Effects, false);
        yield return this.StartCoroutine(this.SingleInAnimation(this.mr_ExitButton.gameObject));
        yield return this.StartCoroutine(this.SingleInAnimation(this.mr_HomeButton.gameObject));
        yield return this.StartCoroutine(this.SingleInAnimation(this.mr_RulesButton.gameObject));
        yield return this.StartCoroutine(this.SingleInAnimation(this.mr_Sound.gameObject));
        this.mr_Pause.SetActive(false);

    }

    private IEnumerator OutAnimation()
    {
        this.mr_Pause.SetActive(true);
        yield return this.StartCoroutine(this.SingleOutAnimation(this.mr_Sound.gameObject));
        yield return this.StartCoroutine(this.SingleOutAnimation(this.mr_RulesButton.gameObject));
        yield return this.StartCoroutine(this.SingleOutAnimation(this.mr_HomeButton.gameObject));
        yield return this.StartCoroutine(this.SingleOutAnimation(this.mr_ExitButton.gameObject));
        this.mr_Pause.SetActive(false);

    }

    private IEnumerator SingleInAnimation(GameObject i_GameObject)
    {
        i_GameObject.SetActive(true);
        yield return this.StartCoroutine(GenericAnimator.Instance.LocalScale(i_GameObject, new Vector3(0, 0, 0), new Vector3(1.2F, 1.2F, 1), 0.075f));
        yield return this.StartCoroutine(GenericAnimator.Instance.LocalScale(i_GameObject, new Vector3(1.2F, 1.2F, 1), Vector3.one, 0.025f));
    }

    private IEnumerator SingleOutAnimation(GameObject i_GameObject)
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.LocalScale(i_GameObject, Vector3.one, new Vector3(0, 0, 0), 0.3f));
        i_GameObject.SetActive(false);
    }


    #region Methods

    public void MenuButtonPressed()
    {
        if (this.m_isMenuOpened)
        {
            this.m_isMenuOpened = false;
            this.StartCoroutine(this.OutAnimation());
        }
        else
        {
            this.m_isMenuOpened = true;
            this.StartCoroutine(this.InAnimation());
        }
    }


    public void ExitButtonPressed()
    {
        this.StartCoroutine(this.m_PopUpManager.Show(PopUpManager.ePopUpType.Exit));
    }

    public void HomeButtonPressed()
    {
        this.StartCoroutine(this.m_PopUpManager.Show(PopUpManager.ePopUpType.BackToMain));
    }

    public void RulesButtonPressed()
    {
        this.StartCoroutine(this.m_PopUpManager.Show(PopUpManager.ePopUpType.Rules));
    }

    public void SoundButtonPressed()
    {
        if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Effects))
        {
            SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Effects, true);
        }
        else
        {
            SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Effects, false);
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Effects))
            {
                this.mr_Sound.SetToggleState("Normal_On");
            }
            else
            {
                this.mr_Sound.SetToggleState("Normal_Off");
            }
        }
    }

    #endregion


    #region Mouseover 


    private void SetInputListeners()
    {
        if (Application.platform != RuntimePlatform.Android)
        {
            this.mr_Sound.AddInputDelegate(this.onMouseOverSound);
        }
        this.mr_HomeButton.AddInputDelegate(this.onMouseOverButtonBackToMain);
        this.mr_ExitButton.AddInputDelegate(this.onMouseOverButtonExit);
        this.mr_RulesButton.AddInputDelegate(this.onMouseOverButtonRules);
    }

    private void onMouseOverButtonExit(ref POINTER_INFO ptrm)
    {
        if (ptrm.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_ExitButton.GetComponentInChildren<TooltipController>().OnShow();
        }
        else if (IsTooltipShoulBeHidden(ptrm))
        {
            this.mr_ExitButton.GetComponentInChildren<TooltipController>().OnHide();
        }
    }


    private void onMouseOverButtonRules(ref POINTER_INFO ptrm)
    {
        if (ptrm.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_RulesButton.GetComponentInChildren<TooltipController>().OnShow();
        }
        else if (IsTooltipShoulBeHidden(ptrm))
        {
            this.mr_RulesButton.GetComponentInChildren<TooltipController>().OnHide();
        }
    }


    private void onMouseOverButtonBackToMain(ref POINTER_INFO ptrm)
    {
        if (ptrm.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_HomeButton.GetComponentInChildren<TooltipController>().OnShow();
        }
        else if (IsTooltipShoulBeHidden(ptrm))
        {
            this.mr_HomeButton.GetComponentInChildren<TooltipController>().OnHide();
        }
    }

    public static bool IsTooltipShoulBeHidden(POINTER_INFO ptrm)
    {
        return ptrm.evt != POINTER_INFO.INPUT_EVENT.MOVE;
    }

    private void onMouseOverButtonSound(ref POINTER_INFO ptrm)
    {
        if (ptrm.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnShow();
        }
        else if (IsTooltipShoulBeHidden(ptrm))
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnHide();
        }
    }

    private void onMouseOverSound(ref POINTER_INFO ptr)
    {
        if (this.mr_Sound.ToggleState() == 4)
        {
            return;
        }

        
        if (this.handleAndroidInput(ptr))
        {
            return;
        }

        this.handleWebInout(ptr);

    }

    private void handleWebInout(POINTER_INFO ptr)
    {
        if (ptr.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnShow();

            if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Effects))
            {
                this.mr_Sound.SetToggleState("Over_On");
            }
            else
            {
                this.mr_Sound.SetToggleState("Over_Off");
            }
        }
        else if (ptr.evt != POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnHide();

            if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Effects))
            {
                this.mr_Sound.SetToggleState("Normal_On");
            }
            else
            {
                this.mr_Sound.SetToggleState("Normal_Off");
            }
        }
    }

    private bool handleAndroidInput(POINTER_INFO ptr)
    {
        
        if (Application.platform == RuntimePlatform.Android)
        {
            return true;
            if (ptr.evt == POINTER_INFO.INPUT_EVENT.PRESS)
            {
                if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Effects))
                {
                    this.mr_Sound.SetToggleState("Normal_On");
                }
                else
                {
                    this.mr_Sound.SetToggleState("Normal_Off");
                }
            }
            return true;
        }
        return false;
    }

    #endregion 

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.PopUp_Manager.Abstracts
{
    using System.Collections;

    public abstract class AbstractPopUpScreen : MonoBehaviour
    {

        public PopUpManager.ePopUpType m_PopUpType;

        public abstract void Reset(object i_Args);

        public virtual IEnumerator Show()
        {
            this.gameObject.SetActive(true);
            yield break;
        }

        public virtual IEnumerator Hide()
        {
            this.gameObject.SetActive(false);
            yield break;
        }

        public virtual void OnRestart()
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MagnetsController : MonoBehaviour
{

    #region Events

    public delegate void MagnetsChosen(ViewEventAbstract i_ViewEvent);

    public event MagnetsChosen MagnetsChosenEvent;


    public delegate void MagnetsDisposed(ViewEventAbstract i_ViewEvent);

    public event MagnetsDisposed MagnetsDisposedEvent;


    public delegate void MagnetIsDragged();

    public event MagnetIsDragged MagnetDraggedEvent;

    #endregion 

    public List<MagnetController> mr_MagnetControllers;

    private List<Vector3> m_startPositions = new List<Vector3>();

    private Dictionary<MagnetController,MagnetController> m_mappedMagnets = new Dictionary<MagnetController, MagnetController>();

    private List<Vector3> m_attachedListRed = new List<Vector3>()
                                                  {
                                                      new Vector3(-400, 482, -20),
                                                      new Vector3(-435, 482, -20),
                                                      new Vector3(-570, 482, -20),
                                                      new Vector3(-605, 482, -20)
                                                  };

    public BoardController.ePlayer mr_Player;
    private  Vector3 m_offset = new Vector3(952,0,0);
    private bool m_isSnapOccured;

    private void Awake()
    {
        if (this.mr_Player == BoardController.ePlayer.Red)
        {
            this.m_offset = Vector3.zero;
        }

        foreach (var magnetController in this.mr_MagnetControllers)
        {
            magnetController.RegisterDownEvent += this.magnetControllerOnRegisterDownEvent;
            magnetController.RegisterUpEvent += this.MagnetControllerOnRegisterUpEvent;
            magnetController.RegisterCollisionEvent += this.MagnetControllerOnRegisterCollisionEvent;
            magnetController.RegisterExitEvent += this.MagnetControllerOnRegisterExitEvent;
            magnetController.ResisterDragEvent+= this.MagnetControllerOnResisterDragEvent;
            this.m_startPositions.Add(magnetController.transform.localPosition);
        }
       
    }

    private void MagnetControllerOnResisterDragEvent(MagnetController i_Invoker)
    {
        if (this.m_mappedMagnets.Count == 0)
        {
            this.reorderMagnets(i_Invoker);
        }
    }

    public void DisableMagnets()
    {
        foreach (var magnetController in this.mr_MagnetControllers)
        {
            magnetController.DisableMagnet();
        }
    }

    public void EnableMagnets()
    {
        foreach (var magnetController in this.mr_MagnetControllers)
        {
            magnetController.EnableMagnet();
        }
    }


    public void SetInput(List<int> i_Choice)
    {
        if (i_Choice.Count != this.mr_MagnetControllers.Count)
        {
            throw new Exception(" Bad input for magnets controller");
        }
        for (int i = 0; i < this.mr_MagnetControllers.Count; i++)
        {
            this.mr_MagnetControllers[i].SetOption(i_Choice[i]);
        }
    }

    private void MagnetControllerOnRegisterExitEvent(MagnetController i_Invoker, MagnetController i_Invoked, Detector i_Detector)
    {
        if (!this.m_mappedMagnets.ContainsKey(i_Invoker) && !this.m_mappedMagnets.ContainsValue(i_Invoker))
        {
            return;
        }

        if (i_Detector.mr_Level == 1)
        {
            if (i_Invoker.transform.localPosition.x < i_Invoked.transform.localPosition.x
                && i_Detector.mr_Side == Detector.eSide.Left)
            {
                this.reorderMagnets(i_Invoker);
                this.m_mappedMagnets.Clear();
            }

            if (i_Invoker.transform.localPosition.x > i_Invoked.transform.localPosition.x && i_Detector.mr_Side == Detector.eSide.Right)
            {
                this.reorderMagnets(i_Invoker);
                this.m_mappedMagnets.Clear();
            }
        }
    }

    public void reorderMagnets(MagnetController i_Invoker , bool i_DoNotTouchInvoker = true)
    {
        this.m_mappedMagnets.Clear();
        var orderedMagnets = this.mr_MagnetControllers.OrderBy(m => m.transform.localPosition.x);
        var orderesStartLocations = this.m_startPositions.OrderBy(m => m.x);
        int index = 0;
        foreach (var magnetController in orderedMagnets)
        {
            if (magnetController == i_Invoker && i_DoNotTouchInvoker)
            {
                index++;
            }
            else
            {
                magnetController.transform.localPosition = orderesStartLocations.ElementAt(index);
                index++;
            }
        }
    }

    /// <summary>
    /// This method will clip magnets together. 
    /// </summary>
    private void clipMagnets()
    {


        int baseNum = 0;
        foreach (var pair in this.m_mappedMagnets.OrderByDescending(m => m.Key.transform.localPosition.x))
        {
            pair.Key.transform.localPosition = new Vector3(this.m_attachedListRed[baseNum + 1].x, this.m_attachedListRed[baseNum + 1].y, this.m_attachedListRed[baseNum + 1].z) + this.m_offset;
            pair.Value.transform.localPosition = new Vector3(this.m_attachedListRed[baseNum].x, this.m_attachedListRed[baseNum].y, this.m_attachedListRed[baseNum].z) + this.m_offset;
            baseNum += 2;
        }
    }

    private void MagnetControllerOnRegisterCollisionEvent(MagnetController i_Invoker, MagnetController i_Invoked, Detector i_Detector)
    {
        bool wasConnectedBefore = false;
        if (i_Detector.mr_Level == 2)
        {
            return;
        }
        if (this.m_mappedMagnets.ContainsKey(i_Invoker) || this.m_mappedMagnets.ContainsValue(i_Invoker))
        {
            wasConnectedBefore = true;
            var tuples = this.getMagnetsTuples();
            foreach (var tuple in tuples)
            {
                if (tuple.Item1 == i_Invoker && tuple.Item2 == i_Invoked)
                {
                    return;
                }
                if (tuple.Item2 == i_Invoker && tuple.Item1 == i_Invoked)
                {
                    return;
                }
            }
        }
        if (i_Invoker.transform.localPosition.x < i_Invoked.transform.localPosition.x)
        {
            this.m_mappedMagnets[i_Invoker] = i_Invoked;
        }
        else
        {
            this.m_mappedMagnets[i_Invoked] = i_Invoker;
        }
        if (wasConnectedBefore)
        {
            this.m_mappedMagnets.Clear();
            this.reorderMagnets(i_Invoker);
        }
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.MagnetsIn, SoundManagerStaticInfo.eTags.Effects, false);
        i_Invoker.SetMagnetStatus(MagnetController.eMagnetStatus.Attached);
        int delta = i_Detector.mr_Side == Detector.eSide.Right ? -25 : 25;
        i_Invoked.transform.parent = i_Invoker.transform;
        i_Invoked.transform.localPosition = new Vector3(delta, i_Invoked.transform.localPosition.y, 1);
        i_Invoked.transform.parent = this.gameObject.transform;
    }

    private void MagnetControllerOnRegisterUpEvent(MagnetController i_Invoker , bool i_IsForced = false)
    {
        if (this.m_mappedMagnets.Count == 0)
        {
            this.reorderMagnets(i_Invoker, false);
            if (this.MagnetsDisposedEvent != null && !this.m_isSnapOccured && !i_IsForced)
            {
                this.MagnetsDisposedEvent(new BreakPair());
            }
            this.m_isSnapOccured = i_IsForced;
        }
        else
        {
            this.m_isSnapOccured = i_IsForced;
            if (i_IsForced)
            {
                this.reorderMagnets(i_Invoker, false);
                return;
            }
            this.mapMagnets();
            if (this.MagnetsChosenEvent != null)
            {
                var tuples = this.getMagnetsTuples();
                if (tuples.Count != 2)
                {
                    throw new Exception("Bad match of magnets");
                }
                this.MagnetsChosenEvent(new PeekPair(tuples.ElementAt(0),tuples.ElementAt(1)));     
            }
        }
        this.makeAllInvokers();
    }


    public void ClipMagnets()
    {
        this.clipMagnets();
    }

    private List<Tuple<MagnetController, MagnetController>> getMagnetsTuples()
    {
        List<Tuple<MagnetController,MagnetController>> tuples = new List<Tuple<MagnetController, MagnetController>>();
        var ordered = this.mr_MagnetControllers.OrderBy(m => m.transform.localPosition.x);
        foreach (var pair in ordered)
        {
            if (!this.m_mappedMagnets.ContainsKey(pair))
            {
                continue;
            }
            Tuple<MagnetController,MagnetController> tuple = new Tuple<MagnetController, MagnetController>(pair,this.m_mappedMagnets[pair]);
            tuples.Add(tuple);
        }
        return tuples;
    }

    private void mapMagnets()
    {
        MagnetController first = null, second = null;
        foreach (var magnetController in this.mr_MagnetControllers.OrderBy(m => m.transform.localPosition.x))
        {
            if (!this.m_mappedMagnets.ContainsKey(magnetController) && !this.m_mappedMagnets.ContainsValue(magnetController))
            {
                if (first == null)
                {
                    first = magnetController;
                }
                else if (second == null)
                {
                    second = magnetController;
                }
            }
        }
        if (first != null && second != null)
        {
            this.m_mappedMagnets[first] = second;
        }
    }

    private void magnetControllerOnRegisterDownEvent(MagnetController i_Invoker)
    {
        if (this.MagnetDraggedEvent != null)
        {
            this.MagnetDraggedEvent();
        }
        foreach (var magnetController in this.mr_MagnetControllers)
        {
            magnetController.MakeMagnetInvoked();
        }
        i_Invoker.MakeMagnetInvoker();
    }

    private void makeAllInvokers()
    {
        foreach (var magnetController in this.mr_MagnetControllers)
        {
            magnetController.MakeMagnetInvoker();
        }
    }

    public void DisableIlegalPair(int i_Sum)
    {
        foreach (var pair in this.m_mappedMagnets)
        {
            int sum = pair.Key.GetNumber + pair.Value.GetNumber;
            if (sum != i_Sum)
            {
                pair.Key.DisableMagnet();
                pair.Value.DisableMagnet();
            }
        }
    }

}

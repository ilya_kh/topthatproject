﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ViewGameState 
{
    public readonly BoardController.ePlayer Player;

    public readonly List<Marker> RedPlayerMarkers;

    public readonly List<CellController> RedPlayerPlayers;

    public readonly List<ColumnController> RedWonColors;

    public readonly List<Marker> PurplePlayerMarkers;

    public readonly List<CellController> PurplePlayerPlayers;

    public readonly List<ColumnController> PurpleWonColors; 

    public ViewGameState(
        BoardController.ePlayer i_Player,
        List<Marker> i_RedPlayerMarkers,
        List<CellController> i_RedPlayerPlayers,
        List<Marker> i_PurplePlayerMarkers,
        List<CellController> i_PurplePlayerPlayers,
        List<ColumnController> i_RedWonColors,
        List<ColumnController> i_PurpleWonColors)
    {
        this.Player = i_Player;
        this.RedPlayerMarkers = i_RedPlayerMarkers;
        this.RedPlayerPlayers = i_RedPlayerPlayers;
        this.PurplePlayerMarkers = i_PurplePlayerMarkers;
        this.PurplePlayerPlayers = i_PurplePlayerPlayers;
        this.RedWonColors = i_RedWonColors;
        this.PurpleWonColors = i_PurpleWonColors;
    }
}

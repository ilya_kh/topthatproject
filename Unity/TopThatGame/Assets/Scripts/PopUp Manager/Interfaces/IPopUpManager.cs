﻿namespace Assets.Scripts.PopUp_Manager.Interfaces
{
    using System.Collections;

    public interface IPopUpManager
    {
        IEnumerator Show(PopUpManager.ePopUpType i_PopUpType, object i_Args = null);

        IEnumerator Hide();

        IEnumerator Init();

        IEnumerator SwitchToPopUp(PopUpManager.ePopUpType i_PopUpType, object i_Args = null);

        void Pause();

        void UnPause();

        void EnablePopUpManager();

        void DisablePopUpManager();
    }
}

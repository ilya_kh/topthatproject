﻿using System;
using System.Collections;
using System.Text.RegularExpressions;

using UnityEngine;

[RequireComponent(typeof(TextMesh))]
[RequireComponent(typeof(MeshRenderer))]

[ExecuteInEditMode]
[System.Serializable]
public class MLTextMesh : MonoBehaviour
{
    #region Constants

    private const int k_maximumWidthIndicatorHeight = 5;

    private const float k_worldSizeToFontSizeFactor = 1.865f;

    private const float EPSILON = 0.0001f;

    #endregion

    #region Public fields and Unity references

    public string mr_TextID;

    public Vector2 m_MaximumSize;

    public bool m_UseAutoWidth;

    public bool m_DrawMaximumWidthLine;

    public bool m_DrawMaximumWidthLineWhenPlaying;

    public Color m_MaximumWidthMarkerColor;

    public bool m_UseAutoScale;

    public Camera m_RenderCamera;

    public bool m_EnableLog;

    #endregion


    #region Private fields

    private Vector2 m_currentMaximumSize;

    private TextMesh m_textMesh;

    private string m_currentText;

    private Texture2D m_lineTexture;

    private bool m_isAutoAdjustingWidth;

    private Vector3 m_startLocalPostion;

    private float m_lineSpacing;

    private bool m_isSubtractingFontSize;

    #endregion


    #region Unity Built-in Mathods

    public void Awake()
    {
        this.Init();
    }

    public void Start()
    {
        this.m_currentMaximumSize += new Vector2(0.001f, 0);
    }

public void Update()
    {
        this.StartCoroutine(this.updateEnumerator());
    }

    private IEnumerator updateEnumerator()
    {
        this.checkTextMeshReference();

        this.checkTextColorChange();

        this.checkMaximumSizeChange();

        this.checkTextChange();

        this.checkTextScale();

        yield return new WaitForSeconds(0.01f);

        TextMesh.renderer.enabled = true;
    }

    public void OnGUI()
    {
        this.drawMaximumWidthLine();
    }

    #endregion


    #region Inits

    public void Init()
    {
        this.checkTextMeshReference();

        this.checkCameraReference();

        this.m_isAutoAdjustingWidth = false;

        this.m_isSubtractingFontSize = false;

        this.m_startLocalPostion = this.transform.localPosition;

        this.m_currentMaximumSize = this.m_MaximumSize;

        this.initializeCurrentText();

        this.initializeLineSpacing();
    }

    public void Init(ArenaTextMeshFontData i_FontData)
    {
        this.Init();
        //TextMesh.text = i_FontData.Text;
        //TextMesh.alignment = i_FontData.Alignment;
        //TextMesh.anchor = i_FontData.Anchor;
        //TextMesh.characterSize = i_FontData.CharSize;
        //TextMesh.color = i_FontData.Color;
        //TextMesh.font = i_FontData.Font;
        //TextMesh.renderer.material = i_FontData.Font.material;
        //TextMesh.fontSize = i_FontData.FontSize;
        //TextMesh.fontStyle = i_FontData.FontStyle;
        //TextMesh.lineSpacing = i_FontData.LineSpacing;
        //TextMesh.tabSize = i_FontData.TabSize;
        //TextMesh.offsetZ = i_FontData.OffsetZ;
        //TextMesh.lineSpacing = i_FontData.LineSpacing;
        //renderer.material.shader = i_FontData.Shader;
        //TextMesh.transform.localPosition = i_FontData.LocalPositionOffset;
        //TextMesh.transform.localEulerAngles = i_FontData.LocalRotation;
    }

    private void initializeLineSpacing()
    {
        if (this.TextMesh != null)
        {
            this.m_lineSpacing = this.TextMesh.lineSpacing;
        }
    }

    private void initializeCurrentText()
    {
        if (this.m_currentText == null)
        {
            this.m_currentText = string.Empty;
        }
        else
        {
            if (this.TextMesh != null)
            {
                this.log("Setting inital text in memory.");
                this.m_currentText = this.TextMesh.text;
            }
        }
    }

    #endregion


    #region Properties

    /// <summary>
    /// Gets the text mesh.
    /// </summary>
    /// <value>
    /// The text mesh.
    /// </value>
    public TextMesh TextMesh
    {
        get
        {
            return this.m_textMesh;
        }

        set
        {
            this.m_textMesh = value;
        }
    }
    /// <summary>
    /// Gets and sets new text via changeText() method
    /// </summary>
    /// <value>
    /// String to set
    /// </value>
    public string Text
    {
        get
        {
            return this.TextMesh == null ? null : this.TextMesh.text;
        }

        set
        {
            this.ChangeText(value);
        }
    }

    #endregion


    #region Reference Finders
    
    private void checkCameraReference ()
    {
        if (this.m_RenderCamera == null)
        {
            if (Camera.main != null)
            {
                this.m_RenderCamera = Camera.main;
                
            }
            else
            {
                if (Camera.current != null)
                {
                    this.m_RenderCamera = Camera.current;
                }
                else
                {
                    this.log("No camera found - will not be able to calculate font size!", true);
                }
            }
        }
    }

    /// <summary>
    /// Gets the text mesh reference.
    /// </summary>
    private void getTextMeshReference ()
    {
        this.TextMesh = this.GetComponent<TextMesh>();
    }

    /// <summary>
    /// Checks the text mesh reference.
    /// </summary>
    private void checkTextMeshReference ()
    {
        if (this.TextMesh == null)
        {
            this.getTextMeshReference();
        }
    }

    #endregion


    #region TextMesh Changes Trackers

    /// <summary>
    /// Checks if the text scale is within bounds.
    /// </summary>
    private void checkTextScale()
    {
        if (this.m_UseAutoScale)
        {
            int scaleStep = 100;

            if (this.isFontSizeExceedingBounds(2 * scaleStep))
            {
                this.TextMesh.fontSize -= scaleStep;
                this.TextMesh.fontSize = Mathf.Max(this.TextMesh.fontSize, 0);
            }
            else if (this.isFontSizeUnderBounds(scaleStep * 2))
            {
                this.TextMesh.fontSize += scaleStep;
            }
            else
            {
                this.log("Text scale is within maximum.");
                this.m_isSubtractingFontSize = false;
                this.m_currentMaximumSize = this.m_MaximumSize;
            }
        }
    }

    /// <summary>
    /// Changes the text of the TextMesh
    /// </summary>
    /// <param name="i_NewText">The new text.</param>
    public void ChangeText(string i_NewText)
    {


        this.Init();
        string text = i_NewText;
        if (i_NewText != null && !i_NewText.Equals(string.Empty))
        {
            text = this.setPosition(i_NewText);
            text = this.setLineSpacing(text);
        }
        if (this.TextMesh != null)
        {
            // Turns off the text until auto-width process is completed to prevent graphical glitch
            if (this.m_UseAutoWidth)
            {
                this.TextMesh.renderer.enabled = false;
            }

            this.TextMesh.text = text;
            
        }
    }

    /// <summary>
    /// Checks for text area size change and adjusts if needed.
    /// </summary>
    private void checkMaximumSizeChange()
    {
        if (this.m_currentMaximumSize != this.m_MaximumSize)
        {
            this.log("Maximum size changed.");
            this.adjustMaximumWidth();
        }
    }

    /// <summary>
    /// Adjusts the text scale to fit in text area size
    /// </summary>
    private bool isFontSizeExceedingBounds(float i_Epsilon)
    {
        // Check length:
            Vector2 styleSize = this.getTextSizeInPixels(this.m_currentText);

            float maximumWidth = this.m_MaximumSize.x * k_worldSizeToFontSizeFactor;
            float maximumHeight = this.m_MaximumSize.y * k_worldSizeToFontSizeFactor;

            if (Math.Abs(maximumWidth - 0) < EPSILON)
            {
                maximumWidth = styleSize.x + 1;
            }

            if (Math.Abs(maximumHeight - 0) < EPSILON)
            {
                maximumHeight = styleSize.y + 1;
            }

            this.log(
                "Simulated text size is " + styleSize + ", current maximum size (in world scale) is (" + maximumWidth
                + ", " + maximumHeight + ")");
            return styleSize.x > maximumWidth + i_Epsilon || styleSize.y > maximumHeight + i_Epsilon;
    }

    private bool isFontSizeUnderBounds ( float i_Epsilon )
    {
        // Check length:
        Vector2 styleSize = this.getTextSizeInPixels(this.m_currentText);

        float maximumWidth = this.m_MaximumSize.x * k_worldSizeToFontSizeFactor;
        float maximumHeight = this.m_MaximumSize.y * k_worldSizeToFontSizeFactor;

        this.log(
            "Simulated text size is " + styleSize + ", current minimum size (in world scale) is (" + maximumWidth
            + ", " + maximumHeight + ")");
        return styleSize.x < maximumWidth - i_Epsilon || styleSize.y < maximumHeight - i_Epsilon;
    }

    /// <summary>
    /// Checks for text change and adjusts if needed.
    /// </summary>
    private void checkTextChange ()
    {
        if (!this.m_currentText.Equals(this.Text))
        {
            this.log("Text has changed.");
            this.m_currentText = this.Text;

            if (this.m_UseAutoWidth)
            {
                this.adjustMaximumWidth();
            }
        }
    }

    /// <summary>
    /// Checks for text color change and adjusts the color.
    /// </summary>
    private void checkTextColorChange ()
    {
        //if (this.TextMesh.color != this.renderer.sharedMaterial.color)
        //{
        //    this.log(
        //        this.name + "'s color has changed to "
        //        + this.TextMesh.renderer.sharedMaterial.GetColor("_Color").ToString());
        //    this.renderer.sharedMaterial.SetColor("_Color", this.TextMesh.color);
        //}
    }

    #endregion


    #region Methods & Auxiliary

    /// <summary>
    /// Draws the maximum width line.
    /// </summary>
    private void drawMaximumWidthLine()
    {
        bool isDrawing = true;
        if (!this.m_DrawMaximumWidthLineWhenPlaying)
        {
            isDrawing = !Application.isPlaying;
        }

        if (Application.isEditor && isDrawing && this.m_DrawMaximumWidthLine)
        {
            if (this.m_RenderCamera != null)
            {
                if (this.m_lineTexture == null)
                {
                    this.m_lineTexture = new Texture2D(1, 1);
                }
                GUI.color = this.m_MaximumWidthMarkerColor;
                Vector2 vect = this.getMaximumLineVector();
                GUI.DrawTexture(
                    new Rect(vect.x, Screen.height - vect.y, this.m_MaximumSize.x, k_maximumWidthIndicatorHeight),
                    this.m_lineTexture);
            }
            else
            {
                this.checkCameraReference();
            }
        }
    }

    /// <summary>
    /// Gets the position to place the maximum line
    /// </summary>
    /// <returns></returns>
    private Vector2 getMaximumLineVector()
    {
        Vector2 vect = this.m_RenderCamera.WorldToScreenPoint(this.transform.position);
        vect = this.adjustMaximumWidthLineVector(vect);
        return vect;
    }

    /// <summary>
    /// Adjusts the maximum width line vector according to text anchor
    /// </summary>
    /// <param name="i_VectorToAdjust">The i_ vector to adjust.</param>
    /// <returns></returns>
    private Vector2 adjustMaximumWidthLineVector(Vector2 i_VectorToAdjust)
    {
        switch (this.TextMesh.anchor)
        {
            case TextAnchor.LowerCenter:
            case TextAnchor.MiddleCenter:
            case TextAnchor.UpperCenter:
                {
                    // Adjust for left anchor
                    i_VectorToAdjust.x -= this.m_MaximumSize.x / 2;
                    break;
                }
            case TextAnchor.LowerRight:
            case TextAnchor.MiddleRight:
            case TextAnchor.UpperRight:
                {
                    // Adjust for right anchor
                    i_VectorToAdjust.x -= this.m_MaximumSize.x;
                    break;
                }
        }
        return i_VectorToAdjust;
    }

    /// <summary>
    /// Sets the line spacing if such a tag exists in the string
    /// </summary>
    /// <param name="i_NewText">The i_ new text.</param>
    /// <returns></returns>
    private string setLineSpacing(string i_NewText)
    {
        var regex = new Regex(".*<space (.*) /space>");
        if (regex.IsMatch(i_NewText))
        {
            this.TextMesh.lineSpacing = this.m_lineSpacing;
            this.transform.localPosition = this.m_startLocalPostion;
            var myCapturedText = regex.Match(i_NewText).Groups[1].Value;
            string result = regex.Replace(i_NewText, string.Empty);
            foreach (Match match in Regex.Matches(myCapturedText, @"[,]*([^=]+)=([^,]*)"))
            {

                string key = match.Groups[1].Value;
                string value = match.Groups[2].Value;

                if (key.Trim().Equals("spacing"))
                {
                    try
                    {
                        decimal dec = Convert.ToDecimal(value);
                        this.TextMesh.lineSpacing = (float)dec;
                        return result;
                    }
                    catch (Exception)
                    {
                        this.log(this.name + ": Line spacing parsing failed.", true);
                    }
                }
            }
        }
        return i_NewText;
    }


    /// <summary>
    /// Sets the a position offset for the text if such a tag exists in the string.
    /// </summary>
    /// <param name="i_NewText">The i_ new text.</param>
    /// <returns></returns>
    private string setPosition(string i_NewText)
    {

        int xOffset = 0;
        int yOffstet = 0;
        var regex = new Regex(".*<vector (.*) /vector>");
        if (regex.IsMatch(i_NewText))
        {
            this.transform.localPosition = this.m_startLocalPostion;
            var myCapturedText = regex.Match(i_NewText).Groups[1].Value;
            string result = regex.Replace(i_NewText, string.Empty);
            foreach (Match match in Regex.Matches(myCapturedText, @"[,]*([^=]+)=([^,]*)"))
            {
                string key = match.Groups[1].Value;
                string value = match.Groups[2].Value;

                if (key.Trim().Equals("x"))
                {
                    try
                    {
                        xOffset = Convert.ToInt32(value);
                    }
                    catch (Exception)
                    {
                        return result;
                    }
                }
                if (key.Trim().Equals("y"))
                {
                    try
                    {
                        yOffstet = Convert.ToInt32(value);
                    }
                    catch (Exception)
                    {
                        return result;
                    }
                }
            }
            this.gameObject.transform.localPosition = this.gameObject.transform.localPosition
                                                      + new Vector3(xOffset, yOffstet);
            return result;
        }
        return i_NewText;
    }

    /// <summary>
    /// Adjusts for maximum width and adjusts if auto adjust is enabled
    /// </summary>
    private void adjustMaximumWidth()
    {
        if (this.m_UseAutoWidth && !this.m_isAutoAdjustingWidth)
        {
            this.m_isAutoAdjustingWidth = true;

            this.log("Auto width enabled. Original text was " + this.m_currentText);
            
            // Reset newlines:
            this.m_currentText = this.m_currentText.Replace("\n", " ");
            this.log("Resetting to one line. Text is now " + this.m_currentText);

            // Check length:
            Vector2 styleSize = this.getTextSizeInPixels(this.m_currentText);
            float maximumWidth = this.m_MaximumSize.x * k_worldSizeToFontSizeFactor;

            this.log("Simulated text length is " + styleSize.x + ", current maximum width (in world scale) is " + maximumWidth);
            if (styleSize.x > maximumWidth)
            {
                this.log("Text is longer than " + maximumWidth + ", splitting lines...");
                this.m_currentText = this.splitStringByPixelWidth(this.m_currentText, maximumWidth);
                this.log("New multiline text is " + this.m_currentText);
            }
            else
            {
                this.log("Text length is within maximum.");
            }
            this.Text = this.m_currentText;
            this.TextMesh.renderer.enabled = true;
            this.m_isAutoAdjustingWidth = false;
            this.m_currentMaximumSize = this.m_MaximumSize;
        }
    }



    /// <summary>
    /// Gets the text size in pixels.
    /// </summary>
    /// <param name="i_Text">The text to measure.</param>
    /// <returns>A Vector2 with its' x value = width, y value = height, in pixels (If camera is pixel perfect).</returns>
    private Vector2 getTextSizeInPixels(string i_Text)
    {
        GUIStyle style = new GUIStyle
                             {
                                 font = this.TextMesh.font,
                                 fontSize = this.TextMesh.fontSize,
                                 fontStyle = this.TextMesh.fontStyle
                             };

        return style.CalcSize(new GUIContent(i_Text));
    }

    /// <summary>
    /// Splits the width of the string by pixel width
    /// </summary>
    /// <param name="i_Text">The text to split.</param>
    /// <param name="i_MaximumWidth">Maximum width required.</param>
    /// <returns>The string split by width</returns>
    private string splitStringByPixelWidth ( string i_Text, float i_MaximumWidth )
    {

        // Split string by char " "    
        string[] splitText = i_Text.Split(" "[0]);

        // Prepare result
        string resultText = "";

        // Temp line string
        string currentLine = "";

        // for each all words     
        foreach (string currentWord in splitText)
        {
            // Append current word into line
            string tempLine = currentLine + " " + currentWord;

            // If line length is bigger than i_MaximumWidth
            if (this.getTextSizeInPixels(tempLine).x > i_MaximumWidth)
            {
                this.log("Line is: " + this.getTextSizeInPixels(tempLine).x + " and max is " + i_MaximumWidth + ", adding 1 line...");
                // Append current line into result
                resultText += currentLine + "\n";
                
                // Remain word append into new line
                currentLine = currentWord;
            }
            else
            {
                // Append current word into current line
                currentLine = tempLine;
            }
        }

        // Append last line into result   
        resultText += currentLine;

        // Return restul without first " " char
        return resultText.Substring(1, resultText.Length - 1);
    }


    /// <summary>
    /// Logs the specified text.
    /// </summary>
    /// <param name="i_Text">The text to log.</param>
    /// <param name="i_IsWarning">if set to <c>true</c> [is warning].</param>
    private void log(string i_Text, bool i_IsWarning = false)
    {
        if (this.m_EnableLog)
        {
            if (i_IsWarning)
            {
                Debug.LogWarning(this.name + ": " + i_Text, this);
            }

            else
            {
                Debug.Log(this.name + ": " + i_Text, this);
            }
        }
    }

    #endregion
}
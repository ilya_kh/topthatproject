﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectionRunner : MonoBehaviour
{

    public tk2dSprite mr_BaseElem;

    public GameObject mr_WorldHolder;

    public GameObject mr_ColumnsHolder;

	// Use this for initialization
    [SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1027:TabsMustNotBeUsed", Justification = "Reviewed. Suppression is OK here.")]
    void Start ()
	{
        Dictionary<int, ColumnController> columnsGameObjects = new Dictionary<int, ColumnController>();

	    for (int i = 2; i < 13; i++)
	    {
	        GameObject column = new GameObject("Column " + i);
	        column.transform.parent = this.mr_ColumnsHolder.transform;
            column.transform.localPosition = new Vector3(0,0,0);
	        var colController = column.AddComponent<ColumnController>();
            colController.mr_CellControllers  = new List<CellController>();
            columnsGameObjects[i] = colController;
	    }

        List<string> names = new List<string>();

	    foreach (var tk2DSpriteDefinition in this.mr_BaseElem.Collection.spriteDefinitions)
	    {
	        string name = tk2DSpriteDefinition.name;
	        var splitedItems = name.Split('_');
	        int columnId = Convert.ToInt32(splitedItems[0]);
	        int xPlace = Convert.ToInt32(splitedItems[1].Substring(1));
	        int yPlace = -Convert.ToInt32(splitedItems[2].Substring(1));
            GameObject cell = new GameObject("Cell");
	        cell.transform.parent = mr_WorldHolder.transform;
	        cell.transform.localPosition = new Vector3(xPlace,yPlace,-10);
	        cell.transform.parent = columnsGameObjects[columnId].transform;
	        var controller = cell.AddComponent<CellController>();
            columnsGameObjects[columnId].mr_CellControllers.Add(controller);
            GameObject pic = new GameObject("Sprite");
	        pic.transform.parent = controller.transform;
	        pic.transform.localPosition = Vector3.zero + new Vector3(0,0,-10);
	        var sprite = pic.AddComponent<tk2dSprite>();
	        sprite.SetSprite(this.mr_BaseElem.Collection, name);
	        controller.mr_Image = pic.GetComponent<tk2dSprite>();
	        controller.mr_ColumnId = columnId;
	    }

        foreach (var columnsGameObject in columnsGameObjects)
        {
            var orderedCells = columnsGameObject.Value.mr_CellControllers.OrderBy(m => m.transform.localPosition.y);
            for (int i = 0; i < orderedCells.Count(); i++)
            {
                orderedCells.ElementAt(i).name += i + 1;
            }
        }
	}
}

﻿using UnityEngine;
using System.Collections;

public class ResignState
{
    public BoardController.ePlayer Resigner { get; set; }

    public ResignState(BoardController.ePlayer i_Resigner)
    {
        this.Resigner = i_Resigner;
    }

    public ResignState()
    {

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class TextService : MonoBehaviour
{
    public struct JsonableFontData
    {
        public string m_Text;

        public float m_CharSize;

        public float m_CharSpacing;

        public int m_Anchor;

        public float m_LineSpacing;

        public int m_Alignment;

        public float[] m_Color;

        public int m_Font;

        public float m_TabSize;

        public int m_FontSize;

        public int m_FontStyle;

        public float m_OffsetZ;

        public bool m_RichText;

        public float[] m_PositionOffset;

        public float[] m_Rotation;

        public bool m_IsUsingMaxWidth;

        public float m_MaximumTextWidth;
    }

    private const string k_textsJsonFileName = "Texts";
    
    public Dictionary<string, JsonableFontData> m_ArenaStaticTexts;

    private static Dictionary<string, string> s_texts;

    public static TextService Instance;

    public Font[] m_Fonts;

    private int m_arenaID;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogWarning("Second instantiation of JSONService blocked.");
        }
    }

    public IEnumerator Init(int i_ArenaID = -1)
    {
        m_arenaID = i_ArenaID;
        yield return this.StartCoroutine(parseJSON());
    }

    public static Font[] Fonts
    {
        get
        {
            if (Instance != null)
            {
                return Instance.m_Fonts;
            }
            return null;
        }
    }

    public static ArenaTextMeshFontData GetFontDataById(string i_Id)
    {
        ArenaTextMeshFontData value = null;
        return value;
    }

    public static string GetTextById(string i_Id)
    {
        if (s_texts != null && s_texts.ContainsKey(i_Id))
        {
            return s_texts[i_Id];
        }
        Debug.LogError("Text not found for id " + i_Id);
        return null;
    }

    public static void ParseJSON()
    {
        Instance.StartCoroutine(Instance.parseJSON());
    }

    private IEnumerator parseJSON()
    {
        yield return StartCoroutine(this.parseJson(s_texts, k_textsJsonFileName));
    }

    private IEnumerator parseJson(IDictionary<string, string> i_Dictionary, string i_JsonFileName)
    {

        TextAsset config = Resources.Load(i_JsonFileName) as TextAsset;

        /*
        string path;
        string assetsPath = Application.dataPath+"/StreamingAssets/";
        if (Application.isEditor)
        {
            path = "file:///" + assetsPath + i_JsonFileName;
        }
        else
        {
            path = assetsPath + i_JsonFileName;
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            path = Application.streamingAssetsPath + "/" + i_JsonFileName;
        }

        Debug.LogWarning("Parsing JSON from " + path);
        WWW www = new WWW(path);
        yield return www;*/
        Dictionary<string, string> json = JsonConvert.DeserializeObject<Dictionary<string, string>>(config.ToString());

        if (json != null)
        {
            print("JSON parsing succeeded.");
            s_texts = json;
        }
        else
        {
            print("JSON parsing failed.");
        }
        yield break;
    }
}
﻿using System;
using System.Linq;
using MVCore;
using TopThat;
using System.Collections.Generic;
using TopThat.Models;

using UnityEngine;

public class GameViewModel
{

    #region Events 

    public delegate void BoardIsUpdated();

    public event BoardIsUpdated BoardIsUpdatedEvent;


    public delegate void GameIsWon(BoardController.ePlayer i_WonPlayer);

    public event GameIsWon GameWonEvent;

    #endregion 

    private readonly Dictionary<BoardController.ePlayer, TopThatGame.ePlayer> m_PlayersMapper =
        new Dictionary<BoardController.ePlayer, TopThatGame.ePlayer>()
            {
                {BoardController.ePlayer.Red, TopThatGame.ePlayer.Player1},
                {BoardController.ePlayer.Purple, TopThatGame.ePlayer.Player2}
            };

    private TopThatGame m_topThatGameModel;

    private BoardController m_BoardController;

    private IGameMove m_selectedMove;


    public GameViewModel(TopThatGame i_TopThatGame , BoardController i_BoardController)
    {
        this.m_topThatGameModel = i_TopThatGame;
        this.m_BoardController = i_BoardController;
        this.m_topThatGameModel.GameEndEvent += this.onTopThatGameModelOnGameEndEvent;
    }

    public void StartGame(BoardController.ePlayer i_Player)
    {
        this.m_topThatGameModel = new TopThatGame(this.m_PlayersMapper[i_Player]);
        //this.m_topThatGameModel = new TopThatGame(this.getSpecialState());
    }

    private void onTopThatGameModelOnGameEndEvent(TopThatGame.ePlayer i_WonPlayer)
    {
        if (i_WonPlayer == TopThatGame.ePlayer.Player1)
        {
            Debug.LogError(" Game won by " + BoardController.ePlayer.Red);
            if (this.GameWonEvent != null)
            {
                this.GameWonEvent(BoardController.ePlayer.Red);
            }
        }

        if (i_WonPlayer == TopThatGame.ePlayer.Player2)
        {
            Debug.LogError(" Game won by " + BoardController.ePlayer.Purple);
            if (this.GameWonEvent != null)
            {
                this.GameWonEvent(BoardController.ePlayer.Purple);
            }
        }
    }

    public IGameMove ModelMove
    {
        set
        {
            this.m_selectedMove = value;
        }
    }


    public void PerformMove()
    {
        if (this.m_selectedMove != null)
        {
            this.m_topThatGameModel.ApplyMove(this.m_selectedMove);
            this.m_selectedMove = null;
        }
        else
        {
            throw new Exception("Trying to apply forbidden move");
        }
    }

    public List<Marker> GetMarkersForPlayer(BoardController.ePlayer i_Player)
    {
        
        List<Marker> markers = new List<Marker>();
        var boardCells = this.m_topThatGameModel.GetMarkers(this.m_PlayersMapper[i_Player]);
        foreach (var boardCell in boardCells)
        {
            CellController cellController =
                this.m_BoardController.mr_ColumnControllers.Find(m => m.m_ColumnId == boardCell.ColumnId)
                    .mr_CellControllers.Find(m => m.mr_CellId == boardCell.CellId);
            markers.Add(cellController.mr_Markers.Find(m => m.mr_Player == i_Player));
        }
        return markers;
    }

    public List<ColumnController> GetWonColumnsForPlayer(BoardController.ePlayer i_Player)
    {
        List<ColumnController> columnControllers = new List<ColumnController>();
        var wonColumns = this.m_topThatGameModel.GetCompletedColumns(this.m_PlayersMapper[i_Player]);
        foreach (var wonColumn in wonColumns)
        {
            ColumnController columnController = this.m_BoardController.mr_ColumnControllers.Find(m => m.m_ColumnId == wonColumn);
            columnControllers.Add(columnController);
        }
        return columnControllers;
    }

    public List<CellController> GetClimbersForPlayer(BoardController.ePlayer i_Player)
    {
        List<CellController> cellControllers = new List<CellController>();
        var boardCells = this.m_topThatGameModel.GetClimbers(this.m_PlayersMapper[i_Player]);
        foreach (var boardCell in boardCells)
        {
            CellController cellController =
                this.m_BoardController.mr_ColumnControllers.Find(m => m.m_ColumnId == boardCell.ColumnId)
                    .mr_CellControllers.Find(m => m.mr_CellId == boardCell.CellId);
            cellControllers.Add(cellController);
        }
        return cellControllers;
    }

    public BoardController.ePlayer GetCurrentTurn()
    {
        if (this.m_topThatGameModel.GetCurrentTurn() == TopThatGame.ePlayer.Player1)
        {
            return BoardController.ePlayer.Red;
        }
        else
        {
            return BoardController.ePlayer.Purple;
        }
    }

    public RandomChoice GetRandomChoice()
    {
        return this.m_topThatGameModel.GetRandomChoice();
    }

    public ICollection<IGameMove> GetMoves(RandomChoice i_RandomChoice)
    {
        return this.m_topThatGameModel.GetPossibleMoves(i_RandomChoice);
    }

    public TopThat.Models.PlayMove GetModelMove(Tuple<MagnetController,MagnetController> i_FirstTuple , Tuple<MagnetController,MagnetController> i_SecondTuple)
    {
        int firstSum, secondSum;
        firstSum = i_FirstTuple.Item1.GetNumber + i_FirstTuple.Item2.GetNumber;
        secondSum = i_SecondTuple.Item1.GetNumber + i_SecondTuple.Item2.GetNumber;
        Debug.Log("First sum : " + firstSum + " , " + secondSum);
        var possiblePlayMoves = this.GetMoves(this.GetRandomChoice()).ToList().FindAll(m => m.GetType() == typeof(TopThat.Models.PlayMove)).Cast<TopThat.Models.PlayMove>().ToList();
        var modelMove = possiblePlayMoves.Find(m => (m.FirstCombination == firstSum || m.SecondCombination == secondSum) || (m.FirstCombination == secondSum || m.SecondCombination == firstSum));
        return modelMove;
    }




    public bool IsThereAPossibleMove()
    {
        return this.m_topThatGameModel.GetPossibleMoves(this.GetRandomChoice()).ToList().Find(m => m.GetType() == typeof(TopThat.Models.PlayMove))!=null;
    }

    public  void PerformExclusiveMove(int i_SelectedOption)
    {
        TopThat.Models.PlayMove playMove = this.m_selectedMove as TopThat.Models.PlayMove;
        if (playMove != null)
        {
            playMove.SetSelectedOption(i_SelectedOption);
            this.PerformMove();
        }
    }

    public void PerformKeepMove()
    {
        var confirmMove = this.m_topThatGameModel.GetPossibleMoves(this.GetRandomChoice()).ToList().Find(m => m.GetType() == typeof(ConfirmMove));
        this.m_topThatGameModel.ApplyMove(confirmMove);
        this.m_selectedMove = null;
    }

    public void PerformForteitMove()
    {
        var forteitMove = this.m_topThatGameModel.GetPossibleMoves(this.GetRandomChoice()).ToList().Find(m => m.GetType() == typeof(TopThat.Models.ForteitMove));
        this.m_topThatGameModel.ApplyMove(forteitMove);
        this.m_selectedMove = null;
    }

    public ViewGameState GenerateViewGameState()
    {
        return new ViewGameState(
            this.GetCurrentTurn(),
            this.GetMarkersForPlayer(BoardController.ePlayer.Red),
            this.GetClimbersForPlayer(BoardController.ePlayer.Red),
            this.GetMarkersForPlayer(BoardController.ePlayer.Purple),
            this.GetClimbersForPlayer(BoardController.ePlayer.Purple),
            this.GetWonColumnsForPlayer(BoardController.ePlayer.Red),
            this.GetWonColumnsForPlayer(BoardController.ePlayer.Purple));
    }

    public ViewMove GenerateViewMove(TopThat.Models.PlayMove i_PlayMove , PeekPair i_PeekPair)
    {
        List<ColumnStatus> columnStatuses = new List<ColumnStatus>();
        int firstSum = i_PeekPair.FirstTuple.Item1.GetNumber + i_PeekPair.FirstTuple.Item2.GetNumber;
        int secondSum = i_PeekPair.SecondTuple.Item1.GetNumber + i_PeekPair.SecondTuple.Item2.GetNumber;
        var columns = this.m_BoardController.mr_ColumnControllers.FindAll(m=>m.m_ColumnId == firstSum || m.m_ColumnId == secondSum);
        foreach (var columnController in columns)
        {
            if (i_PlayMove.FirstCombination == columnController.m_ColumnId || i_PlayMove.SecondCombination == columnController.m_ColumnId)
            {
                var columnStatus = new ColumnStatus(columnController, ColumnStatus.eColumnStatus.PossibleClimb);
                columnStatuses.Add(columnStatus);
                continue;
            }
            if (
                this.m_topThatGameModel.GetCompletedColumns(TopThatGame.ePlayer.Player1).Contains(columnController.m_ColumnId)|| this.m_topThatGameModel.GetCompletedColumns(TopThatGame.ePlayer.Player2).Contains(
                    columnController.m_ColumnId))
            {
                var columnStatus = new ColumnStatus(columnController, ColumnStatus.eColumnStatus.ColumnConquered);
                columnStatuses.Add(columnStatus);
                continue;
            }

            else
            {
                var columnStatus = new ColumnStatus(columnController, ColumnStatus.eColumnStatus.ImpossibleClimb);
                columnStatuses.Add(columnStatus);
            }

        }

        ViewMove viewMove = new ViewMove(columnStatuses);
        return viewMove;
    }


    public BoardController.ePlayer GetWonPlayer()
    {
        var modelPlayer  = this.m_topThatGameModel.IsTerminalState();
        if (modelPlayer == TopThatGame.ePlayer.None)
        {
            return BoardController.ePlayer.None;
        }
        else if (modelPlayer == TopThatGame.ePlayer.Player1)
        {
            return BoardController.ePlayer.Red;
        }
        else
        {
            return BoardController.ePlayer.Purple;
        }
    }



    private State getSpecialState()
    {
        /*
        List<BoardCell> redMarkers = new List<BoardCell>();
        redMarkers.Add(new BoardCell(2, 2));

        Team redTeam = new Team(TopThatGame.ePlayer.Player1, redMarkers, new List<BoardCell>(), new List<int>());
        Team blueTeam = new Team(TopThatGame.ePlayer.Player2, new List<BoardCell>(), new List<BoardCell>(), new List<int>());

        List<Team> teams = new List<Team>();
        teams.Add(redTeam);
        teams.Add(blueTeam);
        State state = new State(teams, TopThatGame.ePlayer.Player1);
        var choice = new List<int>();
        choice.Add(1);
        choice.Add(1);
        choice.Add(1);
        choice.Add(1);
        state.SetRandomChoice(new RandomChoice(TopThatGame.ePlayer.Player1, choice));
        return state;*/


        List<BoardCell> redMarkers = new List<BoardCell>();
        redMarkers.Add(new BoardCell(2, 5));
        redMarkers.Add(new BoardCell(10, 6));

        List<BoardCell> redPlayers = new List<BoardCell>();
        redPlayers.Add(new BoardCell(1,9));
        redPlayers.Add(new BoardCell(5, 6));

        List<int> redConquaredRoutes = new List<int>();
        redConquaredRoutes.Add(2);
        redConquaredRoutes.Add(8);


        List<BoardCell> blueMarkers = new List<BoardCell>();
        blueMarkers.Add(new BoardCell(2,3));
        blueMarkers.Add(new BoardCell(2, 5));
        blueMarkers.Add(new BoardCell(4, 6));
        blueMarkers.Add(new BoardCell(3, 10));


        List<int> blueConquaredRoutes = new List<int>();
        blueConquaredRoutes.Add(7);
        //blueConquaredRoutes.Add(8);


        Team redTeam = new Team(TopThatGame.ePlayer.Player1, redMarkers, new List<BoardCell>(), redConquaredRoutes);
        Team blueTeam = new Team(TopThatGame.ePlayer.Player2, blueMarkers, new List<BoardCell>(), blueConquaredRoutes);

        List<Team> teams = new List<Team>();
        teams.Add(redTeam);
        teams.Add(blueTeam);
        State state = new State(teams, TopThatGame.ePlayer.Player1);
        var choice = new List<int>();
        choice.Add(3);
        choice.Add(3);
        choice.Add(5);
        choice.Add(4);
        state.SetRandomChoice(new RandomChoice(TopThatGame.ePlayer.Player1, choice));
        return state;
    }
}

﻿using UnityEngine;
using System.Collections;

public class Marker : MonoBehaviour
{
    public tk2dSprite mr_Sprite;

    public BoardController.ePlayer mr_Player;

    private bool m_isMarkerIsEarned;

    private string m_startImageName;

    public void ChangeMarkerVisulalState()
    {
        if (this.m_isMarkerIsEarned)
        {
            if (this.mr_Player == BoardController.ePlayer.Red)
            {
                this.mr_Sprite.SetSprite(this.m_startImageName + "_Red");
            }
            else
            {
                this.mr_Sprite.SetSprite(this.m_startImageName + "_Purple");
            }
        }
        else
        {
            this.mr_Sprite.SetSprite(this.m_startImageName);

        }
    }

    private void Awake()
    {
        this.m_startImageName = mr_Sprite.GetCurrentSpriteDef().name;
        if (this.gameObject.name.StartsWith("Red"))
        {
            this.mr_Player = BoardController.ePlayer.Red;
        }
        else
        {
            this.mr_Player = BoardController.ePlayer.Purple;
        }
        this.Reset();
    }

    public void Reset()
    {
        this.m_isMarkerIsEarned = false;
        this.ChangeMarkerVisulalState();
    }

    public void SetMarkerStatus(bool i_Status)
    {
        this.m_isMarkerIsEarned = i_Status;
        this.ChangeMarkerVisulalState();
    }
}

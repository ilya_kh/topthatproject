﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts.TopThat.PopUps
{

    public class RedIlegalMovePopUp : TopThatPanelPopUp
    {
        public MLTextMesh mr_PlayButtonText;

        public override void Reset(object i_Args)
        {
            this.mr_PlayButtonText.ChangeText(TextService.GetTextById("1-0-9"));
        }

        public override System.Collections.IEnumerator Hide()
        {
            this.gameObject.SetActive(false);
            yield break;
        }
    }
}

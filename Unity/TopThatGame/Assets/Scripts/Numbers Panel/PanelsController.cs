﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PanelsController : MonoBehaviour
{

    public event GameView.ActionResponse ActionResponse;

    public List<PanelController> mr_PanelControllers;


    public IEnumerator SetCubes(BoardController.ePlayer i_Player, List<int> i_PanelInput , bool i_FirstTurn = false)
    {
        if (!i_FirstTurn)
        {
            yield return new WaitForSeconds(0.2F);
        }
        this.mr_PanelControllers.Find(m => m.mr_Player == i_Player).mr_MagnetsController.SetInput(i_PanelInput);
    }


    public void Reset()
    {
        foreach (var panelController in this.mr_PanelControllers)
        {
            panelController.Reset();
        }
    }

    public IEnumerator StartTurn(BoardController.ePlayer i_Player, bool i_IsFirstTurn = false)
    {
        this.StartCoroutine(this.mr_PanelControllers.Find(m => m.mr_Player != i_Player).StopTurn(i_IsFirstTurn));
        yield return this.StartCoroutine(this.mr_PanelControllers.Find(m => m.mr_Player == i_Player).StartTurn(i_IsFirstTurn));
    }

    // Use this for initialization
	IEnumerator Start () {

	    foreach (var panelController in this.mr_PanelControllers)
	    {
	        panelController.ActionResponse += this.PanelControllerOnActionResponse;
	        yield return this.StartCoroutine(panelController.m_InteractivePanelController.mr_PanelPopUpManager.Init());
	    }
	}

    private ActionResponseAbstract PanelControllerOnActionResponse(ViewEventAbstract i_ViewEventAbstract)
    {
        if (this.ActionResponse != null)
        {
            return this.ActionResponse(i_ViewEventAbstract);
        }
        return null;
    }
}

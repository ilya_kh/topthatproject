﻿using System;

using Assets.Scripts.PopUp_Manager.Interfaces;
using UnityEngine;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;

[Config("asdf")]
public class SplashMenu : MonoBehaviour
{

    #region Events 

    public delegate void StartGamePressed();

    public event StartGamePressed StartGame;

    #endregion
    
    public GameObject mr_Pause;

    public UIButton mr_ExitButton;

    public UIButton mr_StartGameButton;

    public UIButton mr_RulesButton;

    public UIStateToggleBtn mr_Sound;

    private bool m_isMenuOpened = false;

    private IPopUpManager m_PopUpManager;

    public IEnumerator Init(IPopUpManager i_PopUpManager)
    {
        this.m_PopUpManager = i_PopUpManager;
        this.SetInputListeners();
        this.GetComponentsInChildren<TooltipController>(true).ToList().ForEach(m => m.Init());
        yield break;
    }


    public void PauseMenu()
    {
        this.mr_Pause.SetActive(true);
    }

    public void UnPauseMenu()
    {
        this.mr_Pause.SetActive(false);
    }


    private void DisableButtons()
    {
        this.mr_StartGameButton.gameObject.SetActive(false);
        this.mr_ExitButton.gameObject.SetActive(false);
        this.mr_RulesButton.gameObject.SetActive(false);
        this.mr_Sound.gameObject.SetActive(false);
    }

    private void EnaleButtons()
    {
        this.mr_StartGameButton.gameObject.SetActive(true);
        this.mr_ExitButton.gameObject.SetActive(true);
        this.mr_RulesButton.gameObject.SetActive(true);
        this.mr_Sound.gameObject.SetActive(true);
    }


    #region Methods

    public void PlayButtonClicked()
    {
        if (this.StartGame != null)
        {
            this.StartGame();
        }
    }

    public void ExitButtonPressed()
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.ButtonIn, SoundManagerStaticInfo.eTags.Effects, false);
        this.StartCoroutine(this.m_PopUpManager.Show(PopUpManager.ePopUpType.Exit));
    }

    public void HomeButtonPressed()
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.ButtonIn, SoundManagerStaticInfo.eTags.Effects, false);
        this.StartCoroutine(this.m_PopUpManager.Show(PopUpManager.ePopUpType.BackToMain));
    }

    public void RulesButtonPressed()
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.ButtonIn, SoundManagerStaticInfo.eTags.Effects, false);
        this.StartCoroutine(this.m_PopUpManager.Show(PopUpManager.ePopUpType.Rules));
    }

    public void SoundButtonPressed()
    {
        if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Music))
        {
            SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Music, true);
        }
        else
        {
            SoundManager.SetIsMutedForTag(SoundManagerStaticInfo.eTags.Music, false);
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Music))
            {
                this.mr_Sound.SetToggleState("Normal_On");
            }
            else
            {
                this.mr_Sound.SetToggleState("Normal_Off");
            }
        }
    }

    #endregion


    #region Mouseover


    private void SetInputListeners()
    {
        if (Application.platform != RuntimePlatform.Android)
        {
            this.mr_Sound.AddInputDelegate(this.onMouseOver);
        }
        this.mr_ExitButton.AddInputDelegate(this.onMouseOverButtonExit);
        this.mr_RulesButton.AddInputDelegate(this.onMouseOverButtonRules);
        //this.mr_Sound.AddInputDelegate(this.onMouseOverButtonSound);
    }

    private void onMouseOverButtonExit(ref POINTER_INFO ptrm)
    {
        if (ptrm.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_ExitButton.GetComponentInChildren<TooltipController>().OnShow();
        }
        else if (MenuController.IsTooltipShoulBeHidden(ptrm))
        {
            this.mr_ExitButton.GetComponentInChildren<TooltipController>().OnHide();
        }
    }


    private void onMouseOverButtonRules(ref POINTER_INFO ptrm)
    {
        if (ptrm.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_RulesButton.GetComponentInChildren<TooltipController>().OnShow();
        }
        else if (MenuController.IsTooltipShoulBeHidden(ptrm))
        {
            this.mr_RulesButton.GetComponentInChildren<TooltipController>().OnHide();
        }
    }


    private void onMouseOverButtonSound(ref POINTER_INFO ptrm)
    {
        if (ptrm.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnShow();
        }
        else if (MenuController.IsTooltipShoulBeHidden(ptrm))
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnHide();
        }
    }


    private void onMouseOver(ref POINTER_INFO ptr)
    {
        if (this.mr_Sound.ToggleState() == 4)
        {
            return;
        }

        if (this.handleAndroidInput(ptr))
        {
            return;
        }

        this.handleWebInout(ptr);

    }

    private void handleWebInout(POINTER_INFO ptr)
    {
        if (ptr.evt == POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnShow();

            if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Music))
            {
                this.mr_Sound.SetToggleState("Over_On");
            }
            else
            {
                this.mr_Sound.SetToggleState("Over_Off");
            }
        }
        else if (ptr.evt != POINTER_INFO.INPUT_EVENT.MOVE)
        {
            this.mr_Sound.GetComponentInChildren<TooltipController>().OnHide();

            if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Music))
            {
                this.mr_Sound.SetToggleState("Normal_On");
            }
            else
            {
                this.mr_Sound.SetToggleState("Normal_Off");
            }
        }
    }

    private bool handleAndroidInput(POINTER_INFO ptr)
    {
        
        if (Application.platform == RuntimePlatform.Android)
        {
            return true;
            if (ptr.evt == POINTER_INFO.INPUT_EVENT.PRESS)
            {
                if (SoundManager.IsTagActive(SoundManagerStaticInfo.eTags.Music))
                {
                    this.mr_Sound.SetToggleState("Normal_On");
                }
                else
                {
                    this.mr_Sound.SetToggleState("Normal_Off");
                }
            }
            return true;
        }
        return false;
    }

    #endregion

}

public class ConfigAttribute : Attribute
{
    public readonly string sdf;

    public ConfigAttribute(string i_Asdf)
    {
        this.sdf = i_Asdf;
    }
}

﻿using System.Collections;

public class PurplePanelPopUpManager : PanelPopUpManager
{
    public override IEnumerator ShowMovePopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Purple_MovePopUp, i_Param));
    }

    public override IEnumerator ShowExlusiveMovePopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Purple_ExclusiveMovePopUp, i_Param));
    }

    public override IEnumerator ShowNoMovesPopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Purple_NoLegalMovesPopUp, i_Param));
    }

    public override IEnumerator ShowPlayOrKeppPopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Purple_KeepOrPlayPopUp, i_Param));
    }

    public override IEnumerator ShowIlegalChoicePopUp(object i_Param = null)
    {
        yield return this.StartCoroutine(Show(ePopUpType.Purple_IlegalChoicePopUp, i_Param));
    }
}


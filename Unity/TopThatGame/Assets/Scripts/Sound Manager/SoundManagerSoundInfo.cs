﻿using UnityEngine;
using System.Collections;

public class SoundManagerSoundInfo
{
    public SoundManagerStaticInfo.eSoundIds SoundId { get; set; }

    public AudioSource AudioSource { get; set; }

    public SoundManagerSoundInfo(SoundManagerStaticInfo.eSoundIds i_SoundId, AudioSource i_AudioSource)
    {
        this.SoundId = i_SoundId;
        this.AudioSource = i_AudioSource;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts.TopThat.PopUps
{
    using Assets.Scripts.View.ConcreteEvents;

    public class RedPlayOrKeepPopUp : TopThatPanelPopUp
    {

        public MLTextMesh mr_KeepText;

        public MLTextMesh mr_ContinueText;

        public override void Reset(object i_Args)
        {
            this.mr_KeepText.ChangeText(TextService.GetTextById("1-0-1"));
            this.mr_ContinueText.ChangeText(TextService.GetTextById("1-0-2"));
        }


        public void KeepWasClicked()
        {
            SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.ButtonIn, SoundManagerStaticInfo.eTags.Effects, false);
            this.SendUserInteractionEvent(new KeepMove());
        }


        public void ContinueWasClicked()
        {
            SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.ButtonIn, SoundManagerStaticInfo.eTags.Effects, false);
            this.SendUserInteractionEvent(new ContinueMove());
        }
    }
}

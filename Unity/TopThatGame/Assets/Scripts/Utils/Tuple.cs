﻿using UnityEngine;
using System.Collections;

public class Tuple<T1, T2>
{
    private T1 _firstItem;
    private T2 _secondItem;


    /* Properties of Tuple */
    public T1 Item1
    {
        get
        {
            return _firstItem;
        }

        set
        {
            _firstItem = value;
        }
    }

    public T2 Item2
    {
        get
        {
            return _secondItem;
        }

        set
        {
            _secondItem = value;
        }
    }

    public Tuple(T1 t1, T2 t2)
    {
        _firstItem = t1;
        _secondItem = t2;
    }

    public Tuple()
    {

    }

    public override string ToString()
    {
        return "< " + _firstItem + " , " + _secondItem + " > ";
    }


}

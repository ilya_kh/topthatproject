﻿using UnityEngine;
using System.Collections;

public class InteractivePanel : MonoBehaviour
{

    public virtual IEnumerator Show()
    {
        this.gameObject.SetActive(true);
        yield break;
    }

    public virtual IEnumerator Hide()
    {
        this.gameObject.SetActive(false);
        yield break;
    }
}

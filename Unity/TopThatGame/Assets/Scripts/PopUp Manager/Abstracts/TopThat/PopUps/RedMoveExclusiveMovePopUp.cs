﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts.TopThat.PopUps
{
    using Assets.Scripts.View.ConcreteEvents;

    public class RedMoveExclusiveMovePopUp : TopThatPanelPopUp
    {
        public MLTextMesh mr_FirstOptionText;

        public MLTextMesh mr_SecondOptionText;


        private Tuple<int, int> m_Options; 

        public override void Reset(object i_Args)
        {
            Tuple<int, int> options = i_Args as Tuple<int, int>;
            this.m_Options = options;
            this.mr_FirstOptionText.ChangeText(string.Format(TextService.GetTextById("1-0-3"),options.Item1));
            this.mr_SecondOptionText.ChangeText(string.Format(TextService.GetTextById("1-0-3"), options.Item2));
        }

        public override System.Collections.IEnumerator Hide()
        {
            this.gameObject.SetActive(false);
            yield break;
        }

        public void MakeFirstMove()
        {
            this.SendUserInteractionEvent(new ExclusivePlayMove(this.m_Options.Item1));
        }

        public void MakeSecondMove()
        {
            this.SendUserInteractionEvent(new ExclusivePlayMove(this.m_Options.Item2));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using Assets.Scripts.PopUp_Manager.Abstracts;
using Assets.Scripts.PopUp_Manager.Interfaces;
using UnityEngine;
using System.Collections;

public abstract class PopUpManager : MonoBehaviour, IPopUpManager
{


    #region Events 

    public delegate void PopUpStarted ();

    public delegate void PopUpEnded();

    public event PopUpStarted PopUpStartedEvent;

    public event PopUpEnded PopUpEndedEvent;


    #endregion 


    private Dictionary<ePopUpType, AbstractPopUpScreen> m_PopUpScreens = new Dictionary<ePopUpType, AbstractPopUpScreen>();

        private PopUpManager.ePopUpType m_showingPopUp = ePopUpType.None;

        public Stack<ePopUpType> m_ShowedPopUps = new Stack<ePopUpType>();

        public GameObject mr_Pause;


        private GameObject m_ScreensHolder;

        public enum ePopUpType
        {
            None,

            Red_MovePopUp,

            Red_ExclusiveMovePopUp,

            Red_KeepOrPlayPopUp,

            Red_NoLegalMovesPopUp,

            Red_IlegalChoicePopUp,

            Purple_MovePopUp,

            Purple_ExclusiveMovePopUp,

            Purple_KeepOrPlayPopUp,

            Purple_NoLegalMovesPopUp,

            Purple_IlegalChoicePopUp,

            Rules,

            Settings,

            Success,

            Exit,

            BackToMain, 
            
            Resign
        }


    public void SetPopUp(ePopUpType i_PopUpType)
    {
        this.m_showingPopUp = i_PopUpType;
    }

    public virtual IEnumerator Show(ePopUpType i_PopUpType, object i_Args = null)
        {
            if (this.m_showingPopUp == i_PopUpType)
            {
                yield break;
            }
            if (this.PopUpStartedEvent != null)
            {
                this.PopUpStartedEvent();
            }
            this.Pause();
            this.BlockPopUpContent();
            this.m_ShowedPopUps.Push(i_PopUpType);
            this.m_showingPopUp = i_PopUpType;
            this.m_PopUpScreens[i_PopUpType].gameObject.SetActive(true);
            this.m_PopUpScreens[i_PopUpType].Reset(i_Args);
            yield return this.StartCoroutine(this.m_PopUpScreens[i_PopUpType].Show());
            this.UnBlockPopUpContent();

        }

        public virtual IEnumerator Hide()
        {
          
            if (this.m_ShowedPopUps.Count == 0)
            {
                yield break;
            }

            this.BlockPopUpContent();
            var popUpType = this.m_ShowedPopUps.Peek();
            yield return this.StartCoroutine(this.m_PopUpScreens[popUpType].Hide());
            this.m_PopUpScreens[popUpType].gameObject.SetActive(false);
            this.m_ShowedPopUps.Pop();
            this.m_showingPopUp = ePopUpType.None;
            this.UnBlockPopUpContent();
            this.UnPause();
            if (this.PopUpEndedEvent != null)
            {
                this.PopUpEndedEvent();
            }
        }

        public virtual IEnumerator Init()
        {
            yield return this.StartCoroutine(this.SpecificInit());
            AbstractPopUpScreen[] popUpScreens = this.GetComponentsInChildren<AbstractPopUpScreen>(true);
            foreach (var abstarctPopUpScreen in popUpScreens)
            {
                this.m_PopUpScreens[abstarctPopUpScreen.m_PopUpType] = abstarctPopUpScreen;
                if (this.m_PopUpScreens[abstarctPopUpScreen.m_PopUpType] is IArenaPopUpInitializeable)
                {
                    yield return this.StartCoroutine((this.m_PopUpScreens[abstarctPopUpScreen.m_PopUpType] as IArenaPopUpInitializeable).InitializeEnumerator());
                }
                abstarctPopUpScreen.gameObject.SetActive(false);
            }
            this.m_ScreensHolder = this.transform.FindChild("Screens").gameObject;            
            yield break;
        }


    public void Reset()
    {
        foreach (var abstractPopUpScreen in this.m_PopUpScreens)
        {
            abstractPopUpScreen.Value.gameObject.SetActive(false);
        }
        this.m_ShowedPopUps.Clear();
        this.m_showingPopUp = ePopUpType.None;
    }

    public virtual IEnumerator SpecificInit()
    {
        yield break;
    }

    public IEnumerator SwitchToPopUp(ePopUpType i_PopUpType, object i_Args = null)
    {
        
        if (this.m_showingPopUp == ePopUpType.None)
        {
            Debug.LogError("Switch should be called only with active pop-ups!");
            throw new Exception("Bad call to Switch pop-up");
        }
        else
        {
            this.m_PopUpScreens[i_PopUpType].Reset(i_Args);
            this.BlockPopUpContent();
            this.m_PopUpScreens[i_PopUpType].gameObject.SetActive(true);
            this.m_PopUpScreens[i_PopUpType].Reset(i_Args);
            this.StartCoroutine(this.m_PopUpScreens[this.m_showingPopUp].Hide());
            yield return this.StartCoroutine(this.m_PopUpScreens[i_PopUpType].Show());
            this.m_PopUpScreens[this.m_showingPopUp].gameObject.SetActive(false);
            this.m_ShowedPopUps.Pop();
            this.m_ShowedPopUps.Push(i_PopUpType);         
            this.m_showingPopUp = i_PopUpType;
            this.UnBlockPopUpContent();
        }
        
        yield break;
    }

    public void Pause()
        {
            this.mr_Pause.SetActive(true);
        }

        public void UnPause()
        {
            this.mr_Pause.SetActive(false);
        }

    public void EnablePopUpManager()
    {
        this.m_ScreensHolder.transform.localPosition = new Vector3(0,0,-50);
        var oponedPopUps = this.m_ShowedPopUps.ToArray();
        foreach (ePopUpType oponedPopUp in oponedPopUps)
        {
            this.m_PopUpScreens[oponedPopUp].OnRestart();
        }
    }

    public void DisablePopUpManager()
    {
        this.m_ScreensHolder.transform.localPosition = new Vector3(3000, 3000, -50);
    }

    public ePopUpType GetCurrentPopUp()
    {
        if (this.m_ShowedPopUps.Count == 0)
        {
            return ePopUpType.None;
        }
        else
        {
            return this.m_ShowedPopUps.Peek();
        }
    }


    private void BlockPopUpContent()
    {
        this.mr_Pause.transform.localPosition = new Vector3(this.mr_Pause.transform.localPosition.x, this.mr_Pause.transform.localPosition.y, -200);
    }

    private void UnBlockPopUpContent()
    {
        this.mr_Pause.transform.localPosition = new Vector3(this.mr_Pause.transform.localPosition.x, this.mr_Pause.transform.localPosition.y, 0);
    }

    public bool IsActivePopUp()
    {
        return this.m_ShowedPopUps.Count > 0;
    }
}

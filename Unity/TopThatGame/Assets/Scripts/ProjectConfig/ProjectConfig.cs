﻿using System.Collections.Generic;


public class ProjectConfig  {


    public enum eProjectType
    {
        HSP_APK = 1,

        HSP_WEB  = 2,

        PORTAL = 3,

        LOCAL_APK = 4,

        LOCAL_WEB = 5,

        FACEBOOK = 6,

        GYM = 7
    }



    public string CurrentPlatform { get; set; }

    public List<string> SupportedPlatforms { get; set; }

    public string UnityVersion { get; set; }

    public string BuildVersion { get; set; }

}

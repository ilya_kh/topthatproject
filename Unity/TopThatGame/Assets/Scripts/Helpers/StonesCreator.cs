﻿using Newtonsoft.Json;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft;

public class StonesCreator : MonoBehaviour
{

    public BoardController mr_Board;

    public GameObject mr_WorldHolder;

    public tk2dSprite mr_BaseSprite;

    public Random Random = new Random();

    public readonly Dictionary<int, int> finishPositions = new Dictionary<int, int>()
                                                                    {
                                                                        {2,3},
                                                                        {3,5},
                                                                        {4,7},
                                                                        {5,9},
                                                                        {6,11},
                                                                        {7,13},
                                                                        {8,11},
                                                                        {9,9},
                                                                        {10,7},
                                                                        {11,5},
                                                                        {12,3}
                                                                    };
	// Use this for initialization
	IEnumerator Start ()
	{
	    //this.CreateJson();

	    foreach (var columnController in this.mr_Board.mr_ColumnControllers)
	    {
	        foreach (var cellController in columnController.mr_CellControllers)
	        {
	            GameObject markersHolder = new GameObject("Markers holder");
	            markersHolder.transform.parent = cellController.transform;
	            markersHolder.transform.localPosition = Vector3.zero;

	        }
	    }

	    string path = "file://" + Application.streamingAssetsPath + "/Stones.txt";
        Debug.Log("Path : " + path);
        WWW www = new WWW(path);
	    yield return www;
	    List<StoneLocation> locations = JsonConvert.DeserializeObject<List<StoneLocation>>(www.text);
	    foreach (var stoneLocation in locations)
	    {
            ColumnController columnController = this.mr_Board.mr_ColumnControllers.Find(m => m.m_ColumnId == stoneLocation.ColumnId);
            CellController cellController = columnController.mr_CellControllers.Find(m => m.mr_CellId == stoneLocation.CelldId);
            GameObject markersHodler = cellController.transform.FindChild("Markers holder").gameObject;


	        GameObject marker = new GameObject("Marker First");
	        marker.transform.parent = this.mr_WorldHolder.transform;
            marker.transform.localPosition = new Vector3(stoneLocation.FirstData.XPlace, -stoneLocation.FirstData.YPlace , -20);
	        if (cellController.mr_Markers == null)
	        {
                cellController.mr_Markers = new List<Marker>();
	        }
	        var component = marker.AddComponent<Marker>();
            this.AddTexture(component);
            cellController.mr_Markers.Add(component);
	        component.transform.parent = markersHodler.transform;

            GameObject markerSecond  = new GameObject("Marker Second");
            markerSecond.transform.parent = this.mr_WorldHolder.transform;
            markerSecond.transform.localPosition = new Vector3(stoneLocation.SecondData.XPlace, -stoneLocation.SecondData.YPlace, -20);
            if (cellController.mr_Markers == null)
            {
                cellController.mr_Markers = new List<Marker>();
            }
            var componentSecond = markerSecond.AddComponent<Marker>();
            this.AddTexture(componentSecond);
            cellController.mr_Markers.Add(componentSecond);
            componentSecond.transform.parent = markersHodler.transform;


	    }

	}


    private void AddTexture(Marker i_Marker)
    {
        
        GameObject sprite = new GameObject("Sprite");
        var spriteComponent = sprite.AddComponent<tk2dSprite>();
        int nextSprite = Random.Range(1, 6);
        spriteComponent.SetSprite(this.mr_BaseSprite.Collection, nextSprite.ToString());
        i_Marker.mr_Sprite = spriteComponent;
        sprite.transform.parent = i_Marker.transform;
        sprite.transform.localPosition = Vector3.zero;
    }

    private void CreateJson()
    {
        List<StoneLocation> locations = new List<StoneLocation>();
        for (int i = 2; i < 13; i++)
        {
            for (int j = 0; j < this.finishPositions[i]; j++)
            {
                StoneLocation stoneLocation = new StoneLocation();
                stoneLocation.ColumnId = i;
                stoneLocation.CelldId = j + 1;
                stoneLocation.FirstData = new StoneData();
                stoneLocation.SecondData = new StoneData();
                locations.Add(stoneLocation);
            }
        }
        Debug.Log(JsonConvert.SerializeObject(locations));
    }

    // Update is called once per frame
	void Update () {
	
	}
}

﻿using System;
using Assets.Scripts.View.ConcreteResponses;
using UnityEngine;
using System.Collections;
using System.Linq;

public class PanelController : MonoBehaviour
{

    #region Events 

    public event GameView.ActionResponse ActionResponse;


    #endregion 


    public BoardController.ePlayer mr_Player;

    public MagnetsController mr_MagnetsController;

    public InteractivePanelController m_InteractivePanelController;

    private Vector3 m_startLocation;

    private Vector3 slideAmount = new Vector3(0,200,0);

    private bool m_isPanelHidden = true;

    public GameObject mr_MagnetsHolder;

    // Use this for initialization
    void Awake()
    {
        this.m_startLocation = this.mr_MagnetsHolder.transform.localPosition;
        this.mr_MagnetsHolder.transform.localPosition += this.slideAmount;
        this.mr_MagnetsController.MagnetsChosenEvent += this.InteractivePanelControllerOnUserInteraction;
        this.mr_MagnetsController.MagnetsDisposedEvent += this.InteractivePanelControllerOnUserInteraction;
        this.m_InteractivePanelController.UserInteraction += this.InteractivePanelControllerOnUserInteraction;
        this.mr_MagnetsController.MagnetDraggedEvent += this.onMagnetsControllerOnMagnetDraggedEvent;
    }


    public void Reset()
    {
        this.m_isPanelHidden = true;
        this.mr_MagnetsHolder.transform.localPosition = new Vector3(this.mr_MagnetsHolder.transform.localPosition.x, 200, this.mr_MagnetsHolder.transform.localPosition.z);
        this.m_InteractivePanelController.mr_PanelPopUpManager.Reset();
    }

    public IEnumerator ShowOneByOne()
    {
        var orderedMagnets = this.mr_MagnetsController.mr_MagnetControllers.OrderBy(m => m.transform.localPosition.x);     
        this.mr_MagnetsHolder.transform.localPosition =  new Vector3(this.mr_MagnetsHolder.transform.localPosition.x, 0, this.mr_MagnetsHolder.transform.localPosition.z);
        foreach (var magnetController in orderedMagnets)
        {
            magnetController.transform.localPosition = magnetController.transform.localPosition + new Vector3(0, 140, 0);
        }
        for (int i = 0; i < orderedMagnets.Count(); i++)
        {
            yield return
                this.StartCoroutine(
                    GenericAnimator.Instance.MoveLocal(
                        orderedMagnets.ElementAt(i).gameObject,
                        orderedMagnets.ElementAt(i).transform.localPosition,
                        orderedMagnets.ElementAt(i).transform.localPosition + new Vector3(0, -140, 0),
                        0.15F));
        }
        this.mr_MagnetsHolder.GetComponentsInChildren<BoxCollider>().ToList().ForEach(m => m.enabled = true);
        this.m_isPanelHidden = false;
    }

    public IEnumerator Show()
    {
        if (!this.m_isPanelHidden)
        {
            yield break;
        }
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.RopesSlide, SoundManagerStaticInfo.eTags.Effects, false);
        yield return StartCoroutine(this.ShowOneByOne());
    }

    public IEnumerator HideMagnets(bool i_Immedeate = false)
    {
        if (this.m_isPanelHidden)
        {
            yield break;
        }

        this.mr_MagnetsHolder.GetComponentsInChildren<BoxCollider>().ToList().ForEach(m => m.enabled = false);


        yield return
            this.StartCoroutine(
                GenericAnimator.Instance.MoveLocal(
                    this.mr_MagnetsHolder,
                    this.mr_MagnetsHolder.transform.localPosition,
                    new Vector3(this.mr_MagnetsHolder.transform.localPosition.x, 200, this.mr_MagnetsHolder.transform.localPosition.z),
                    i_Immedeate  ? 0 : 0.6F));
        this.m_isPanelHidden = true;
    }

    private void onMagnetsControllerOnMagnetDraggedEvent()
    {
        this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.HidePopUp());
        this.mr_MagnetsController.EnableMagnets();
        if (this.ActionResponse != null)
        {
            this.ActionResponse(new StartPeek());
        }
    }

    private void InteractivePanelControllerOnUserInteraction(ViewEventAbstract i_ViewEventAbstract)
    {
        if (this.ActionResponse != null)
        {
            this.StartCoroutine(this.HandleUserEventEnumarator(i_ViewEventAbstract));
        }
    }

    private IEnumerator HandleUserEventEnumarator(ViewEventAbstract i_ViewEventAbstract)
    {
        yield return this.StartCoroutine(this.HideInteractivePanel());
        Debug.Log("<color=yellow>" + i_ViewEventAbstract.GetType() + "</color>");
        var actionResponse = this.ActionResponse(i_ViewEventAbstract);
        this.handleActionResponse(actionResponse);
    }


    private void handleActionResponse(ActionResponseAbstract i_ActionResponse)
    {
        if (i_ActionResponse == null)
        {
            return;
        }

        if (i_ActionResponse.GetType() == typeof(ShowPerformSimpleMoveActionResponse))
        {
            this.mr_MagnetsController.ClipMagnets();
            var response = i_ActionResponse as ShowPerformSimpleMoveActionResponse;
            if (response.OnlyOneMovement)
            {
                int playbleOption = response.PlayMove.FirstCombination == -1
                                        ? response.PlayMove.SecondCombination
                                        : response.PlayMove.FirstCombination;
                this.mr_MagnetsController.DisableIlegalPair(playbleOption);
            }

            this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.ShowMovePopUp());
        }
        if (i_ActionResponse.GetType() == typeof(ShowKeepOrPlayActionResponse))
        {

            this.StartCoroutine(this.showContinueOrKeepEnumarator());
        }
        if (i_ActionResponse.GetType() == typeof(ShowPeekPairActionResponse))
        {
            this.StartCoroutine(this.ShowStartMessage());
        }

        if (i_ActionResponse.GetType() == typeof(ShowIlegalMoveActionResponse))
        {
            this.mr_MagnetsController.reorderMagnets(null,false);
            SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.BadMove, SoundManagerStaticInfo.eTags.Effects, false);
            this.StartCoroutine(this.ShowStartMessage());
        }

        if (i_ActionResponse.GetType() == typeof(ShowPerformExclusiveMoveActionResponse))
        {
            this.mr_MagnetsController.ClipMagnets();
            this.StartCoroutine(this.showExclusiveMoveEnumarator(i_ActionResponse));
        }


        if (i_ActionResponse.GetType() == typeof(ShowNoPossibleMovesActionResponse))
        {
            this.StartCoroutine(this.showNoMovesPopUp());
        }
            
    }




    public IEnumerator HideInteractivePanel()
    {
        yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.HidePopUp());
    }


    private IEnumerator showContinueOrKeepEnumarator()
    {
        yield return this.StartCoroutine(this.HideMagnets());
        yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.HidePopUp());
        yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.ShowPlayOrKeppPopUp());
        yield break;
    }



    private IEnumerator showExclusiveMoveEnumarator(ActionResponseAbstract i_ActionResponse)
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.MagnetsIn, SoundManagerStaticInfo.eTags.Effects, false);
        this.mr_MagnetsController.EnableMagnets();
        ShowPerformExclusiveMoveActionResponse showPerformExclusive = i_ActionResponse as ShowPerformExclusiveMoveActionResponse;
        yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.HidePopUp());
        yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.ShowExlusiveMovePopUp(showPerformExclusive.Options));
        yield break;
    }

    private IEnumerator showNoMovesPopUp()
    {
        this.mr_MagnetsController.DisableMagnets();
        this.mr_MagnetsController.reorderMagnets(null, false);
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.RopesSlide, SoundManagerStaticInfo.eTags.Effects, false);
        yield return this.StartCoroutine(this.Show());
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.NoCombination, SoundManagerStaticInfo.eTags.Effects, false);
        yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.ShowNoMovesPopUp());
        yield break;
    }


    public IEnumerator ShowStartMessage(bool i_IsFirstTurn = false)
    {

        yield return this.StartCoroutine(this.ShowMagnetsPanel());
        if (!i_IsFirstTurn)
        {
            yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.Hide());
 
            yield return this.StartCoroutine(this.m_InteractivePanelController.mr_PanelPopUpManager.ShowIlegalChoicePopUp());
        }
    }

    private IEnumerator ShowMagnetsPanel()
    {
        this.mr_MagnetsController.reorderMagnets(null, false);
        this.mr_MagnetsController.EnableMagnets();
        yield return this.StartCoroutine(this.Show());
    }

    public IEnumerator StopTurn(bool i_Immedeate = false)
    {
       this.StartCoroutine(this.HideMagnets(i_Immedeate));
       yield return this.StartCoroutine(this.HideInteractivePanel());
       yield break;
    }

    public IEnumerator StartTurn(bool i_IsFirstTurn = false)
    {
        this.StartCoroutine(this.ShowStartMessage(i_IsFirstTurn));
        yield break;
    }


    #region Resign 

    public void ResignClicked()
    {
        if (this.ActionResponse != null)
        {
            this.ActionResponse(new ResignAsked(this.mr_Player));
        }
    }

    #endregion 
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

public class PersonsController : MonoBehaviour
{


    public List<PersonController> mr_PersonControllers;

    private string i_JsonFileName  = "Positions";

    private List<Vector3Serializer> m_PlayerPositions;


    // Use this for initialization
	IEnumerator Start () {

        TextAsset config = Resources.Load(i_JsonFileName) as TextAsset;

        /*
        string path;
        string assetsPath = Application.dataPath + "/StreamingAssets/";
        if (Application.isEditor)
        {
            path = "file:///" + assetsPath + i_JsonFileName;
        }
        else
        {
            path = assetsPath + i_JsonFileName;
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            path = Application.streamingAssetsPath + "/" + i_JsonFileName;
        }

        WWW www = new WWW(path);
        yield return www;*/
	    this.m_PlayerPositions = JsonConvert.DeserializeObject<List<Vector3Serializer>>(config.ToString());
        yield break;
	}


    public void SetPlayers(List<CellController> i_CellControllers)
    {
        foreach (var cellController in i_CellControllers)
        {
            var personController = this.mr_PersonControllers.Find(m => m.ColumnId == cellController.mr_ColumnId);
            if (personController != null)
            {
                var vector = this.getNewPosition(cellController);
                personController.SetPlayer(
                    cellController.mr_CurrentPlayer, cellController.mr_ColumnId, cellController.mr_CellId, vector);
            }
            else
            {
                var newPerson = this.mr_PersonControllers.Find(m => m.ColumnId == 0);
                var vector = this.getNewPosition(cellController);
                newPerson.SetPlayer(
                    cellController.mr_CurrentPlayer, cellController.mr_ColumnId, cellController.mr_CellId, vector);
            }
        }
    }


    public void Reset()
    {
        foreach (var personController in this.mr_PersonControllers)
        {
            personController.Reset();
        }
    }

    public IEnumerator DropPlayers()
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Bust, SoundManagerStaticInfo.eTags.Effects, false);
        foreach (var personController in this.mr_PersonControllers)
        {
            this.StartCoroutine(GenericAnimator.Instance.Fade(personController.gameObject, 1, 0, 0.6f));
        }
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.gameObject.transform.localPosition, this.gameObject.transform.localPosition + new Vector3(0, -800, 0), 0.6F));
        this.Reset();
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.gameObject.transform.localPosition, this.gameObject.transform.localPosition + new Vector3(0, 800, 0), 0.1F));
        foreach (var personController in this.mr_PersonControllers)
        {
            this.StartCoroutine(GenericAnimator.Instance.Fade(personController.gameObject, 0, 1, 0.1f));
        }
    }

    private Vector3Serializer getNewPosition(CellController i_CellController)
    {
        return this.m_PlayerPositions.Find(m => m.CellId == i_CellController.mr_CellId && m.ColumnId == i_CellController.mr_ColumnId);
    }




    #region Serializable Vector3

    [Serializable]
    public class Vector3Serializer
    {
        public float x;

        public float y;

        public float z;

        public int ColumnId;

        public int CellId;

        public void Fill(Vector3 v3)
        {
            x = v3.x;
            y = v3.y;
            z = v3.z;
        }

        public void SetCoordinaties(int i_ColumnId, int i_CellId)
        {
            this.ColumnId = i_ColumnId;
            this.CellId = i_CellId;
        }
    }

    #endregion 
}





﻿using System.Collections.Generic;

using UnityEngine;
using System.Collections;

public class RulesTexts : MonoBehaviour 
{
    public enum eTexts
    {
        Rule1Headline = 0,
        Rule1Body,
        Rule2Headline,
        Rule2Body,
        Rule3Headline,
        Rule3Body,
        Rule3Label1,
        Rule3Label2,
        Rule3Label3,
        Rule3Button1,
        Rule4Headline,
        Rule4Body,
        Rule5Headline,
        Rule5Body,
        Rule6Headline,
        Rule6Body,
        Rule6Label1,
        Rule6Label2,
        Rule6Button1,
        Rule7Headline,
        Rule7Body,
        Rule7Label1,
        Rule7Label2,
        Rule7Button1,
        Rule8Headline,
        Rule8Body,
        Rule8Label1,
        Rule8Label2,
        Rule8Button1,
        Rule9Headline,
        Rule9Body,
        Rule9Label1,
        Rule10Headline,
        Rule10Body,
        Rule10Label1,
        Rule10Label2,
        Rule10Button1,
        Rule10Button2,
        Rule11Headline,
        Rule11Body,
        Rule11Label1,
        Rule12Headline,
        Rule12Body,
        Rule13Headline,
        Rule13Body,
        Rule13Label1,
        Rule14Headline,
        Rule14Body,
        Rule15Headline,
        Rule15Body,
        Rule16Headline,
        Rule16Body,
        Rule16Button1,
        Rule17Headline,
        Rule17Body,
        Rule17Button1,
        Rule18Headline,
        Rule18Body,
        Rule19Headline,
        Rule19Body,
    }

    public List<MLTextMesh> mr_Texts;

    public void Init()
    {
        mr_Texts[(int)eTexts.Rule1Headline].ChangeText(TextService.GetTextById("3.3.3.1"));
        mr_Texts[(int)eTexts.Rule1Body].ChangeText(TextService.GetTextById("3.3.3.2"));

        mr_Texts[(int)eTexts.Rule2Headline].ChangeText(TextService.GetTextById("3.3.3.3"));
        mr_Texts[(int)eTexts.Rule2Body].ChangeText(TextService.GetTextById("3.3.3.4"));

        mr_Texts[(int)eTexts.Rule3Headline].ChangeText(TextService.GetTextById("3.3.3.5"));
        mr_Texts[(int)eTexts.Rule3Body].ChangeText(TextService.GetTextById("3.3.3.6"));
        mr_Texts[(int)eTexts.Rule3Label1].ChangeText(TextService.GetTextById("3.3.3.7"));
        mr_Texts[(int)eTexts.Rule3Label2].ChangeText(TextService.GetTextById("3.3.3.8"));
        mr_Texts[(int)eTexts.Rule3Label3].ChangeText(TextService.GetTextById("3.3.3.9"));
        mr_Texts[(int)eTexts.Rule3Button1].ChangeText(TextService.GetTextById("3.3.3.10"));

        mr_Texts[(int)eTexts.Rule4Headline].ChangeText(TextService.GetTextById("3.3.3.11"));
        mr_Texts[(int)eTexts.Rule4Body].ChangeText(TextService.GetTextById("3.3.3.12"));

        mr_Texts[(int)eTexts.Rule5Headline].ChangeText(TextService.GetTextById("3.3.3.13"));
        mr_Texts[(int)eTexts.Rule5Body].ChangeText(TextService.GetTextById("3.3.3.14"));

        mr_Texts[(int)eTexts.Rule6Headline].ChangeText(TextService.GetTextById("3.3.3.15"));
        mr_Texts[(int)eTexts.Rule6Body].ChangeText(TextService.GetTextById("3.3.3.16"));
        mr_Texts[(int)eTexts.Rule6Label1].ChangeText(TextService.GetTextById("3.3.3.17"));
        mr_Texts[(int)eTexts.Rule6Label2].ChangeText(TextService.GetTextById("3.3.3.18"));
        mr_Texts[(int)eTexts.Rule6Button1].ChangeText(TextService.GetTextById("3.3.3.19"));

        mr_Texts[(int)eTexts.Rule7Headline].ChangeText(TextService.GetTextById("3.3.3.20"));
        mr_Texts[(int)eTexts.Rule7Body].ChangeText(TextService.GetTextById("3.3.3.21"));
        mr_Texts[(int)eTexts.Rule7Label1].ChangeText(TextService.GetTextById("3.3.3.22"));
        mr_Texts[(int)eTexts.Rule7Label2].ChangeText(TextService.GetTextById("3.3.3.23"));
        mr_Texts[(int)eTexts.Rule7Button1].ChangeText(TextService.GetTextById("3.3.3.24"));

        mr_Texts[(int)eTexts.Rule8Headline].ChangeText(TextService.GetTextById("3.3.3.25"));
        mr_Texts[(int)eTexts.Rule8Body].ChangeText(TextService.GetTextById("3.3.3.26"));
        mr_Texts[(int)eTexts.Rule8Label1].ChangeText(TextService.GetTextById("3.3.3.27"));
        mr_Texts[(int)eTexts.Rule8Label2].ChangeText(TextService.GetTextById("3.3.3.28"));
        mr_Texts[(int)eTexts.Rule8Button1].ChangeText(TextService.GetTextById("3.3.3.29"));

        mr_Texts[(int)eTexts.Rule9Headline].ChangeText(TextService.GetTextById("3.3.3.30"));
        mr_Texts[(int)eTexts.Rule9Body].ChangeText(TextService.GetTextById("3.3.3.31"));
        mr_Texts[(int)eTexts.Rule9Label1].ChangeText(TextService.GetTextById("3.3.3.32"));

        mr_Texts[(int)eTexts.Rule10Headline].ChangeText(TextService.GetTextById("3.3.3.33"));
        mr_Texts[(int)eTexts.Rule10Body].ChangeText(TextService.GetTextById("3.3.3.34"));
        mr_Texts[(int)eTexts.Rule10Label1].ChangeText(TextService.GetTextById("3.3.3.35"));
        mr_Texts[(int)eTexts.Rule10Label2].ChangeText(TextService.GetTextById("3.3.3.36"));
        mr_Texts[(int)eTexts.Rule10Button1].ChangeText(TextService.GetTextById("3.3.3.37"));
        mr_Texts[(int)eTexts.Rule10Button2].ChangeText(TextService.GetTextById("3.3.3.38"));

        mr_Texts[(int)eTexts.Rule11Headline].ChangeText(TextService.GetTextById("3.3.3.39"));
        mr_Texts[(int)eTexts.Rule11Body].ChangeText(TextService.GetTextById("3.3.3.40"));
        mr_Texts[(int)eTexts.Rule11Label1].ChangeText(TextService.GetTextById("3.3.3.41"));

        mr_Texts[(int)eTexts.Rule12Headline].ChangeText(TextService.GetTextById("3.3.3.42"));
        mr_Texts[(int)eTexts.Rule12Body].ChangeText(TextService.GetTextById("3.3.3.43"));

        mr_Texts[(int)eTexts.Rule13Headline].ChangeText(TextService.GetTextById("3.3.3.44"));
        mr_Texts[(int)eTexts.Rule13Body].ChangeText(TextService.GetTextById("3.3.3.45"));
        mr_Texts[(int)eTexts.Rule13Label1].ChangeText(TextService.GetTextById("3.3.3.46"));

        mr_Texts[(int)eTexts.Rule14Headline].ChangeText(TextService.GetTextById("3.3.3.47"));
        mr_Texts[(int)eTexts.Rule14Body].ChangeText(TextService.GetTextById("3.3.3.48"));

        mr_Texts[(int)eTexts.Rule15Headline].ChangeText(TextService.GetTextById("3.3.3.49"));
        mr_Texts[(int)eTexts.Rule15Body].ChangeText(TextService.GetTextById("3.3.3.50"));

        mr_Texts[(int)eTexts.Rule16Headline].ChangeText(TextService.GetTextById("3.3.3.51"));
        mr_Texts[(int)eTexts.Rule16Body].ChangeText(TextService.GetTextById("3.3.3.52"));
        mr_Texts[(int)eTexts.Rule16Button1].ChangeText(TextService.GetTextById("3.3.3.53"));

        mr_Texts[(int)eTexts.Rule17Headline].ChangeText(TextService.GetTextById("3.3.3.54"));
        mr_Texts[(int)eTexts.Rule17Body].ChangeText(TextService.GetTextById("3.3.3.55"));
        mr_Texts[(int)eTexts.Rule17Button1].ChangeText(TextService.GetTextById("3.3.3.56"));

        mr_Texts[(int)eTexts.Rule18Headline].ChangeText(TextService.GetTextById("3.3.3.57"));
        mr_Texts[(int)eTexts.Rule18Body].ChangeText(TextService.GetTextById("3.3.3.58"));

        mr_Texts[(int)eTexts.Rule19Headline].ChangeText(TextService.GetTextById("3.3.3.59"));
        mr_Texts[(int)eTexts.Rule19Body].ChangeText(TextService.GetTextById("3.3.3.60"));
    }

}

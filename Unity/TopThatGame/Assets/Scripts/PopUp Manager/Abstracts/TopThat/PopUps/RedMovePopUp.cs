﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts.TopThat.PopUps
{
    public class RedMovePopUp : TopThatPanelPopUp
    {
        public MLTextMesh mr_ButtonText;

        public override void Reset(object i_Args)
        {
            this.mr_ButtonText.ChangeText(TextService.GetTextById("1-0-0"));
        }



        public override System.Collections.IEnumerator Hide()
        {
            this.gameObject.SetActive(false);
            yield break;
        }



        public void PlayWasClicked()
        {
            this.SendUserInteractionEvent(new PlayMove());
        }
    }
}

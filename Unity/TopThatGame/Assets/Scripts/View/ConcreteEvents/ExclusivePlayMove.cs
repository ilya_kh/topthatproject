﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.View.ConcreteEvents
{
    public class ExclusivePlayMove : ViewEventAbstract
    {
        public readonly int SelectedOptionId;

        public ExclusivePlayMove(int i_SelectedOption)
        {
            this.SelectedOptionId = i_SelectedOption;
        }
    }
}

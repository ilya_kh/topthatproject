﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts
{
    using UnityEngine;

    public abstract class AbstractContentPopUp : AbstractPopUpScreen
    {
        public UIButton mr_Button;

        public TextMesh mr_Header;

        public TextMesh mr_Body;

        public TextMesh mr_ButtonText;

        public override System.Collections.IEnumerator Show()
        {
            return base.Show();
        }

        public override System.Collections.IEnumerator Hide()
        {
            return base.Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance
    {
        get
        {
            return s_instance;
        }
    }

    private const string k_configFileName = "SoundsConfig";
    
    private const string k_soundsFilePathPrefix = "Sounds/";

    private static SoundManager s_instance;

    private static Dictionary<string, List<SoundInfo>> s_soundInfos;

    private static Dictionary<SoundManagerStaticInfo.eSoundIds, string> s_soundIdsToNames;

    private static Dictionary<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> s_soundsByTags;

    private static Dictionary<SoundManagerStaticInfo.eSoundIds, SoundManagerAudioSourceConfig> s_audioConfigByIds;

    private static Dictionary<SoundManagerStaticInfo.eTags, SoundManagerAudioSourceConfig> s_audioConfigByTags;

    private static bool s_isInitializingComplete;

    private static Transform s_soundsHolder;

    public static bool IsInitializingComplete
    {
        get
        {
            return s_isInitializingComplete;
        }
    }

    public void Awake()
    {
        if (s_instance == null)
        {
            s_instance = this;
            Instance.initializeEnumerator();
        }
        else
        {
            return;
        }
    }

    private void initializeEnumerator()
    {
        s_isInitializingComplete = false;

        GameObject soundsHolder = new GameObject("Sounds Holder");
        s_soundsHolder = soundsHolder.transform;

        s_soundsByTags = new Dictionary<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>>();
        s_audioConfigByIds = new Dictionary<SoundManagerStaticInfo.eSoundIds, SoundManagerAudioSourceConfig>();
        s_audioConfigByTags = new Dictionary<SoundManagerStaticInfo.eTags, SoundManagerAudioSourceConfig>();

        this.parseConfigJson();
        this.finishInitializeFromConfig();

        s_isInitializingComplete = true;
    }

    private void finishInitializeFromConfig()
    {
        StringBuilder pathBuilder = new StringBuilder("");
        StringBuilder nameBuilder;
        SoundManagerStaticInfo.eSoundIds soundId;

        s_soundIdsToNames = new Dictionary<SoundManagerStaticInfo.eSoundIds, string>();
        s_audioConfigByIds = new Dictionary<SoundManagerStaticInfo.eSoundIds, SoundManagerAudioSourceConfig>();

        foreach (KeyValuePair<string, List<SoundInfo>> pair in s_soundInfos)
        {
            pathBuilder = new StringBuilder("");
            pathBuilder.Append(k_soundsFilePathPrefix);
            pathBuilder.Append(pair.Key);

            foreach (SoundInfo info in pair.Value)
            {
                soundId = (SoundManagerStaticInfo.eSoundIds)Enum.Parse(typeof(SoundManagerStaticInfo.eSoundIds), info.SoundId);

                nameBuilder = new StringBuilder(pathBuilder.ToString());
                nameBuilder.Append(info.SoundFileName);

                s_soundIdsToNames.Add(soundId, nameBuilder.ToString());
                SoundManagerAudioSourceConfig config = new SoundManagerAudioSourceConfig();
                config.Volume = info.Volume;
                config.Pitch = info.Pitch;
                s_audioConfigByIds.Add(soundId, config);
            }
        }
        s_soundInfos = null;
    }

    #region Parse

    private void parseConfigJson()
    {
        TextAsset config = Resources.Load(k_configFileName) as TextAsset;

        Dictionary<string, List<SoundInfo>> json = JsonConvert.DeserializeObject<Dictionary<string, List<SoundInfo>>>(config.ToString());
        JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();

        if (json != null)
        {
            print("JSON parsing succeeded.");
            s_soundInfos = json;
        }
        else
        {
            print("JSON parsing failed.");
        }

        
    }

    #endregion

    public static void PlaySound(SoundManagerStaticInfo.eSoundIds i_Id, SoundManagerStaticInfo.eTags i_Tag, bool i_IsLoop)
    {
        GameObject newThing = new GameObject(Enum.GetName(typeof(SoundManagerStaticInfo.eSoundIds), i_Id));
        AudioSource source = newThing.AddComponent<AudioSource>();
        SoundManagerSoundInfo soundInfo;

        newThing.transform.parent = s_soundsHolder;
        if (s_soundIdsToNames.ContainsKey(i_Id) == false)
        {
            Debug.LogError("Sound Manager : Not found sound " + i_Id);
            return;
        }
        source.clip = Resources.Load(s_soundIdsToNames[i_Id]) as AudioClip;
        if (s_soundsByTags.ContainsKey(i_Tag) == false)
        {
            s_soundsByTags.Add(i_Tag, new List<SoundManagerSoundInfo>());
        }
        soundInfo = new SoundManagerSoundInfo(i_Id, source);
        s_soundsByTags[i_Tag].Add(soundInfo);

        if (i_IsLoop)
        {
            source.loop = true;
        }

        // volume and pitch control
        changeSoundPitchByTag(i_Id, i_Tag, source);
        changeSoundVolumeByTag(i_Id, i_Tag, source);

        source.Play();

        if (i_IsLoop == false)
        {
            Instance.StartCoroutine(destroySoundOnFinish(soundInfo, i_Tag));
        }
    }

    private static IEnumerator destroySoundOnFinish(SoundManagerSoundInfo i_Sound, SoundManagerStaticInfo.eTags i_Tag)
    {
        while (i_Sound.AudioSource.isPlaying)
        {

            yield return 0;
        }
        s_soundsByTags[i_Tag].Remove(i_Sound);
        Destroy(i_Sound.AudioSource.gameObject);
    }

    public static List<AudioSource> GetAllSoundsByTag(SoundManagerStaticInfo.eTags i_Tag)
    {
        List<AudioSource> result = new List<AudioSource>();
        if (s_soundsByTags.ContainsKey(i_Tag))
        {
            foreach (SoundManagerSoundInfo info in s_soundsByTags[i_Tag])
            {
                result.Add(info.AudioSource);
            }
        }
        return result;
    }

    private static void changeSoundVolumeByTag(SoundManagerStaticInfo.eSoundIds i_Id, SoundManagerStaticInfo.eTags i_Tag, AudioSource i_Source)
    {
        float volumeFactor = 1;

        if (s_audioConfigByTags.ContainsKey(i_Tag))
        {
            volumeFactor = s_audioConfigByTags[i_Tag].Volume;
        }

        i_Source.volume = volumeFactor * s_audioConfigByIds[i_Id].Volume;
    }

    private static void changeSoundPitchByTag(SoundManagerStaticInfo.eSoundIds i_Id, SoundManagerStaticInfo.eTags i_Tag, AudioSource i_Source)
    {
        float pitchFactor = 1;

        if (s_audioConfigByTags.ContainsKey(i_Tag))
        {
            pitchFactor = s_audioConfigByTags[i_Tag].Pitch;
        }

        i_Source.pitch = pitchFactor * s_audioConfigByIds[i_Id].Pitch;
    }

    public static void SetVolumeForSoundId(SoundManagerStaticInfo.eSoundIds i_SoundId, float i_Volume)
    {
        if (s_audioConfigByIds.ContainsKey(i_SoundId) == false)
        {
            s_audioConfigByIds.Add(i_SoundId, new SoundManagerAudioSourceConfig());
        }
        s_audioConfigByIds[i_SoundId].Volume = i_Volume;

        foreach (KeyValuePair<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> pair in s_soundsByTags)
        {
            foreach (SoundManagerSoundInfo info in pair.Value)
            {
                if (info.SoundId == i_SoundId)
                {
                    changeSoundVolumeByTag(info.SoundId, pair.Key, info.AudioSource);
                }
            }
        }
    }

    public static void SetPitchForSoundId(SoundManagerStaticInfo.eSoundIds i_SoundId, float i_Pitch)
    {
        if (s_audioConfigByIds.ContainsKey(i_SoundId) == false)
        {
            s_audioConfigByIds.Add(i_SoundId, new SoundManagerAudioSourceConfig());
        }
        s_audioConfigByIds[i_SoundId].Pitch = i_Pitch;

        foreach (KeyValuePair<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> pair in s_soundsByTags)
        {
            foreach (SoundManagerSoundInfo info in pair.Value)
            {
                if (info.SoundId == i_SoundId)
                {
                    changeSoundPitchByTag(info.SoundId, pair.Key, info.AudioSource);
                }
            }
        }
    }

    public static void SetPitchForTag(SoundManagerStaticInfo.eTags i_Tag, float i_Pitch)
    {
        if (s_audioConfigByTags.ContainsKey(i_Tag) == false)
        {
            s_audioConfigByTags.Add(i_Tag, new SoundManagerAudioSourceConfig());
        }
        s_audioConfigByTags[i_Tag].Pitch = i_Pitch;

        foreach (KeyValuePair<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> pair in s_soundsByTags)
        {
            if (pair.Key == i_Tag)
            {
                foreach (SoundManagerSoundInfo info in pair.Value)
                {
                    changeSoundPitchByTag(info.SoundId, i_Tag, info.AudioSource);
                }
            }
        }
    }

    public static void SetVolumeForTag(SoundManagerStaticInfo.eTags i_Tag, float i_Volume)
    {
        if (s_audioConfigByTags.ContainsKey(i_Tag) == false)
        {
            s_audioConfigByTags.Add(i_Tag, new SoundManagerAudioSourceConfig());
        }
        s_audioConfigByTags[i_Tag].Volume = i_Volume;

        foreach (KeyValuePair<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> pair in s_soundsByTags)
        {
            if (pair.Key == i_Tag)
            {
                foreach (SoundManagerSoundInfo info in pair.Value)
                {
                    changeSoundVolumeByTag(info.SoundId, i_Tag, info.AudioSource);
                }
            }
        }
    }


    public static bool IsTagActive(SoundManagerStaticInfo.eTags i_Tag)
    {

        return !s_audioConfigByTags[i_Tag].IsMuted;
    }

    public static void SetIsMutedForTag(SoundManagerStaticInfo.eTags i_Tag, bool i_IsMuted)
    {
        float newVolume;
        if (s_audioConfigByTags.ContainsKey(i_Tag) == false)
        {
            s_audioConfigByTags.Add(i_Tag, new SoundManagerAudioSourceConfig());
        }

        // to prevent bugs, cant muted twice
        if (s_audioConfigByTags[i_Tag].IsMuted && i_IsMuted)
        {
            return;
        }

        s_audioConfigByTags[i_Tag].IsMuted = i_IsMuted;
        if (i_IsMuted)
        {
            newVolume = 0;
            s_audioConfigByTags[i_Tag].VolumeToReturnToAfterMute = s_audioConfigByTags[i_Tag].Volume;
        }
        else
        {
            newVolume = s_audioConfigByTags[i_Tag].VolumeToReturnToAfterMute;
        }

        SetVolumeForTag(i_Tag, newVolume);
    }

    public static void SetIsMutedForSoundId(SoundManagerStaticInfo.eSoundIds i_SoundId, bool i_IsMuted)
    {
        float newVolume;
        if (s_audioConfigByIds.ContainsKey(i_SoundId) == false)
        {
            s_audioConfigByIds.Add(i_SoundId, new SoundManagerAudioSourceConfig());
        }

        // to prevent bugs, cant muted twice
        if (s_audioConfigByIds[i_SoundId].IsMuted && i_IsMuted)
        {
            return;
        }

        s_audioConfigByIds[i_SoundId].IsMuted = i_IsMuted;
        if (i_IsMuted)
        {
            newVolume = 0;
            s_audioConfigByIds[i_SoundId].VolumeToReturnToAfterMute = s_audioConfigByIds[i_SoundId].Volume;
        }
        else
        {
            newVolume = s_audioConfigByIds[i_SoundId].VolumeToReturnToAfterMute;
        }

        SetVolumeForSoundId(i_SoundId, newVolume);
    }

    public static void StopAllSoundsByTag(SoundManagerStaticInfo.eTags i_Tag)
    {
        List<SoundManagerSoundInfo> infosToRemove = new List<SoundManagerSoundInfo>();

        if (s_soundsByTags.ContainsKey(i_Tag) == false)
        {
            return;
        }

        foreach (KeyValuePair<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> pair in s_soundsByTags)
        {
            if (pair.Key == i_Tag)
            {
                foreach (SoundManagerSoundInfo info in pair.Value)
                {
                    if (info.AudioSource.loop)
                    {
                        infosToRemove.Add(info);
                    }
                    info.AudioSource.Stop();
                }
            }
        }

        foreach (SoundManagerSoundInfo info in infosToRemove)
        {
            s_soundsByTags[i_Tag].Remove(info);
        }
    }

    public static void StopAllSoundsById(SoundManagerStaticInfo.eSoundIds i_SoundId)
    {
        Dictionary<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> tagsDictionary = new Dictionary<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>>();

        foreach (KeyValuePair<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> pair in s_soundsByTags)
        {
            foreach (SoundManagerSoundInfo info in pair.Value)
            {
                if (info.SoundId == i_SoundId)
                {
                    if (info.AudioSource.loop)
                    {
                        if (tagsDictionary.ContainsKey(pair.Key) == false)
                        {
                            tagsDictionary.Add(pair.Key, new List<SoundManagerSoundInfo>());
                        }
                        tagsDictionary[pair.Key].Add(info);
                    }
                    info.AudioSource.Stop();
                }
            }
        }

        foreach (KeyValuePair<SoundManagerStaticInfo.eTags, List<SoundManagerSoundInfo>> pair in tagsDictionary)
        {
            foreach (SoundManagerSoundInfo info in pair.Value)
            {
                s_soundsByTags[pair.Key].Remove(info);
            }
        }
    }
}

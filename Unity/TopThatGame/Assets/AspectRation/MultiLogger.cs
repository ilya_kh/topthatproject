﻿using UnityEngine;
using System.Collections;

public class MultiLogger : MonoBehaviour {

    #region Web Player Log

        public static void Multilog(string i_Message)
        {
            if (Application.isEditor || Application.isWebPlayer)
            {
                logInUnityAndBrowser(i_Message);
            }
        }

        /// <summary>
        /// Debug Log entry in Unity and also in browser console.
        /// </summary>
        /// <param name="i_Message">
        /// The message.
        /// </param>
        private static void logInUnityAndBrowser(string i_Message)
        {
            print(i_Message);
            Application.ExternalCall("console.log", i_Message);
        }

    #endregion
}

﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ColumnController : MonoBehaviour
{

    public int m_ColumnId;

    public List<CellController> mr_CellControllers;

    private BoardController.ePlayer m_ConquredPlayer = BoardController.ePlayer.None;


    public void MakeColumnUnAvaliable()
    {
        if (this.m_ConquredPlayer != BoardController.ePlayer.None)
        {
            return;
        }

        foreach (var cellController in this.mr_CellControllers)
        {
            cellController.MarkCellAsUnAvaliable();
        }
    }


    public void MakeColumnHighlated()
    {
        foreach (var cellController in this.mr_CellControllers)
        {
            cellController.HighlightCell();
        }
    }


    public void MakeColumnConquared(BoardController.ePlayer i_Player)
    {

        if (this.m_ConquredPlayer != BoardController.ePlayer.None)
        {
            return;
        }
        this.m_ConquredPlayer = i_Player;
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.ConqureRoot, SoundManagerStaticInfo.eTags.Effects, false);
        this.StartCoroutine(this.ConquareRoot(i_Player));
    }


    public IEnumerator ConquareRoot(BoardController.ePlayer i_Player)
    {
        foreach (var cellController in this.mr_CellControllers.OrderBy(m=>m.mr_CellId))
        {
            cellController.ChangeCellVisulalState(i_Player);
            yield return new WaitForSeconds(0.05f);
        }
    }

    public IEnumerator UnConquareRoot()
    {
        foreach (var cellController in this.mr_CellControllers.OrderByDescending(m => m.mr_CellId))
        {
            cellController.ChangeCellVisulalState(BoardController.ePlayer.None);
            yield return new WaitForSeconds(0.05f);
        }
    }


    public IEnumerator WinAnimation(BoardController.ePlayer i_Player, float i_DurationTime , float i_ExchangeTime)
    {
        float endTime = Time.time + i_DurationTime;
        while (Time.time < endTime)
        {
            foreach (var cellController in this.mr_CellControllers.OrderByDescending(m => m.mr_CellId))
            {
                cellController.ChangeCellVisulalState(i_Player);
            }
            yield return new WaitForSeconds(i_ExchangeTime);
            foreach (var cellController in this.mr_CellControllers.OrderByDescending(m => m.mr_CellId))
            {
                cellController.ChangeCellVisulalState(BoardController.ePlayer.None);
            }
            yield return new WaitForSeconds(i_ExchangeTime);
        }

        foreach (var cellController in this.mr_CellControllers.OrderByDescending(m => m.mr_CellId))
        {
            cellController.ChangeCellVisulalState(i_Player);
        }
    }

    public IEnumerator WinAnimation(BoardController.ePlayer i_Player)
    {
        if (this.m_ConquredPlayer != i_Player)
        {
            yield break;
        }

        yield return this.StartCoroutine(this.UnConquareRoot());
        yield return this.StartCoroutine(this.ConquareRoot(i_Player));
        yield return this.StartCoroutine(this.UnConquareRoot());
        yield return this.StartCoroutine(this.ConquareRoot(i_Player));
    }


    public void Reset()
    {
        this.m_ConquredPlayer = BoardController.ePlayer.None;
        foreach (var cellController in this.mr_CellControllers.OrderByDescending(m => m.mr_CellId))
        {
            cellController.ClearCell();
        }
    }


    public bool IsColumnWillBeConquredFirst()
    {
        return this.m_ConquredPlayer == BoardController.ePlayer.None;
    }



    public void ClearCells()
    {
        if (this.m_ConquredPlayer != BoardController.ePlayer.None)
        {
            return;
        }
        foreach (var cellController in this.mr_CellControllers)
        {
            cellController.ClearCell();
        }
    }


    public BoardController.ePlayer GetConauredPlayer()
    {
        return this.m_ConquredPlayer;
    }
}

﻿using UnityEngine;

public class InteractivePanelController : MonoBehaviour
{
    public event GameView.UserInteraction UserInteraction;

    public PanelPopUpManager mr_PanelPopUpManager;

    private void Awake()
    {
        this.mr_PanelPopUpManager.UserInteractionEvent += this.PanelPopUpManagerOnUserInteractionEvent;
    }

    private void PanelPopUpManagerOnUserInteractionEvent(ViewEventAbstract i_ViewEventAbstract)
    {
        if (this.UserInteraction != null)
        {
            this.UserInteraction(i_ViewEventAbstract);
        }
    }
}

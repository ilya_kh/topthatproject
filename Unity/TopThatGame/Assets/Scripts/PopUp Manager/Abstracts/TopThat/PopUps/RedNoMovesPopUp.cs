﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts.TopThat.PopUps
{
    using Assets.Scripts.View.ConcreteEvents;

    using UnityEngine;

    public class RedNoMovesPopUp : TopThatPanelPopUp
    {

        public MLTextMesh mr_Text;

        public MLTextMesh mr_Header;

        public MLTextMesh mr_ButtonText;

        private Vector3 m_slideStartPosition;

        private readonly Vector3 m_SlideAmount = new Vector3(350,0,0);

        private Vector3 m_StartPosition;

        private void Awake()
        {
            this.m_StartPosition = this.transform.localPosition;
            if (this.m_PopUpType == PopUpManager.ePopUpType.Purple_NoLegalMovesPopUp)
            {
                this.m_slideStartPosition = this.transform.localPosition + this.m_SlideAmount;
            }

            if (this.m_PopUpType == PopUpManager.ePopUpType.Red_NoLegalMovesPopUp)
            {
                this.m_slideStartPosition = this.transform.localPosition - this.m_SlideAmount;
            }
        }

        public override void Reset(object i_Args)
        {
            this.mr_Text.ChangeText(TextService.GetTextById("1-0-4"));
            if (this.m_PopUpType == PopUpManager.ePopUpType.Purple_NoLegalMovesPopUp)
            {
                this.mr_Header.ChangeText(TextService.GetTextById("1-0-5-1"));
            }
            else
            {
                this.mr_Header.ChangeText(TextService.GetTextById("1-0-5-2"));
            }
            this.mr_ButtonText.ChangeText(TextService.GetTextById("1-0-6"));
        }

        public void OkClicked()
        {
            this.SendUserInteractionEvent(new ForteitMove());
        }


        public override System.Collections.IEnumerator Show()
        {
           
            this.transform.localPosition = this.m_slideStartPosition;
            yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0));
            yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.transform.localPosition, this.m_StartPosition, 0.5F));
            //yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.transform.localPosition, this.m_StartPosition, 0.2F));
        }
    }
}

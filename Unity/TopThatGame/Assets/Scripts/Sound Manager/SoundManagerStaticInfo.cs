﻿
public class SoundManagerStaticInfo
{
    public enum eSoundIds
    {


        /*PopUps*/
        SuccsesMusic,

        Earthquake,

        Equipment,

        Whoosh,

        SuccessWhoosh,

        Click,

        FadeInOut,

        MiniWhoosh,

        TightFabric,

        WhooshOut,

        /*Main*/

        MainMusic,

        MusicRope,

        Whoosh3,

        TightRope,

        StoneSound,
        /*Game*/
        Bust,

        MagnetsIn,

        MagnetsOut,

        RopesSlide,

        TurnIn,

        BadMove,

        StartClimb,

        Climb,

        ButtonIn,

        ConqureRoot,

        NoCombination,

        WinGame ,

        MenuButtons 
 
    }

    public enum eTags
    {
        Effects,
        Music
    }
}

﻿using System;
using System.Collections;
using Assets.Scripts.View.ConcreteEvents;
using Assets.Scripts.View.ConcreteResponses;
using TopThat;
using UnityEngine;

public class GameView : MonoBehaviour
{

    #region Events 

    public delegate void GameWasEnded(BoardController.ePlayer i_WonPlayer, bool i_IsGameWasResigned = false);

    public delegate void UserInteraction(ViewEventAbstract i_ViewEventAbstract);

    public delegate ActionResponseAbstract ActionResponse(ViewEventAbstract i_ViewEventAbstract);

    public delegate void BackToMainRequested();

    public event GameWasEnded GameWasEndedEvent;

    public event BackToMainRequested BackToMainRequestedEvent;

    #endregion 

    private GameViewModel m_gameViewModel;

    public BoardController mr_BoardController;

    public PanelsController mr_PanelsController;

    public GamePopUpManager m_PopUpManager;

    public TurnIndicator mr_TurnIndicator;

    public MenuController mr_MenuController;

    public GameObject mr_Pause;

    private BoardController.ePlayer m_StartingPlayer = BoardController.ePlayer.Purple;


    private void Start()
    {
        Debug.Log("Ilya added this method in sub-moudle");
    }

    public IEnumerator Init ()
    {
        this.m_gameViewModel = new GameViewModel(new TopThatGame(), this.mr_BoardController);
        this.mr_PanelsController.ActionResponse += this.PanelsControllerOnActionResponse;
        yield return this.StartCoroutine(this.mr_MenuController.Init(this.m_PopUpManager));
        this.setListnersToGamePopUpManagerEvents();
    }


    public void Reset()
    {
        this.mr_BoardController.Reset();
        this.mr_PanelsController.Reset();
    }

    public IEnumerator StartGame()
    {
        
        this.mr_Pause.SetActive(true);
        this.decideTurn();
        this.m_gameViewModel.StartGame(this.m_StartingPlayer);
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        this.mr_BoardController.DrawState(boardState,true);
        yield return this.StartCoroutine(this.firstTurnRoutine());
        this.mr_Pause.SetActive(false);

    }


    private void setListnersToGamePopUpManagerEvents()
    {
        this.m_PopUpManager.GameWasEndedEvent += this.onPopUpManagerOnGameWasEndedEvent;
        this.m_PopUpManager.BackToMainEvent += this.onPopUpManagerOnBackToMainEvent;
        this.m_PopUpManager.PopUpStartedEvent += this.onPopUpManagerOnPopUpStartedEvent;
        this.m_PopUpManager.PopUpEndedEvent += this.onPopUpManagerOnPopUpEndedEvent;
    }

    private void onPopUpManagerOnPopUpEndedEvent()
    {
        this.mr_Pause.SetActive(false);
    }

    private void onPopUpManagerOnPopUpStartedEvent()
    {
        this.mr_Pause.SetActive(true);
    }

    private void onPopUpManagerOnBackToMainEvent()
    {
        if (this.BackToMainRequestedEvent != null)
        {
            this.BackToMainRequestedEvent();
        }
    }

    private void onPopUpManagerOnGameWasEndedEvent(BoardController.ePlayer i_WonPlayer, bool i_IsGameWasResigned)
    {
        if (this.GameWasEndedEvent != null)
        {
            this.GameWasEndedEvent(i_WonPlayer, i_IsGameWasResigned);
        }
    }

    private void decideTurn()
    {
        if (this.m_StartingPlayer == BoardController.ePlayer.Red)
        {
            this.m_StartingPlayer = BoardController.ePlayer.Purple;
        }
        else if (this.m_StartingPlayer == BoardController.ePlayer.Purple)
        {
            this.m_StartingPlayer = BoardController.ePlayer.Red;
        }
    }

    private IEnumerator firstTurnRoutine()
    {
        yield return this.StartCoroutine(this.StartTurn(false, true));
        yield return this.StartCoroutine(this.mr_TurnIndicator.ShowTurn(this.m_gameViewModel.GetCurrentTurn()));
        this.StartCoroutine(this.StartTurn());
    }

    private void SetCubes(BoardController.ePlayer i_Player)
    {
        var choice = this.m_gameViewModel.GetRandomChoice();
        this.StartCoroutine(this.mr_PanelsController.SetCubes(i_Player, choice.Choice));
    }

    private ActionResponseAbstract PanelsControllerOnActionResponse(ViewEventAbstract i_ViewEventAbstract)
    {
        Debug.Log("<color=green>" + i_ViewEventAbstract.GetType()+ "</color>");

        if (i_ViewEventAbstract.GetType() == typeof(PeekPair))
        {
            return this.handlePeekMoveEvent(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(BreakPair))
        {
            return this.handleBreakMoveEvent(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(PlayMove))
        {
            return this.handlePlayMove(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(ContinueMove))
        {
            return this.handleContinueMove(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(KeepMove))
        {
            return this.handleKeepMove(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(ExclusivePlayMove))
        {
            return this.handleExlusivePlayMove(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(ForteitMove))
        {
            return this.handleForteitMove(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(StartPeek))
        {
            return this.handleFStartPeek(i_ViewEventAbstract);
        }
        if (i_ViewEventAbstract.GetType() == typeof(ResignAsked))
        {
            return this.handleFResignAsked(i_ViewEventAbstract);
        }
        return null;
    }

    private ActionResponseAbstract handleFResignAsked(ViewEventAbstract i_ViewEventAbstract)
    {
        ResignAsked resignAsked = i_ViewEventAbstract as ResignAsked;
        this.StartCoroutine(this.m_PopUpManager.ShowPopUp((PopUpManager.ePopUpType.Resign) , new ResignState(resignAsked.PlayerAsked)));
        return null;
    }



    private ActionResponseAbstract handleFStartPeek(ViewEventAbstract i_ViewEventAbstract)
    {
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        this.mr_BoardController.DrawState(boardState);
        return null;
    }

    private ActionResponseAbstract handleExlusivePlayMove(ViewEventAbstract i_ViewEventAbstract)
    {
        ExclusivePlayMove exclusivePlayMove = i_ViewEventAbstract as ExclusivePlayMove;
        this.m_gameViewModel.PerformExclusiveMove(exclusivePlayMove.SelectedOptionId);
        this.SetCubes(this.m_gameViewModel.GetCurrentTurn());
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        this.mr_BoardController.DrawState(boardState);   
        return new ShowKeepOrPlayActionResponse();
    }

    private ActionResponseAbstract handlePlayMove(ViewEventAbstract i_ViewEventAbstract)
    {
        this.m_gameViewModel.PerformMove();
        this.SetCubes(this.m_gameViewModel.GetCurrentTurn());
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        this.mr_BoardController.DrawState(boardState);
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Climb, SoundManagerStaticInfo.eTags.Effects, false);
        return new ShowKeepOrPlayActionResponse();
    }


    private ActionResponseAbstract handleBreakMoveEvent(ViewEventAbstract i_ViewEventAbstract)
    {
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        this.mr_BoardController.DrawState(boardState);
        return new ShowPeekPairActionResponse();
    }

    private ActionResponseAbstract handlePeekMoveEvent(ViewEventAbstract i_ViewEventAbstract)
    {
        PeekPair peekPair = i_ViewEventAbstract as PeekPair;   
        var modelMove = this.m_gameViewModel.GetModelMove(peekPair.FirstTuple, peekPair.SecondTuple);
        this.m_gameViewModel.ModelMove = modelMove;
        if (modelMove == null)
        {
            return new ShowIlegalMoveActionResponse();
        }
        if (modelMove.IsExclusive)
        {
            int firstSum = peekPair.FirstTuple.Item1.GetNumber + peekPair.FirstTuple.Item2.GetNumber;
            int secondSum = peekPair.SecondTuple.Item1.GetNumber + peekPair.SecondTuple.Item2.GetNumber;
            Tuple<int,int> options = new Tuple<int, int>(firstSum,secondSum);
            var viewMove = this.m_gameViewModel.GenerateViewMove(modelMove,peekPair);
            this.mr_BoardController.DrawChosenMove(viewMove);
            return new ShowPerformExclusiveMoveActionResponse(options);
        }
        var viewMove1 = this.m_gameViewModel.GenerateViewMove(modelMove, peekPair);
        this.mr_BoardController.DrawChosenMove(viewMove1);
        return new ShowPerformSimpleMoveActionResponse(modelMove,modelMove.FirstCombination == -1 || modelMove.SecondCombination == -1);
    }

    private ActionResponseAbstract handleForteitMove(ViewEventAbstract i_ViewEventAbstract)
    {
        this.m_gameViewModel.PerformForteitMove();
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        this.StartCoroutine(this.mr_BoardController.ForteitTurn(boardState));
        this.SwitchTurn();
        return null;
    }

    private ActionResponseAbstract handleContinueMove(ViewEventAbstract i_ViewEventAbstract)
    {
        this.SetCubes(this.m_gameViewModel.GetCurrentTurn());
        bool thereArePossibleMoves = this.m_gameViewModel.IsThereAPossibleMove();
        if (!thereArePossibleMoves)
        {
            return new ShowNoPossibleMovesActionResponse();
        }
        return new ShowPeekPairActionResponse();
    }

    private ActionResponseAbstract handleKeepMove(ViewEventAbstract i_ViewEventAbstract)
    {
        this.m_gameViewModel.PerformKeepMove();
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        bool isConqureHappened = this.mr_BoardController.KeepMove(boardState);
        this.SwitchTurn(isConqureHappened);
        return null;
    }


    private void SwitchTurn(bool i_IsConquareHappend = false)
    {
        if (this.m_gameViewModel.GetWonPlayer() != BoardController.ePlayer.None)
        {
            this.StartCoroutine(this.WinRoutine(this.m_gameViewModel.GetWonPlayer()));
            return;
        }

        this.StartCoroutine(this.StartTurn(i_IsConquareHappend));
    }


    private IEnumerator WinRoutine(BoardController.ePlayer i_WonPlayer)
    {
        var boardState = this.m_gameViewModel.GenerateViewGameState();
        this.mr_BoardController.DrawState(boardState);
        yield return new WaitForSeconds(2);
        //yield return this.StartCoroutine(this.mr_BoardController.WinAnimation(i_WonPlayer));
        yield return this.StartCoroutine(this.mr_BoardController.NewWinAnimation(i_WonPlayer));

        if (this.GameWasEndedEvent != null)
        {
            this.GameWasEndedEvent(i_WonPlayer);
        }
    }

    private IEnumerator StartTurn(bool i_IsConquareHappend = false , bool i_IsFirstTurn = false)
    {
        if (i_IsConquareHappend)
        {
            yield return new WaitForSeconds(0.3F);
        }
        var player = this.m_gameViewModel.GetCurrentTurn();
        var choice = this.m_gameViewModel.GetRandomChoice();
        yield return this.StartCoroutine(this.mr_PanelsController.SetCubes(player, choice.Choice,i_IsFirstTurn));
        yield return this.StartCoroutine(this.mr_PanelsController.StartTurn(player,i_IsFirstTurn));
    }
}

﻿using UnityEngine;
using System.Collections;

public class TooltipController : MonoBehaviour
{

    public GameObject mr_Tooltip;

    public MLTextMesh mr_Text;

    public string mr_TextId;

    public bool ShowStatus { get; set; }

    public void Init()
    {
        this.mr_Text.ChangeText(TextService.GetTextById(mr_TextId));
        this.mr_Tooltip.gameObject.SetActive(false);
    }


    public void OnShow()
    {
        if (Application.isWebPlayer || Application.isEditor)
        {
            this.ShowStatus = true;
            this.StartCoroutine(this.startWaiting());
        }
    }

    public void OnHide()
    {
        if (Application.isWebPlayer || Application.isEditor)
        {
            this.ShowStatus = false;
            this.mr_Tooltip.gameObject.SetActive(false);
        }
    }

    private IEnumerator startWaiting()
    {
        yield return new WaitForSeconds(0.5F);
        if (this.ShowStatus)
        {
            this.mr_Tooltip.gameObject.SetActive(true);
        }

        yield break;
    }
}

﻿using Assets.Scripts.PopUp_Manager.Abstracts;

using UnityEngine;
using System.Collections;

public abstract class ElementsArenaContentPopUpAbstract : AbstractContentPopUp
{
    public override IEnumerator Show()
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0.5F));
    }

    public override IEnumerator Hide()
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 1, 0, 0.5F));
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CellController : MonoBehaviour
{

    public tk2dSprite mr_Image;

    public int mr_CellId;

    public int mr_ColumnId;

    public List<Marker> mr_Markers;

    public BoardController.ePlayer mr_CurrentPlayer;

    private Color m_startColor;


    private Color m_initialColor = new Color(0.88f, 0.88f , 0.88f);

    private void Awake()
    {
        this.mr_Image.color = this.m_initialColor;
        this.m_startColor = this.mr_Image.color;
        
        this.mr_CurrentPlayer = BoardController.ePlayer.None;
    }



    public void ClearCell()
    {
        this.ChangeCellVisulalState(BoardController.ePlayer.None);
        this.clearMarkers();
    }


    private void clearMarkers()
    {
        foreach (var marker in this.mr_Markers)
        {
            marker.SetMarkerStatus(false);
        }
    }


    public void ChangeCellVisulalState(BoardController.ePlayer i_CurrentPlayer)
    {
        this.mr_CurrentPlayer = i_CurrentPlayer;
        switch (i_CurrentPlayer)
        {
            case BoardController.ePlayer.None:
                this.mr_Image.color = this.m_startColor;
                break;

            case BoardController.ePlayer.Purple:
                this.mr_Image.color = new Color(0.46f,0.23f,0.97f);
                break;

            case BoardController.ePlayer.Red:
                this.mr_Image.color = new Color(1f,0.39f,0.77f);
                break;

            default:
                break;

        }
    }


    public void HighlightCell()
    {
        if (this.mr_CurrentPlayer == BoardController.ePlayer.None)
        {
            this.mr_Image.color = new Color(1,1,-0.5F);
        }
    }

    public void StoreMarker()
    {
        if (this.mr_CurrentPlayer == BoardController.ePlayer.Red)
        {
            this.mr_Markers.Find(m => m.mr_Player == BoardController.ePlayer.Red).SetMarkerStatus(true);
        }
        if (this.mr_CurrentPlayer == BoardController.ePlayer.Purple)
        {
            this.mr_Markers.Find(m => m.mr_Player == BoardController.ePlayer.Purple).SetMarkerStatus(true);

        }
    }

    public void MarkCellAsUnAvaliable()
    {
        this.mr_Image.color = new Color(0.42f, 0.54f, 1f);
    }
}

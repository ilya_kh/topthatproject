﻿using UnityEngine;
using System.Collections;

public class StoneLocation  {

    public int ColumnId { get; set; }

    public int CelldId { get; set; }

    public StoneData FirstData { get; set; }

    public StoneData SecondData { get; set; }

}

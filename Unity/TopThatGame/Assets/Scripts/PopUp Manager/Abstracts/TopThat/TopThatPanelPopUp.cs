﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PopUp_Manager.Abstracts.TopThat
{
    public abstract class TopThatPanelPopUp : AbstractPopUpScreen
    {
        public event GameView.UserInteraction UserEvent;

        protected void SendUserInteractionEvent(ViewEventAbstract i_ViewEvent)
        {
            if (this.UserEvent != null)
            {
                this.UserEvent(i_ViewEvent);
            }
        }

        public override System.Collections.IEnumerator Show()
        {
            yield return StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0.3F));
        }

        public override System.Collections.IEnumerator Hide()
        {
            yield return StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 1, 0, 0.3F));
        }
    }
}

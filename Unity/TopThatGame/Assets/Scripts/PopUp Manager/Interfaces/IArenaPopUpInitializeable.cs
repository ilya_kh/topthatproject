﻿using UnityEngine;
using System.Collections;

public interface IArenaPopUpInitializeable
{
    IEnumerator InitializeEnumerator();
}

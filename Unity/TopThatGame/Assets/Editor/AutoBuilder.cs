﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;


public static class AutoBuilder
{
    private static string k_KeyStorePassWord = "GJ)(^IKHBKRKHGK@JHG4626hh2Y@@hh2$(GJK$^1!*#";

    static ProjectConfig ReadConfig()
    {
        string content = string.Empty;
        try
        {
            content = File.ReadAllText(Application.dataPath + "/Resources/ProjectConfig.txt");

        }
        catch (Exception)
        {

            return null;
        }
        ProjectConfig projectConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectConfig>(content);
        return projectConfig;
    }

    private static string GetOutputPath()
    {
        string content = string.Empty;
        try
        {
            content = File.ReadAllText(Application.dataPath + "/Resources/BuildConfig.txt");

        }
        catch (Exception)
        {

            return null;
        }

        Dictionary<string, string> buildDic = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
        PlayerSettings.bundleVersion = buildDic["Version"];
        return buildDic["OutputLocation"];
    }


    static string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        return s[s.Length - 2];
    }

    static string[] GetScenePaths()
    {
        string[] scenes = new string[EditorBuildSettings.scenes.Length];

        for (int i = 0; i < scenes.Length; i++)
        {
            scenes[i] = EditorBuildSettings.scenes[i].path;
        }

        return scenes;
    }

    [MenuItem("File/AutoBuilder/LocalAndroid")]
    private static bool PerformLocalAndroid(ProjectConfig projectConfig)
    {
        string androidManifestPath = Application.dataPath + "/Plugins/Android/AndroidManifest.xml";
        bool isManifestExsist = File.Exists(androidManifestPath);
        if (!isManifestExsist)
        {
            Debug.LogError("Android manifest was not found.");
            return false;
        }
        File.Move(androidManifestPath, Application.dataPath+"/Resources" + "/AndroidManifest.xml");
        PlayerSettings.Android.keystorePass = k_KeyStorePassWord;
        PlayerSettings.Android.keyaliasPass = k_KeyStorePassWord;
        PlayerSettings.Android.keystoreName = Application.dataPath + "/../mindlab.keystore";
        try
        {
            var response = BuildPipeline.BuildPlayer(GetScenePaths(), GetOutputPath() + PlayerSettings.bundleIdentifier + ".apk", BuildTarget.Android, BuildOptions.None);
            File.Move(Application.dataPath + "/Resources/AndroidManifest.xml", androidManifestPath);
            if (response == null)
            {
                return true;
            }
            else
            {
                Debug.LogError("Error occured during build pipeline");
                return false;
            }
        }
        catch (Exception)
        {
            File.Move(Application.dataPath + "/Resources/AndroidManifest.xml", androidManifestPath);

            return false;
        }

    }

    [MenuItem("File/AutoBuilder/HSP_Android")]
    private static bool PerformHspAndroidBuild(ProjectConfig projectConfig)
    {  
        PlayerSettings.Android.keystorePass = k_KeyStorePassWord;
        PlayerSettings.Android.keyaliasPass = k_KeyStorePassWord;
        PlayerSettings.Android.keystoreName = Application.dataPath + "/../mindlab.keystore";

        try
        {
            var response = BuildPipeline.BuildPlayer(GetScenePaths(), GetOutputPath() + PlayerSettings.bundleIdentifier + ".apk", BuildTarget.Android, BuildOptions.None);
            if (response == null)
            {
                return true;
            }
            else
            {
                Debug.LogError("Error occured during build pipeline");
                return false;
            }
        }
        catch (Exception)
        {
            return false;
        }

    }


    [MenuItem("File/AutoBuilder/Web")]
    static bool PerformWebBuild(ProjectConfig projectConfig)
    {
        var response = BuildPipeline.BuildPlayer(GetScenePaths(), GetOutputPath() + PlayerSettings.productName, BuildTarget.WebPlayer, BuildOptions.None);
        Debug.Log("<color=yellow>" + response + "</color>");
        if (response == null)
        {
            return true;
        }
        else
        {
            Debug.LogError("Error occured during build pipeline");
            return false;
        }
    }


    private static void PerformBuild()
    {
        ProjectConfig projectConfig = ReadConfig();
        if (projectConfig == null)
        {
            throw new Exception("No project config was found.");
        }

        bool isBuildPerformed = true;
        var enumValue = (ProjectConfig.eProjectType)Enum.Parse(typeof(ProjectConfig.eProjectType), projectConfig.CurrentPlatform, true);

        switch (enumValue)
        {
            case ProjectConfig.eProjectType.LOCAL_APK:
                isBuildPerformed = PerformLocalAndroid(projectConfig);
                break;

            case ProjectConfig.eProjectType.LOCAL_WEB:
                isBuildPerformed = PerformWebBuild(projectConfig);
                break;


            case ProjectConfig.eProjectType.HSP_APK:
                isBuildPerformed = PerformHspAndroidBuild(projectConfig);
                break;

            case ProjectConfig.eProjectType.HSP_WEB:
                isBuildPerformed = PerformWebBuild(projectConfig);
                break;

            default:
                break;
        }
    }

    private static void SwitchPlatform()
    {
        ProjectConfig projectConfig = ReadConfig();
        if (projectConfig == null)
        {
            throw new Exception("No project config was found.");
        }
        var enumValue = (ProjectConfig.eProjectType)Enum.Parse(typeof(ProjectConfig.eProjectType), projectConfig.CurrentPlatform, true);

        switch (enumValue)
        {
            case ProjectConfig.eProjectType.LOCAL_APK:
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
                break;

            case ProjectConfig.eProjectType.LOCAL_WEB:
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.WebPlayer);
                break;


            case ProjectConfig.eProjectType.HSP_APK:
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
                break;

            case ProjectConfig.eProjectType.HSP_WEB:
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.WebPlayer);
                break;

            default:
                break;
        }
    }
}
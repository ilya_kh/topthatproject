﻿using System.Collections;

using Assets.Scripts.PopUp_Manager.Abstracts;
using UnityEngine;

public class SuccessPopup : AbstractPopUpScreen
{
    //Events
    public delegate void PlayAgainButtonClickedDelegate();
    public event PlayAgainButtonClickedDelegate PlayAgainButtonWasClicked;

    public delegate void HomeButtonClickedDelegate();
    public event HomeButtonClickedDelegate HomeButtonWasClicked;

    // Logic 

    private SuccessState mr_SuccsesState;


    // Holders 

    public GameObject mr_WonPlayerHolder;

    public GameObject mr_PanelAndTextsHolder;

    public Animator mr_Animator;

    //Buttons
    public GameObject mr_RedButtons;
    public GameObject mr_PurpleButtons;

    //Sprites
    public GameObject mr_RedVictorySprite;
    public GameObject mr_PurpleVictorySprite;

    public MLTextMesh mr_RedPlayAgainButtonText;
    public MLTextMesh mr_RedHomeButtonText;
    public MLTextMesh mr_PurplePlayAgainButtonText;
    public MLTextMesh mr_PurpleHomeButtonText;

    public MLTextMesh mr_WinnerText;
    public MLTextMesh mr_MiddleHeadlineText;
    public MLTextMesh mr_WinReasonText;

    public override void Reset(object i_ResetObject)
    {
        if (i_ResetObject is SuccessState)
        {
            
            SuccessState successState = i_ResetObject as SuccessState;
            this.mr_SuccsesState = successState; 
            this.initFromSuccessState(successState);
            this.setButtonTexts();
        }
        else
        {
            Debug.LogError("Error: Argument must be of type: 'SuccessState'");
        }
    }

    private void initFromSuccessState(SuccessState i_SuccessState)
    {
        if (i_SuccessState.Winner == BoardController.ePlayer.Red)
        {
            this.setRedWinnerState(i_SuccessState);
        }
        else
        {
            if (i_SuccessState.Winner == BoardController.ePlayer.Purple)
            {
                this.setPurpleWinnerState(i_SuccessState);
            }
        }

        this.setCommonTexts();
    }

    private void setRedWinnerState(SuccessState i_SuccessState)
    {
        mr_RedButtons.gameObject.SetActive(true);
        mr_PurpleButtons.gameObject.SetActive(false);

        mr_RedVictorySprite.gameObject.SetActive(true);
        mr_PurpleVictorySprite.gameObject.SetActive(false);

        this.setRedWinnerTexts(i_SuccessState);
    }

    private void setPurpleWinnerState(SuccessState i_SuccessState)
    {
        mr_PurpleButtons.gameObject.SetActive(true);
        mr_RedButtons.gameObject.SetActive(false);

        mr_PurpleVictorySprite.gameObject.SetActive(true);
        mr_RedVictorySprite.gameObject.SetActive(false);

        this.setPurpleWinnerTexts(i_SuccessState);
    }

    private void setRedWinnerTexts(SuccessState i_SuccessState)
    {
        mr_WinnerText.ChangeText(TextService.GetTextById("3.4.2.2"));

        if (i_SuccessState.HasLoserResigned)
        {
            mr_WinReasonText.ChangeText(TextService.GetTextById("3.4.2.3"));
            mr_WinReasonText.gameObject.SetActive(true);
        }
        else
        {
            mr_WinReasonText.gameObject.SetActive(false);
        }
    }

    private void setPurpleWinnerTexts(SuccessState i_SuccessState)
    {
        mr_WinnerText.ChangeText(TextService.GetTextById("3.4.2.4"));

        if (i_SuccessState.HasLoserResigned)
        {
            mr_WinReasonText.ChangeText(TextService.GetTextById("3.4.2.5"));
            mr_WinReasonText.gameObject.SetActive(true);
        }
        else
        {
            mr_WinReasonText.gameObject.SetActive(false);
        }
    }

    private void setCommonTexts()
    {
        mr_MiddleHeadlineText.ChangeText(TextService.GetTextById("3.4.2.6"));
    }

    private void setButtonTexts()
    {
        mr_RedPlayAgainButtonText.ChangeText(TextService.GetTextById("3.4.1.1"));
        mr_PurplePlayAgainButtonText.ChangeText(TextService.GetTextById("3.4.1.1"));

        mr_RedHomeButtonText.ChangeText(TextService.GetTextById("3.4.1.2"));
        mr_PurpleHomeButtonText.ChangeText(TextService.GetTextById("3.4.1.2"));
    }

    // Update is called once per frame
    public override IEnumerator Show()
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.SuccsesMusic, SoundManagerStaticInfo.eTags.Effects, false);


        this.mr_WonPlayerHolder.gameObject.SetActive(false);
        this.mr_PanelAndTextsHolder.gameObject.SetActive(false);
        if (this.mr_SuccsesState.HasLoserResigned)
        {
            yield return this.StartCoroutine(this.showFromResign());
        }
        else
        {
            yield return this.StartCoroutine(this.showFromGame());
        }
    }


    private IEnumerator showFromResign()
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0.3F));
        this.mr_Animator.Play("WinAnimation");
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Earthquake, SoundManagerStaticInfo.eTags.Effects, false);
        yield return new WaitForSeconds(0.5F);
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.SuccessWhoosh, SoundManagerStaticInfo.eTags.Effects, false);
    }

    private IEnumerator showFromGame()
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0));
        this.gameObject.SetActive(true);
        this.mr_Animator.Play("WinAnimation");      
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Earthquake, SoundManagerStaticInfo.eTags.Effects, false);
        yield return new WaitForSeconds(0.5F);
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.SuccessWhoosh, SoundManagerStaticInfo.eTags.Effects, false);
        yield break;
    }



    public override IEnumerator Hide()
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 1, 0, 0.3F));
    }

    #region Buttons Click Handling

        public void PlayAgainButtonClicked()
        {
            SoundManager.StopAllSoundsByTag(SoundManagerStaticInfo.eTags.Effects);
            SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Equipment, SoundManagerStaticInfo.eTags.Effects, false);

            if (PlayAgainButtonWasClicked != null)
            {
                PlayAgainButtonWasClicked();
            }
        }

        public void HomeButtonClicked()
        {
            SoundManager.StopAllSoundsByTag(SoundManagerStaticInfo.eTags.Effects);
            SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Whoosh, SoundManagerStaticInfo.eTags.Effects, false);

            if (HomeButtonWasClicked != null)
            {
                HomeButtonWasClicked();
            }
        }

    #endregion
}

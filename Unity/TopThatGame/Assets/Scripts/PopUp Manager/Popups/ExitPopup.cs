﻿using Assets.Scripts.PopUp_Manager.Abstracts;

using UnityEngine;

public class ExitPopup : AbstractPopUpScreen
{
    //Events
    public delegate void YesButtonClickedDelegate();
    public event YesButtonClickedDelegate YesButtonWasClicked;

    public delegate void NoButtonClickedDelegate();
    public event NoButtonClickedDelegate NoButtonWasClicked;

    //Texts
    public MLTextMesh mr_Headline;
    public MLTextMesh mr_BodyText;
    public MLTextMesh mr_YesButtonText;
    public MLTextMesh mr_NoButtonText;


    public GameObject mr_Holder; 



    #region Buttons Click Handling

        public void YesButtonClicked()
        {
            if (YesButtonWasClicked != null)
            {
                YesButtonWasClicked();
            }
        }

        public void NoButtonClicked()
        {
            if (NoButtonWasClicked != null)
            {
                NoButtonWasClicked();
            }
        }

    #endregion

    public override void Reset(object i_Args)
    {
        this.initTexts();
    }

    private void initTexts()
    {
        mr_Headline.ChangeText(TextService.GetTextById("3.6.3.1"));
        mr_BodyText.ChangeText(TextService.GetTextById("3.6.3.2"));
        mr_YesButtonText.ChangeText(TextService.GetTextById("3.6.1.5"));
        mr_NoButtonText.ChangeText(TextService.GetTextById("3.6.1.6"));
    }


    public override System.Collections.IEnumerator Show()
    {
        this.mr_Holder.transform.localPosition = new Vector3(0, -880, 0);
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Earthquake, SoundManagerStaticInfo.eTags.Effects, false);
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0.3F));
        
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.mr_Holder, this.mr_Holder.transform.localPosition, Vector3.zero, 0.4F));

    }

    public override System.Collections.IEnumerator Hide()
    {
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.WhooshOut, SoundManagerStaticInfo.eTags.Effects, false);
        yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 1, 0, 0.4F));
    }
}

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PauseScreen.cs" company="Mindlab Group">
//   All rights reserverd (c) 2013
// </copyright>
// <summary>
//   The pause screen.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;

/// <summary>
///     The pause screen.
/// </summary>
public class PauseScreen : MonoBehaviour
{
    // Use this for initialization
    #region Public Methods and Operators

    /// <summary>
    ///     The hide.
    /// </summary>
    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    /// <summary>
    ///     The show.
    /// </summary>
    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    /// <summary>
    ///     The start.
    /// </summary>
    public void Start()
    {
        this.gameObject.transform.position = new Vector3(0, 0, -370);
        this.gameObject.SetActive(false);
    }

    #endregion
}
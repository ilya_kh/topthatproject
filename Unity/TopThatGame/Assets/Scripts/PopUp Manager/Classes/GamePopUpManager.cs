﻿using System;
using System.Collections;
using UnityEngine;

public class GamePopUpManager : PopUpManager
{

    #region Events 

    public event GameView.BackToMainRequested BackToMainEvent;

    public event GameView.GameWasEnded GameWasEndedEvent;

    public event SuccsesScreenView.GoToHome GoToHomeEvent;

    public event SuccsesScreenView.RestartGame RestartEvent;

    #endregion 


    private IEnumerator awakeEnumerator()
    {
        //Test
        yield return this.StartCoroutine(TextService.Instance.Init(-1));

        yield return this.StartCoroutine(this.Init());

        //Success Test
        SuccessState successState = new SuccessState();
        successState.Winner = BoardController.ePlayer.Red;
        successState.HasLoserResigned = true;

        yield return this.StartCoroutine(this.ShowPopUp(ePopUpType.Success, successState));

        //Rules test
        //yield return this.StartCoroutine(this.ShowPopUp(ePopUpType.Rules));

        //Exit test
        //yield return this.StartCoroutine(this.ShowPopUp(ePopUpType.Exit));

        //Back To Main test
        //yield return this.StartCoroutine(this.ShowPopUp(ePopUpType.BackToMain));

        //Resign Test

//        ResignState resignState = new ResignState();
//        resignState.Resigner = BoardController.ePlayer.Purple;
//
//        yield return this.StartCoroutine(this.ShowPopUp(ePopUpType.Resign, resignState));

        this.UnPause();
    }

    public IEnumerator ShowPopUp(ePopUpType i_PopUpType, object i_Args = null)
    {
        yield return this.StartCoroutine(this.Show(i_PopUpType, i_Args));
    }


    public override IEnumerator SpecificInit()
    {
        RulesPopup rulesPopup = this.GetComponentInChildren<RulesPopup>();
        if (rulesPopup != null)
        {
            rulesPopup.CloseButtonWasClicked += this.onRulesPopupOnCloseButtonWasClicked;
        }
        ResignPopup resignPopup = this.GetComponentInChildren<ResignPopup>();
        if (resignPopup != null)
        {
            resignPopup.YesButtonWasClicked += this.onResignPopUpConfirmedEvent;
            resignPopup.NoButtonWasClicked += this.onResignPopUpDeclineEvent;
        }

        ExitPopup exitPopup = this.GetComponentInChildren<ExitPopup>();
        if (exitPopup != null)
        {
            exitPopup.YesButtonWasClicked += this.onExitPopupOnYesButtonWasClicked;
            exitPopup.NoButtonWasClicked += this.onExitPopupOnNoButtonWasClicked;
        }
        BackToMainPopup backToMainPopup = this.GetComponentInChildren<BackToMainPopup>();
        if (backToMainPopup != null)
        {
            backToMainPopup.YesButtonWasClicked += this.onBackToMainPopupOnYesButtonWasClicked;
            backToMainPopup.NoButtonWasClicked += this.onBackToMainPopupOnNoButtonWasClicked;
        }
        SuccessPopup successPopup = this.GetComponentInChildren<SuccessPopup>();
        if (successPopup != null)
        {
            successPopup.PlayAgainButtonWasClicked += this.onSuccessPopupOnPlayAgainButtonWasClicked;
            successPopup.HomeButtonWasClicked += this.onSuccessPopupOnHomeButtonWasClicked;
        }

        yield break;
    }

    private void onSuccessPopupOnHomeButtonWasClicked()
    {
        if (this.GoToHomeEvent != null)
        {
            this.GoToHomeEvent();
        }
    }

    private void onSuccessPopupOnPlayAgainButtonWasClicked()
    {
        if (this.RestartEvent != null)
        {
            this.RestartEvent();
        }
    }

    private void onBackToMainPopupOnNoButtonWasClicked()
    {
        this.StartCoroutine(this.Hide());
    }

    private void onBackToMainPopupOnYesButtonWasClicked()
    {
        this.StartCoroutine(this.onBackToMainEnumarator());
    }

    private void onExitPopupOnNoButtonWasClicked()
    {
        this.StartCoroutine(this.Hide());
    }

    private void onExitPopupOnYesButtonWasClicked()
    {
        // TODO : Add listener to WEB case .
        if (Application.platform == RuntimePlatform.Android)
        {
            Application.Quit();
        }
        if (Application.platform == RuntimePlatform.WindowsWebPlayer)
        {
            Application.ExternalEval("jQuery(document).trigger('unityClose');");
        }
    }

    private void onResignPopUpDeclineEvent()
    {
        this.StartCoroutine(this.Hide());
    }

    private void onResignPopUpConfirmedEvent(BoardController.ePlayer i_Player)
    {
        this.StartCoroutine(this.onResignEnumarator(i_Player));
    }

    private void onRulesPopupOnCloseButtonWasClicked()
    {
        this.StartCoroutine(this.Hide());
    }



    private IEnumerator onBackToMainEnumarator()
    {
        yield return StartCoroutine(this.Hide());
        if (this.BackToMainEvent != null)
        {
            this.BackToMainEvent();
        }
    }


    private IEnumerator onResignEnumarator(BoardController.ePlayer i_Player)
    {
        if (this.GameWasEndedEvent != null)
        {
            this.GameWasEndedEvent(i_Player == BoardController.ePlayer.Purple ? BoardController.ePlayer.Red : BoardController.ePlayer.Purple, true);
        }
        yield break;
    }
}


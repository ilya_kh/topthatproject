﻿using System;

using UnityEngine;
using System.Collections;

/// <summary>
/// This class is main View class of Splash/Main screen .
/// </summary>
public class MainScreenView : MonoBehaviour
{

    public bool m_IsActiveScreen = true;

    private readonly float m_MaxWaitingTime = 60F;

    private float m_WaitedTime = 0;

    public Animator mr_Splash;

    #region Events 

    public delegate void StartGame();

    public event StartGame StartGameEvent;

    #endregion

    public GameObject mr_Girl;

    public SplashMenu mr_SplashMenu;

    public PopUpManager m_PopUpManager;


    public IEnumerator Init()
    {
        this.mr_SplashMenu.StartGame += this.onSplashMenuOnStartGame;
        yield return this.StartCoroutine(this.mr_SplashMenu.Init(this.m_PopUpManager));
        yield break;
    }

    private void onSplashMenuOnStartGame()
    {
        this.m_IsActiveScreen = false;
        this.StopAllCoroutines();
        SoundManager.StopAllSoundsByTag(SoundManagerStaticInfo.eTags.Music);
        if (this.StartGameEvent != null)
        {
            this.StartGameEvent();
        }
    }

    public IEnumerator EnableScreen(bool i_Fade = true)
    {
        this.gameObject.transform.localPosition = Vector3.zero;
        this.m_IsActiveScreen = true;
        if (i_Fade)
        {
            yield return this.StartCoroutine(GenericAnimator.Instance.Fade(this.gameObject, 0, 1, 0.3F));
        }
        this.StartCoroutine(this.girlAnimation());
        yield break;
    }

    public void DisableScreen()
    {
        this.gameObject.transform.localPosition = new Vector3(3000,3000,3000);
        this.m_IsActiveScreen = false;
    }


    public IEnumerator HideAnimation()
    {
        yield return this.StartCoroutine(GenericAnimator.Instance.MoveLocal(this.gameObject, this.gameObject.transform.localPosition, new Vector3(0, 900, 0), 0.5F));
        this.DisableScreen();
        yield break;
    }

    public IEnumerator StartSplashAnimation()
    {

        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.MainMusic, SoundManagerStaticInfo.eTags.Music, true);
        // TODO : Implement animation .
        this.mr_Splash.Play("Splash");
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Earthquake, SoundManagerStaticInfo.eTags.Effects, false);
        yield return new WaitForSeconds(0.8F);
        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.Whoosh3, SoundManagerStaticInfo.eTags.Effects, false);
        yield return new WaitForSeconds(1.8F);
        this.mr_Splash.enabled = false;

        this.StartCoroutine(this.girlAnimation());
        //this.mr_Splash.Play("GirlAnim");
        //this.mr_Splash.Play("GirlAnim");
        this.m_IsActiveScreen = true;
        //this.mr_SplashAnimation.Stop();
        yield break;
    }

    private IEnumerator girlAnimation()
    {
        while (this.m_IsActiveScreen)
        {
            yield return this.StartCoroutine(GenericAnimator.Instance.RotateLocal(this.mr_Girl, new Vector3(0, 0, 0), new Vector3(0, 0, -1), 1));
            yield return this.StartCoroutine(GenericAnimator.Instance.RotateLocal(this.mr_Girl, new Vector3(0, 0, -1), new Vector3(0, 0, 0), 1));
            yield return this.StartCoroutine(GenericAnimator.Instance.RotateLocal(this.mr_Girl, new Vector3(0, 0, 0), new Vector3(0, 0, 1), 1));
            yield return this.StartCoroutine(GenericAnimator.Instance.RotateLocal(this.mr_Girl, new Vector3(0, 0, 1), new Vector3(0, 0, 0), 1));
        }
    }


    private IEnumerator IdleAnimation()
    {

        SoundManager.PlaySound(SoundManagerStaticInfo.eSoundIds.StoneSound, SoundManagerStaticInfo.eTags.Effects, false);
        yield return this.StartCoroutine(GenericAnimator.Instance.LocalScale(this.mr_SplashMenu.mr_StartGameButton.gameObject, Vector3.one, new Vector3(1.2F,1.2F,1), 0.5F));
        yield return this.StartCoroutine(GenericAnimator.Instance.LocalScale(this.mr_SplashMenu.mr_StartGameButton.gameObject, new Vector3(1.2F, 1.2F, 1),Vector3.one, 0.2F));
    }

    private void Update()
    {
        if (!this.m_IsActiveScreen)
        {
            return;
        }

        this.m_WaitedTime = this.m_WaitedTime + Time.deltaTime;
        if (this.m_WaitedTime > this.m_MaxWaitingTime)
        {
            this.m_WaitedTime = 0;
            this.StartCoroutine(this.IdleAnimation());
        }
    }

}
